FROM alpine:latest

RUN echo -e "http://dl-cdn.alpinelinux.org/alpine/v3.6/community/\nhttp://dl-cdn.alpinelinux.org/alpine/v3.6/main/" >> /etc/apk/repositories
RUN apk add --no-cache nodejs-current=7.10.1-r1

WORKDIR /app
COPY . /app
ARG registry
RUN apk --no-cache add --virtual build-dependencies ca-certificates nodejs-current-npm=7.10.1-r1 \
    && npm install --quiet --production --no-progress --registry=${registry:-https://registry.npmjs.org} \
    && npm cache clean --force \
    && apk del build-dependencies nodejs-current-npm

ARG port
EXPOSE $port
ENTRYPOINT ["node"]
CMD ["index.js"]
