NAME         := poc-operationalmonitoring-ui
NPM_REGISTRY := $(shell npm config get registry)
DOCKER_REGISTRY ?= registry.gitlab.com/tincatibo/
PORT         := 5001
NPM          := npm
DOCKER       := docker
GIT          := git
TAG          := latest
IMAGE        := $(DOCKER_REGISTRY)$(NAME):$(TAG)
NOCACHE      ?= 0

ifeq ($(NOCACHE), 1)
		DOCKER_BUILD_OPTS=--no-cache
endif

node_modules: package.json ## install node modules
	$(NPM) install --quiet --registry=$(NPM_REGISTRY)

./bundle/bundle.js: node_modules ## install node modules
	$(NPM) run build

image: ./bundle/bundle.js  ## build docker image
	$(DOCKER) build $(DOCKER_BUILD_OPTS)  --tag $(IMAGE) --build-arg registry=$(NPM_REGISTRY) --build-arg port=$(PORT) .

push: ## push docker image to repository
	$(DOCKER) push $(IMAGE)

cleanup: ## remove build artifacts
	-rm -rf node_modules coverage

help: ## display help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z._-]+:.*?## / {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help
