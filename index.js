const path = require('path');
const express = require('express');
const co = require('co');

const Config = require('./srv/config/config');
const Logger = require('./srv/config/logger');

const applicationName = process.env.APPLICATION_NAME || 'self-monitoring';
const configURI = process.env.CONFIG_ENDPOINT || 'http://spider.io/config/v1/config';

const log = Logger(applicationName);

co(function * (){
    const config = yield Config.initConfig(configURI, applicationName, log);

    const app = express();

    // enable loading local static files
    app.use(express.static('public'));

    //expose bundle
    app.use('/static',express.static('bundle'));

    app.get('/', function(req, res) {
        res.sendFile(path.join(__dirname, 'index.html'));
    });

    app.get('/config', function(req, res) {
        res.setHeader('Content-Type','application/ld+json');
        res.status(200).send(config.getClientConfig());
    });

    const server = app.listen(config.serverPort, function(err) {
        if (err) {
            throw err;
        }

        log.info(`Spider ${applicationName} listening on port ${config.serverPort}!`);

        process.on('SIGTERM', () => {
            server.close(() => {
                setTimeout(() => {
                    log.info(`Spider ${applicationName} exited!`);
                    process.exit(0);
                }, 1000)
            })
        });
    });
}).catch(err => {
    log.error(err, `Couldn't launch application Spider ${applicationName}`);
    process.exit(1);
});
