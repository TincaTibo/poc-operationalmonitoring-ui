const path = require('path');
const webpack = require('webpack');

module.exports = {
    //devtool: 'source-map',
    entry: [
        'babel-polyfill',
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'bundle'),
        filename: 'bundle.js',
        publicPath: '/network/static/'
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compressor: {
                warnings: true
            }
        })
    ],
    resolve: {
        extensions: ['', '.js', '.jsx'],
        alias: {
            joi: 'joi-browser'
        }
    },
    module: {
        loaders: [{
            loader: 'babel-loader',

            // Skip any files outside of your project's `src` directory
            include: [
                path.resolve(__dirname, 'src'),
            ],
            exclude: [/joi-browser/],
            // Only run `.js` and `.jsx` files through Babel
            test: /\.jsx?$/,

            // Options to configure babel with
            query: {
                plugins: ['transform-runtime'],
                presets: ['es2015', 'stage-0', 'react'],
            }
        },
            {
                test: /\.less$/,
                loader: 'style!css!less'
            },
            {
                test: /\.css$/,
                loader: 'style!css'
            }]
    },
};
