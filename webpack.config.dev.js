const path = require('path');
const webpack = require('webpack');

module.exports = {
    devtool: 'cheap-module-eval-source-map',
    entry: [
        'eventsource-polyfill', // necessary for hot reloading with IE
        'webpack-hot-middleware/client?path=/__webpack_hmr',
        'babel-regenerator-runtime', // needed to use co or Q on client side
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        localPath: '/static/',
        publicPath: '/network/static/'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ],
    // require('./code') vs. require('./code.jsx');
    // import foo from './code';
    resolve: {
        extensions: ['', '.js', '.jsx'],
        alias: {
            joi: 'joi-browser'
        }
    },
    module: {
        loaders: [{
            test: /\.js|\.jsx?/,
            loaders: ['babel'],
            include: path.join(__dirname, 'src'),
        },
            {
                test: /\.less$/,
                loader: "style!css!less"
            },
            {
                test: /\.css$/,
                loader: "style!css"
            }],
        // require('style.less')
        // --> <link ...
    }
};
