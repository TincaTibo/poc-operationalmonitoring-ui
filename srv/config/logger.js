const bunyan = require('bunyan');
const bformat = require('bunyan-format');

let log = null;

module.exports = (applicationName) => {
    if(!log && applicationName) {
        log = bunyan.createLogger({
            name: applicationName,
            streams: [
                {
                    stream: bformat({outputMode: 'long'}),
                    level: process.env.LOG_LEVEL_STDOUT || bunyan.INFO
                },
                {
                    type: 'rotating-file',
                    path: `/var/log/spider/${applicationName}-${process.env.HOSTNAME}.log`,
                    period: '1d',
                    count: 3,
                    level: process.env.LOG_LEVEL_FILE || bunyan.INFO
                }],
            serializers: bunyan.stdSerializers
        });
    }

    return log;
};