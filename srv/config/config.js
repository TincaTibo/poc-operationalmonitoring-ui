const FS = require('q-io/fs');
const _ = require('lodash');

let config = null;
const EXPECTED_VERSION = '0.1';

class Config{
    constructor(source){
        _.assign(this, source);

        config = this;
    }

    getClientConfig(){
        return this.guiConfig;
    }

    static *initConfig(spiderConfigURI, applicationName, log){

        const conf = yield FS.read(`config.json`);
        return new Config(_.assign(JSON.parse(conf), {applicationName, log}));
    }

    static getInstance () {
        return config;
    }
}


module.exports = Config;