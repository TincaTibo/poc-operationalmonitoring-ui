import {connect} from 'react-redux';

import OptionsIconBar from './OptionsIconBar';

const mapStateToProps = (state, ownProps) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const OptionsIconBarContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(OptionsIconBar);

export default OptionsIconBarContainer;