import React, { Component, PropTypes } from 'react';
import _ from 'lodash';

import {DATETIME_FORMAT_MINUTE} from '../../../config/locale';
import GridContainer from '../GridContainer';
import ItemShortViewContainer from '../../../containers/view/ItemShortViewContainer';
import DownloadItem from '../icons/DownloadItem';

const defaultColumnsDef = new Map([
    ['park', {
        id: 'park',
        title: 'Park',
        style: {width: 80, minWidth: 80},
        visible: true,
        resizable: true,
        render: item => <ItemShortViewContainer item={item.park}/>
    }],
    ['pos', {
        id: 'pos',
        title: 'Terminal',
        style: {width: 80, minWidth: 80},
        visible: true,
        resizable: true,
        render: item => <ItemShortViewContainer item={item.pos}/>
    }],
    ['eventName', {
        id: 'eventName',
        title: 'Status\u00a0ID',
        style: {width: 80, minWidth: 80},
        visible: true,
        resizable: true,
        render: item => item.eventName
    }],
    ['eventStatus', {
        id: 'eventStatus',
        title: 'Status',
        style: {width: 80, minWidth: 80},
        visible: true,
        resizable: true,
        render: item => item.eventStatus && item.eventStatus.replace('sp:','')
    }],
    ['startDate', {
        id: 'startDate',
        title: 'Start\u00a0date',
        style: {width: 130, minWidth: 130},
        visible: true,
        resizable: true,
        sortKey: 'startTime', //tells if sortable and what key is used for it, same key can be used for many columns
        render: item => item.startTime.format(DATETIME_FORMAT_MINUTE)
    }],
    ['endDate', {
        id: 'endDate',
        title: 'End\u00a0date',
        style: {width: 130, minWidth: 130},
        visible: true,
        resizable: true,
        sortKey: 'endTime', //tells if sortable and what key is used for it, same key can be used for many columns
        render: item => item.endTime && item.endTime.format(DATETIME_FORMAT_MINUTE)
    }],
    ['updateDate', {
        id: 'updateDate',
        title: 'Update\u00a0date',
        style: {width: 130, minWidth: 130},
        visible: true,
        resizable: true,
        sortKey: 'lastUpdatedDate', //tells if sortable and what key is used for it, same key can be used for many columns
        render: item => item.lastUpdatedDate && item.lastUpdatedDate.format(DATETIME_FORMAT_MINUTE)
    }],
    ['eventType', {
        id: 'eventType',
        title: 'Event\u00a0type',
        style: {width: 130, minWidth: 130},
        visible: true,
        resizable: true,
        render: item => item.eventType && item.eventType.replace('sp:','')
    }],
    ['family', {
        id: 'family',
        title: 'Family',
        style: {width: 130, minWidth: 130},
        visible: true,
        resizable: true,
        render: item => item.peripheralType && item.peripheralType.replace('sp:','')
    }],
    ['actions', {
        id: 'actions',
        title: 'Actions',
        style: {width: 40, minWidth: 40},
        visible: true,
        resizable: true,
        render: item => <DownloadItem item={item}/>
    }],
]);

function renderOneToMany(items, render){
    if(items){
        if(_.isArray(items) && items.length){
            return (
                <span>
                    {render ? render(items[0]) : items[0]} {items.length-1 ? `(+${items.length-1})` : ''}
                </span>
            )
        }
        else if(_.isString(items)){
            return render ? render(items) : items;
        }
        else {
            return '';
        }
    }
    else {
        return '';
    }
}

const marginWidth = 50;
const checkBoxWidth = 25;

export default class LogsGrid extends Component {

    constructor(props){
        super(props);

        //Table of columns def in the wanted order
        this.state = {
            columns : this.prepareColumns(props)
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ columns: this.prepareColumns(nextProps)});
    }

    prepareColumns(props){
        let totalWidth = 0;
        let growableCols = [];

        //Array of object with {id, width, visible}
        const newColumns = props.columns.map((col) => {
            const defaultColDef = defaultColumnsDef.get(col.id);

            if(!defaultColDef){
                console.log(`Error, column unknown: ${col.id}`);
                return null;
            }
            else{
                const columnDef = {
                    ...defaultColDef
                };

                columnDef.style.width = col.width || defaultColDef.style.width;
                columnDef.visible = col.visible !== undefined ? col.visible : defaultColDef.visible;

                if(columnDef.visible && !columnDef.growable){
                    totalWidth += columnDef.style.width;
                }
                else if(columnDef.visible && columnDef.growable){
                    growableCols.push(columnDef);
                }

                return columnDef;
            }
        }).filter(col => col);


        if(growableCols){
            growableCols.forEach(col =>
                col.style.width = _.max([(props.width - marginWidth - totalWidth - checkBoxWidth)/growableCols.length, col.style.minWidth])
            )
        }

        return newColumns;
    }

    render() {
        return (
            <GridContainer
                columns={this.state.columns}
                resourceType="LOG"
                width={this.props.width}
                height={this.props.height}
            />
        );
    }

    static propTypes = {
        columns: PropTypes.array,
        width: PropTypes.number,
        height: PropTypes.number,
    };

}