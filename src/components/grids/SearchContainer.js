import { connect } from 'react-redux';
import Search from '../../components/search';
import {GridsActions} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        query: state.grids[ownProps.resourceType].query,
        error: state.grids[ownProps.resourceType].searchError,
        historySteps: state.userInfo.historySteps,
        view: ownProps.resourceType,
        width: ownProps.width
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onSearch: query => {
            dispatch(GridsActions.requestSearch({
                collection: ownProps.resourceType,
                query
            }));
        },
        onClear: () => {
            dispatch(GridsActions.requestSearch({
                collection: ownProps.resourceType,
                query: ''
            }));
        },
        onUndo: () => {
            window.history.back();
        }
    }
};

const SearchContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Search);

export default SearchContainer;