import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';

import GridHeader from './GridHeader';
import GridRecords from './GridRecords';
import OptionsIconBar from './OptionsIconBarContainer';
import SelectContainer from './SelectContainer';
import SelectAllContainer from './SelectAllContainer';
import Counter from './Counter';
import Colors from '../../config/colors';

const styles = {
    recordsDiv:{
        overflow: 'hidden',
    },
    counter: {
    },
    title: {
        fontSize: 15,
        fontFamily: 'Roboto, sans-serif',
        color: Colors.medium,
        paddingBottom: 10,
        paddingLeft: 10,
    }
};

const iconBarWidth = 25;

export default class Grid extends Component {

    constructor(props){
        super(props);

        this.recordsContainer = null;
        this.headerContainer = null;

        this.selected = null;
        this.state = {
            fetching: false,
            scrolling: false,
            searching: false,
            resetScroll: false,
        };

        if(props.selected){
            this.state = {
                ...this.state,
                ...this.setScrolling(props)
            }
        }
    }

    setScrolling(props){
        let newState = {};
        if(_.findLastIndex(props.items, {'@id': props.selected}) === -1) {
            if(props.totalItems && props.items.length < props.totalItems) {
                newState.fetching = true;
                newState.searching = true;
                props.onFetchMore();
            }
        }
        else{
            newState.scrolling = true;
        }
        return newState;
    }

    componentWillReceiveProps(nextProps){
        //back to top if new table
        let newState = {};

        if(nextProps.items !== this.props.items && !this.state.fetching){
            newState = {
                ...newState,
                fetching: false,
                searching: false,
                scrolling: false,
                resetScroll: true,
            };
        }

        //selected item -> scroll to it if possible (and have a limit)
        if(nextProps.selected && nextProps.selected !== this.props.selected){
            newState = {
                ...newState,
                ...this.setScrolling(nextProps)
            };
        }

        //returned from fetchMore
        if(this.state.fetching && nextProps.items !== this.props.items){
            //we were searching for a selected item
            if(this.state.searching && nextProps.shouldSearchForSelected){
                //if we fetched the record, stop fetching
                if(_.findLastIndex(nextProps.items, {'@id': nextProps.selected}) > -1){
                    newState = {
                        ...newState,
                        fetching: false,
                        searching: false,
                        scrolling: true
                    };
                }
                else{
                    //else, fetch more if not at the end
                    if(nextProps.totalItems && nextProps.items.length < nextProps.totalItems){
                        nextProps.onFetchMore();
                    }
                }
            }
            else{
                newState = {
                    ...newState,
                    fetching: false,
                    searching: false,
                    scrolling: false
                };
            }
        }

        if(Object.getOwnPropertyNames(newState).length){
            this.setState(newState);
        }
    }

    render() {
        const columns = (this.props.local || !this.props.showIcons) ?
            this.props.columns
            : [
                {
                    id: 'select',
                    title: <SelectAllContainer collection={'LOG'} items={this.props.items} style={{marginLeft: 9}}/>,
                    style: {width: 25, minWidth: 25, textAlign: 'left', display:'table-cell'},
                    visible: true,
                    resizable: false,
                    render: (item) => (
                        <SelectContainer
                            item={item}
                            collection={this.props.resourceType}
                        />
                    )
                },
                ...this.props.columns
            ];

        return (
            <div
                id={'grid'}
                style={{
                    display: 'inline-block',
                    width: this.props.width,
                    height: this.props.height
                }}
            >
                {this.props.showIcons &&
                <div style={{
                    display: 'table-cell',
                    width: iconBarWidth,
                    minWidth: iconBarWidth,
                    paddingRight: 5
                }}>
                    {this.renderIcons()}
                </div>
                }
                <div
                    style={{
                        display: 'table-cell',
                        overflow: 'auto',
                        backgroundColor: '#ffffff',
                        borderRadius: 5,
                        maxWidth: this.props.width - (this.props.showIcons ? iconBarWidth : 0 ) - 5,
                    }}>
                    <div style={{
                        height: 25,
                        paddingLeft: 12,
                        paddingTop: 10,
                        paddingRight: 12
                    }}>
                        <Counter
                            total={this.props.totalItems}
                            current={this.props.items && this.props.items.length}
                            selected={this.props.selectedItems}
                            prefix=""
                            suffix=" records"
                            noRecordText="No record"
                            style={styles.counter}
                        />
                        <div style={{
                            display: 'inline-block',
                            float: 'right',
                            fontSize: '12px',
                            fontFamily: 'Roboto, sans-serif',
                            userSelect: 'none',
                            color: 'grey'
                        }}>
                            {this.props.lastUpdated && `Last updated: ${this.props.lastUpdated.humanize()} ago`}
                        </div>
                    </div>
                    <div ref={value => {this.headerContainer = value}}
                         style={{
                             overflow: 'hidden'
                         }}
                    >
                        <GridHeader
                            columns={columns}
                            items={this.props.items}
                            local={this.props.local}
                            sort={this.props.sort}
                            onResize={this.props.onResize}
                            onEndResize={this.props.onEndResize}
                            onChangeSort={this.props.onChangeSort}
                        />
                    </div>
                    <div style={{
                        ...styles.recordsDiv,
                        height: this.props.height - 55
                    }}
                         onScroll={this.handleScroll}
                    >
                        <GridRecords
                            columns={columns}
                            items={this.props.items}
                            selected={this.props.selected}
                            onOpen={this.props.onOpen}
                            setSelected={(value) => {this.selected = value}}
                            setScrollDiv={(value) => {this.recordsContainer = value}}
                        />
                    </div>
                </div>
            </div>
        );
    }

    renderIcons(){
        return (
            <OptionsIconBar
                columns={this.props.columns}
                collection={this.props.resourceType}
            />
        );
    }

    componentDidUpdate() {
        const scrollDiv = ReactDOM.findDOMNode(this.recordsContainer).parentElement.parentElement;
        const newState = {};

        if (this.state.scrolling && this.selected) {
            const offsetToShow = ReactDOM.findDOMNode(this.selected).offsetTop;
            //if not in window
            if (offsetToShow < scrollDiv.scrollTop ||
                offsetToShow > scrollDiv.scrollTop + scrollDiv.clientHeight - 24) {
                scrollDiv.scrollTop = offsetToShow - scrollDiv.clientHeight / 2;
            }

            newState.scrolling = false;
        }

        if (this.state.resetScroll) {
            scrollDiv.scrollTop = 0;


            newState.resetScroll = false;
        }

        //Ask for more if we display all (no scroll) and got more to show
        if (!this.state.fetching) {
            if(this.props.totalItems && this.props.items.length < this.props.totalItems){
                if((scrollDiv.scrollHeight - (scrollDiv.scrollTop + scrollDiv.clientHeight)) < 20){
                    newState.fetching = true;
                    this.props.onFetchMore();
                }
            }
        }

        if(Object.getOwnPropertyNames(newState).length) {
            this.setState(newState);
        }
    }

    handleScroll = (event) => {
        let container = event.target;

        this.headerContainer.scrollLeft = container.scrollLeft;

        //block if fetching
        if(!this.state.fetching){
            //when only 50 px from end, ask for more
            if((this.props.totalItems && this.props.items.length < this.props.totalItems) || !this.props.totalItems){
                if((container.scrollHeight - (container.scrollTop + container.clientHeight)) < 20){
                    this.setState({fetching: true});
                    this.props.onFetchMore();
                }
            }
        }
    };

    static propTypes = {
        resourceType: PropTypes.string.isRequired,
        columns: PropTypes.array.isRequired,
        local: PropTypes.bool,
        sort: PropTypes.shape({
            key: PropTypes.string.isRequired,
            order: PropTypes.string.isRequired
        }),
        items: PropTypes.array.isRequired,
        totalItems: PropTypes.number,
        height: PropTypes.number.isRequired,
        selected: PropTypes.string,
        shouldSearchForSelected: PropTypes.bool,
        showIcons: PropTypes.bool,
        selectedItems: PropTypes.number,
        lastUpdated: PropTypes.object,

        onOpen: PropTypes.func.isRequired,
        onResize: PropTypes.func,
        onEndResize: PropTypes.func,
        onChangeSort: PropTypes.func,
        onFetchMore: PropTypes.func
    };

    static defaultProps = {
        local: false,
        showIcons: true
    }
}