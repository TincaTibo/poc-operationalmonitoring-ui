import React, { PropTypes } from 'react';
import Button from 'material-ui/IconButton';
import Icon from 'material-ui/svg-icons/file/file-download';
import ToolTip from 'rc-tooltip';
import Base64 from 'js-base64';
import '../../tips/tipDark.css';

const style={
    width: 18,
    height: 18,
    padding: 0
};

const DownloadItem = ({item}) => (
    <ToolTip
        placement={'top'}
        prefixCls={'rc-tooltip-dark'}
        overlay={'Download item'}
        mouseEnterDelay={0.5}
        destroyTooltipOnHide={true}
    >
        <Button style={style}
                onTouchTap={(event) => {event.stopPropagation(); downloadItem(item)}}
        >
            <Icon hoverColor='#64C4E0'/>
        </Button>
    </ToolTip>
);

function downloadItem(toDownload){
    const payload = Base64.Base64.encode(JSON.stringify(toDownload, null, 2));

    const title = `${toDownload['@type'] ? toDownload['@type']:'UnTyped'}-${toDownload['@id']}${toDownload['alternateName'] ? '-' + toDownload['alternateName']:''}`;

    const link = document.createElement('a');
    link.download = title + '.json-ld';
    link.href = 'data:application/ld+json;base64,' + payload;
    link.click();
}

DownloadItem.propTypes = {
    item: PropTypes.object.isRequired
};

export default DownloadItem;