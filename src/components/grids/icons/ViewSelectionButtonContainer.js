import {connect} from 'react-redux';

import ViewSelectionButton from './ViewSelectionButton';
import {UserMessagesActions, GridsActions} from '../../../actions';

const mapStateToProps = (state, ownProps) => {
    const selected = state.grids[ownProps.collection].selected;
    return {
        selected,
        disabled: !(selected && selected.length)
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onClick: (selected) => {
            if (selected.length) {
                dispatch(GridsActions.showSelected({collection: ownProps.collection}));
            }
            else{
                dispatch(UserMessagesActions.showMessage('No selection'));
            }
        },
    }
};

const ViewSelectionButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ViewSelectionButton);

export default ViewSelectionButtonContainer;