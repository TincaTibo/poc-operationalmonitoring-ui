import React, { PropTypes, Component } from 'react';
import Button from 'material-ui/FlatButton';
import Icon from 'material-ui/svg-icons/action/view-column';
import ToolTip from 'rc-tooltip';
import Popover from 'material-ui/Popover';
import Checkbox from 'material-ui/Checkbox';

import Colors from '../../../config/colors';

const styles = {
    checkbox: {
        marginBottom: 0
    },
    label:{
        fontSize: '13px',
        lineHeight: '24px'
    },
    button: {
        width: 20,
        height: 20,
        minWidth: 20,
        padding: 0,
        margin: 3,
        lineHeight: '20px'
    },
};

export default class SelectColumnsButton extends Component{
    constructor(props) {
        super(props);

        this.state = {
            open: false,
        };
    }

    handleTouchTap = (event) => {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            open: true,
            anchorEl: event.currentTarget,
        });
    };

    handleRequestClose = () => {
        this.setState({
            open: false,
        });
    };

    render() {

        return (
            <div>
                <ToolTip
                    placement={this.props.tipPlacement}
                    prefixCls={'rc-tooltip-dark'}
                    overlay={'Select columns'}
                    mouseEnterDelay={0.5}
                    destroyTooltipOnHide={true}
                >
                    <Button
                        icon={<Icon
                            color= {this.props.disabled ? Colors.menu.iconInactive : Colors.menu.iconActive}
                            viewBox="0 3 24 21"
                            style={{
                                width: 18,
                                height: 18,
                            }}
                        />}
                        onTouchTap={this.handleTouchTap}
                        style={styles.button}
                        hoverColor={Colors.menu.iconHover}
                    />
                </ToolTip>
                <Popover
                    style={{
                        minWidth: 50,
                        height: this.props.maxHeight <= 190 ? this.props.maxHeight : 190
                    }}
                    open={this.state.open}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{horizontal: 'middle', vertical: 'top'}}
                    targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    onRequestClose={this.handleRequestClose}
                >
                    <div style={{
                        margin: '10px'
                    }}>
                        {this.props.columns.map(col =>
                            <Checkbox
                                key={col.id}
                                label={col.title}
                                style={styles.checkbox}
                                checked={col.visible}
                                labelStyle={styles.label}
                                onCheck={(event, checked) => { this.props.onChangeVisibility(col.id, checked) }}
                            />
                        )}
                    </div>
                </Popover>
            </div>
        );
    }

    static propTypes = {
        columns: PropTypes.array.isRequired,
        maxHeight: PropTypes.number,
        tipPlacement: PropTypes.string,
        onChangeVisibility: PropTypes.func.isRequired
    };

    static defaultProps = {
        tipPlacement: 'right'
    }
}