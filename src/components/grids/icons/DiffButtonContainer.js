import {connect} from 'react-redux';

import DiffButton from './DiffButton';
import {DetailsActions} from '../../../actions';


const mapStateToProps = (state, ownProps) => {
    const selected = state.grids[ownProps.collection].selected.map(item => item['@id']);
    return {
        selected,
        disabled: !(selected && selected.length > 1)
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onClick: ({view, selected}) => {
            dispatch(DetailsActions.requestOpenDetails('DIFF', {
                view: ownProps.collection,
                selected
            }))
        },
    }
};

const DiffButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DiffButton);

export default DiffButtonContainer;