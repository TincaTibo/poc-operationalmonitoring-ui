import React, { PropTypes } from 'react';
import Button from 'material-ui/FlatButton';
import Icon from 'material-ui/svg-icons/file/file-download';
import ToolTip from 'rc-tooltip';

import Colors from '../../../config/colors';
import Base64 from "js-base64";

const styles={
    button: {
        width: 20,
        height: 20,
        minWidth: 20,
        padding: 0,
        margin: 3,
        lineHeight: '20px'
    },
};

const DownloadSelected = (props) =>(
    <ToolTip
        placement={props.tipPlacement}
        prefixCls={'rc-tooltip-dark'}
        overlay={'Download selected'}
        mouseEnterDelay={0.5}
        destroyTooltipOnHide={true}
    >
        <Button
            onTouchTap={() => download(props.selected)}
            icon={<Icon
                color= {props.disabled ? Colors.menu.iconInactive : Colors.menu.iconActive}
                viewBox="0 3 24 21"
                style={{
                    width: 18,
                    height: 18,
                }}
            />}
            style={{
                ...styles.button,
                cursor: props.disabled ? 'not-allowed' : 'pointer'
            }}
            hoverColor={Colors.menu.iconHover}
            disabled={props.disabled}
        />
    </ToolTip>
);

function download(selected){
    const payload = Base64.Base64.encode(JSON.stringify(
        {
            '@type': 'sp:partialCollectionView',
            'items': selected
        },
        null, 2));

    const title = `${selected[0]['@type'] ? selected[0]['@type']:'UnTyped'}-${selected.length} records`;

    const link = document.createElement('a');
    link.download = title + '.json-ld';
    link.href = 'data:application/ld+json;base64,' + payload;
    link.click();
}

DownloadSelected.propTypes = {
    tipPlacement: PropTypes.string,
    selected: PropTypes.array,
    disabled: PropTypes.bool,
    view: PropTypes.string
};

DownloadSelected.defaultProps = {
    tipPlacement: 'right'
};

export default DownloadSelected;
