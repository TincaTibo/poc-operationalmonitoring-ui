import {connect} from 'react-redux';

import SelectColumnsButton from './SelectColumnsButton';
import {GridsActions} from '../../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        maxHeight: 150,
        columns: ownProps.columns
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onChangeVisibility: (column, visible) => {
            dispatch(GridsActions.toggleColumnVisibility(ownProps.collection, column, visible));
        },
    }
};

const SelectColumnsButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SelectColumnsButton);

export default SelectColumnsButtonContainer;