import React, { PropTypes } from 'react';
import Button from 'material-ui/FlatButton';
import Icon from 'material-ui/svg-icons/action/pageview';
import ToolTip from 'rc-tooltip';

import Colors from '../../../config/colors';

const styles={
    button: {
        width: 20,
        height: 20,
        minWidth: 20,
        padding: 0,
        margin: 3,
        lineHeight: '20px'
    },
};

const ViewSelectionButton = (props) =>(
    <ToolTip
        placement={props.tipPlacement}
        prefixCls={'rc-tooltip-dark'}
        overlay={'Show selection'}
        mouseEnterDelay={0.5}
        destroyTooltipOnHide={true}
    >
        {renderButton(props)}
    </ToolTip>
);

function renderButton(props){
    return (
        <Button
            onTouchTap={() => props.onClick(props.selected)}
            icon={<Icon
                color= {props.disabled ? Colors.menu.iconInactive : Colors.menu.iconActive}
                viewBox="0 3 24 21"
                style={{
                    width: 18,
                    height: 18,
                }}
            />}
            style={{
                ...styles.button,
                cursor: props.disabled ? 'not-allowed' : 'pointer'
            }}
            hoverColor={Colors.menu.iconHover}
            disabled={props.disabled}
        />
    );
}

ViewSelectionButton.propTypes = {
    tipPlacement: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    selected: PropTypes.array
};

ViewSelectionButton.defaultProps = {
    tipPlacement: 'right'
};

export default ViewSelectionButton;
