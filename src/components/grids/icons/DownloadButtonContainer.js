import {connect} from 'react-redux';

import DownloadButton from './DownloadButton';


const mapStateToProps = (state, ownProps) => {
    const selected = state.grids[ownProps.collection].selected;
    return {
        selected,
        disabled: !(selected && selected.length > 1)
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
};

const DownloadButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DownloadButton);

export default DownloadButtonContainer;