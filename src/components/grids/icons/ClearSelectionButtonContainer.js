import {connect} from 'react-redux';

import ClearSelectionButton from './ClearSelectionButton';
import {GridsActions} from '../../../actions';

const mapStateToProps = (state, ownProps) => {
    const selected = state.grids[ownProps.collection].selected;
    return {
        disabled: !(selected && selected.length)
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onClick: (view) => {
            dispatch(GridsActions.clearSelection({
                collection: ownProps.collection
            }))
        },
    }
};

const ClearSelectionButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ClearSelectionButton);

export default ClearSelectionButtonContainer;