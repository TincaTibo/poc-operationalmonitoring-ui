import React, { Component, PropTypes } from 'react';
import {Table, TableHeader, TableHeaderColumn, TableRow} from 'material-ui/Table';
import _ from 'lodash';
import {select, drag, event} from 'd3';

import ColumnSort from './ColumnSort';

const headerHeight = 20;

const styles = {
    headerRow: {
        height: '20px',
        paddingTop: 0,
        paddingBottom: 0,
        backgroundColor: '#f3f5f6',
        paddingLeft: 5,
        paddingRight: 5,
        border: undefined,
    },
    scrollCell: {
        width: 15,
        paddingLeft: 0,
        paddingRight: 0,
        height: headerHeight,
        userSelect: 'none'
    },
    defaultHeader: {
        marginLeft: 0,
        marginRight: 0,
        paddingLeft: 0,
        paddingRight: 0,
        height: headerHeight,
        verticalAlign: 'middle',
    },
    header:{
        cell:{
            height: '100%',
            display: 'flex',
            flexDirection: 'row',
            paddingLeft: 0,
            paddingRight: 0,
            borderRight: '1px solid transparent',
            borderLeft: '1px solid transparent',
        },
        colResizeLeft:{
            display: 'table-cell',
            flexShrink: 1,
            width: 5,
            height: headerHeight,
            cursor: 'col-resize'
        },
        colNoResizeLeft:{
            display: 'table-cell',
            flexShrink: 1,
            width: 5,
            height: headerHeight,
        },
        title:{
            display: 'table-cell',
            flexGrow: 1,
            height: 14,
            paddingTop: 3,
            textAlign: 'inherit',
            userSelect: 'none'
        },
        colResizeRight:{
            display: 'table-cell',
            flexShrink: 1,
            width: 5,
            height: headerHeight,
            cursor: 'col-resize'
        },
        colNoResizeRight:{
            display: 'table-cell',
            flexShrink: 1,
            width: 5,
            height: headerHeight,
        },
        colSort:{
            display: 'table-cell',
            flexShrink: 1,
            float: 'right',
            height: 14,
            paddingTop: 4
        }
    }
};

export default class GridHeader extends Component {

    constructor(props){
        super(props);

        this.columnElements = {};
        this.selected = null;
        this.newColumn = false;
        this.resizeColumnElements = {
            left: {},
            right: {}
        };
        this.state = {
            over: ''
        };
    }

    componentDidMount(){
        this.setResizeBehaviors();
    }

    componentWillReceiveProps(nextProps){
        //back to top if new table
        if(_.countBy(nextProps.columns, 'visible')['true'] !== _.countBy(this.props.columns, 'visible')['true']){ //change in columns, assign drag to new columns
            this.columnElements = {};
            this.resizeColumnElements = {
                left: {},
                right: {}
            };
            this.newColumn = true;
        }
    }

    render() {
        return (
            <Table fixedHeader={true}
                   showCheckboxes={false}
                   wrapperStyle={{
                       overflow: 'visible'
                   }}
            >
                <TableHeader adjustForCheckbox={false}
                             displaySelectAll={false}
                             style={{
                                 border: undefined,
                             }}
                >
                    <TableRow style={styles.headerRow}>
                        {this.props.columns.map(col => {
                            if(col.visible){
                                return <TableHeaderColumn key={col.title}
                                                          style={_.merge({}, styles.defaultHeader, col.style)}
                                >
                                    <div style={styles.header.cell}
                                         onMouseOver={this.headerMouseOver(col)}
                                         onMouseOut={this.headerMouseOut(col)}
                                         ref={this.associateColumnElement(col)}
                                    >
                                        <div style={styles.header.colNoResizeLeft}/>
                                        <div style={styles.header.title}>
                                            {col.title}
                                        </div>
                                        {col.sortKey && this.props.onChangeSort && this.props.sort  && !this.props.local &&
                                            <ColumnSort sortKey={col.sortKey}
                                                        sort={this.props.sort}
                                                        style={styles.header.colSort}
                                                        onChangeSort={this.props.onChangeSort}
                                            />
                                        }
                                        {col.resizable && !this.props.local ?
                                            <div style={styles.header.colResizeRight}
                                                 ref={this.associateResizeColumnElement('right', col)}
                                            /> :
                                            <div style={styles.header.colNoResizeRight}/>
                                        }
                                    </div>
                                </TableHeaderColumn>
                            }
                        })}
                        <TableHeaderColumn style={styles.scrollCell}>&nbsp;</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
            </Table>
        );
    }

    componentDidUpdate() {
        //new column was added, add drag listeners
        if(this.newColumn){
            this.newColumn = false;
            this.setResizeBehaviors();
        }
    }

    setResizeBehaviors(){
        Object.keys(this.resizeColumnElements.left).map(id => {
            const selection = select(this.resizeColumnElements.left[id]);
            if(!selection.empty() && !selection.on('drag')){
                selection.call(drag()
                    .container(document.getElementById('root'))
                    .on('drag', () => {
                        this.props.onResize(id, - event.dx);
                    })
                    .on('end', () => {
                        this.props.onEndResize()
                    })
                )
            }
        });

        Object.keys(this.resizeColumnElements.right).map(id => {
            const selection = select(this.resizeColumnElements.right[id]);
            if(!selection.empty() && !selection.on('drag')){
                selection.call(drag()
                    .container(document.getElementById('root'))
                    .on('drag', () => {
                        this.props.onResize(id, event.dx);
                    })
                    .on('end', () => {
                        this.props.onEndResize();
                    })
                )
            }
        });
    }

    associateColumnElement = (col) => {
        return value => {
            this.columnElements[col.id] = value;
        }
    };

    associateResizeColumnElement = (side,col) => {
        return value => {
            this.resizeColumnElements[side][col.id] = value;
        }
    };

    headerMouseOver = (col) => {
        if(col.resizable){
            return (event) => {
                this.columnElements[col.id].style.borderRight = '1px solid darkGrey';
                this.columnElements[col.id].style.borderLeft = '1px solid darkGrey';
                this.setState({over: col.title});
            }
        }
        else{
            return () => {
                this.setState({over: col.title});
            }
        }
    };

    headerMouseOut = (col) => {
        if(col.resizable){
            return (event) => {
                this.columnElements[col.id].style.borderRight = '1px solid transparent';
                this.columnElements[col.id].style.borderLeft = '1px solid transparent';
                this.setState({over: ''});
            }
        }
        else{
            return () => {
                this.setState({over: ''});
            }
        }
    };

    static propTypes = {
        columns: PropTypes.array.isRequired,
        local: PropTypes.bool,
        sort: PropTypes.shape({
            key: PropTypes.string.isRequired,
            order: PropTypes.string.isRequired
        }),
        items: PropTypes.array.isRequired,

        onResize: PropTypes.func,
        onEndResize: PropTypes.func,
        onChangeSort: PropTypes.func
    };

    static defaultProps = {
        local: false
    }
}