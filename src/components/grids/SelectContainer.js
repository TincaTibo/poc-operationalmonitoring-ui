import {connect} from 'react-redux';
import _ from 'lodash';

import Select from './Select.jsx';
import {GridsActions} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        selected: (_.findIndex(state.grids[ownProps.collection].selected, {'@id' : ownProps.item['@id']}) > -1)
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onCheck: (view) => {
            dispatch(GridsActions.selectItem({
                collection: ownProps.collection,
                item: ownProps.item
            }));
        },
    }
};

const SelectContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Select);

export default SelectContainer;