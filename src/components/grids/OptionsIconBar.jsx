import React, { PropTypes } from 'react';
import Divider from 'material-ui/Divider';

import SelectColumnsButtonContainer from './icons/SelectColumnsButtonContainer';
import ViewSelectionButtonContainer from './icons/ViewSelectionButtonContainer';
import ClearSelectionButtonContainer from './icons/ClearSelectionButtonContainer';
import DiffButtonContainer from './icons/DiffButtonContainer';
import DownloadButtonContainer from './icons/DownloadButtonContainer';

const styles = {
    bar:{
        width: 26,
        display: 'block',
        position: 'absolute',
        left: 0,
        backgroundColor: '#ffffff',
        borderRadius: 5
    }
};

const OptionsIconBar = (props) => (
    <div style={{
        ...styles.bar,
        ...props.style
    }}>
        <SelectColumnsButtonContainer
            columns={props.columns}
            collection={props.collection}
        />
        <Divider/>
        <ViewSelectionButtonContainer
            collection={props.collection}
        />
        <ClearSelectionButtonContainer
            collection={props.collection}
        />
        <Divider/>
        <DiffButtonContainer
            collection={props.collection}
        />
        <DownloadButtonContainer
            collection={props.collection}
        />
    </div>
);

OptionsIconBar.propTypes = {
    style: PropTypes.object,
    shouldDisplayDiff: PropTypes.bool,
};

export default OptionsIconBar;