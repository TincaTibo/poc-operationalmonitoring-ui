import {connect} from 'react-redux';
import _ from 'lodash';

import Select from './Select.jsx';
import {GridsActions} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        view: state.main.view,
        selected:
            ownProps.items.length ?
            _.differenceBy(
                ownProps.items, state.grids[ownProps.collection].selected, '@id'
            ).length === 0 : false,
        disabled: !ownProps.items.length,
        style: ownProps.style
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onCheck: (view) => {
            dispatch(GridsActions.selectItems({
                collection: ownProps.collection,
                items: ownProps.items
            }));
        },
    }
};

const SelectContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Select);

export default SelectContainer;