import React, {PropTypes, Component} from 'react';
import Button from 'material-ui/IconButton';
import SortAsc from 'material-ui/svg-icons/navigation/arrow-drop-up';
import SortDesc from 'material-ui/svg-icons/navigation/arrow-drop-down';

const styleWidth = 8, styleHeight = 6;

const styles = {
    icon: {
        width: styleWidth,
        height: styleHeight
    },
    buttonAsc: {
        width: styleWidth,
        height: styleHeight,
        padding: 0,
    },
    buttonDesc: {
        width: styleWidth,
        height: styleHeight,
        padding: 0,
    },
};

export default class ColumnSort extends Component{
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div style={_.merge({
                marginLeft: 3,
                marginRight: 3,
                width: 7
            }, this.props.style)}>
                <div style={{
                    display: 'flex',
                    flexDirection: 'column'
                }}>
                    <Button style={styles.buttonAsc}
                            iconStyle={styles.icon}
                            onTouchTap={() => {this.props.onChangeSort({ key: this.props.sortKey, order: 'asc'})}}
                    >
                        <SortAsc hoverColor={this.props.sortKey === this.props.sort.key && this.props.sort.order === 'asc' ? null : '#64C4E0'}
                                 viewBox="7 10 10 5"
                                 color={this.props.sortKey === this.props.sort.key && this.props.sort.order === 'asc' ? '#ff816b' : '#b4b4b4'}
                        />
                    </Button>
                    <Button style={styles.buttonDesc}
                            iconStyle={styles.icon}
                            onTouchTap={() => {this.props.onChangeSort({ key: this.props.sortKey, order: 'desc'})}}
                    >
                        <SortDesc hoverColor={this.props.sortKey === this.props.sort.key && this.props.sort.order === 'desc' ? null : '#64C4E0'}
                                  viewBox="7 10 10 5"
                                  color={this.props.sortKey === this.props.sort.key && this.props.sort.order === 'desc' ? '#ff816b' : '#b4b4b4'}
                        />
                    </Button>
                </div>
            </div>
        );
    }

    static propTypes = {
        sort: PropTypes.shape({
            key: PropTypes.string.isRequired,
            order: PropTypes.string.isRequired
        }).isRequired,
        sortKey: PropTypes.string.isRequired,
        style: PropTypes.object,
        onChangeSort: PropTypes.func.isRequired
    };
}

