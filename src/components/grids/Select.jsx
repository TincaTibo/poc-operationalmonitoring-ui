import React, {PropTypes} from 'react';
import Checkbox from 'material-ui/Checkbox';

import Colors from '../../config/colors';

const styles = {
    checkbox: {
        marginBottom: 0,
    },
    checkBoxIcon: {
        width: 17,
        height: 17,
        fill: Colors.medium
    }
};

const Select = props => (
    <Checkbox
        style={{
            ...styles.checkbox,
            ...props.style
        }}
        iconStyle={styles.checkBoxIcon}
        checked={props.selected}
        labelStyle={styles.label}
        onTouchTap={event => {
            props.onCheck(props.view);
            event.stopPropagation();
        }}
    />
);

Select.propTypes = {
    view: PropTypes.string,
    selected: PropTypes.bool,
    onCheck: PropTypes.func,
    disabled: PropTypes.bool,
    style: PropTypes.object
};

export default Select;