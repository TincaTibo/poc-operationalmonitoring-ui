import { connect } from 'react-redux';
import _ from 'lodash';

import Grid from '../../components/grids/Grid';
import {GridsActions, DetailsActions} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        height: ownProps.height,
        width: ownProps.width,
        columns: ownProps.columns,
        sort: state.grids[ownProps.resourceType].sort,
        items: state.grids[ownProps.resourceType].items,
        totalItems: state.grids[ownProps.resourceType].total,
        selected: state.details.open && state.details.type === ownProps.resourceType ? state.details.id : null,
        selectedItems: state.grids[ownProps.resourceType].selected.length,
        lastUpdated: state.grids[ownProps.resourceType].lastUpdated,
        shouldSearchForSelected: state.grids[ownProps.resourceType].shouldSearchForSelected,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onOpen: (id) => {
            dispatch(DetailsActions.requestOpenDetails(ownProps.resourceType, id));
        },
        onChangeSort: (sort) => {
            dispatch(GridsActions.sortGrid(ownProps.resourceType, sort));
        },
        onFetchRecords: () => {
            dispatch(GridsActions.requestUpdateGrid({collection: ownProps.resourceType}));
        },
        onFetchMore: () => {
            dispatch(GridsActions.requestNextPageCollection(ownProps.resourceType));
        },
        onResize: (column, dx) => {
            let columnIndex = _.findIndex(ownProps.columns, {id: column});
            let newWidth = ownProps.columns[columnIndex].style.width + dx;
            if (newWidth < ownProps.columns[columnIndex].style.minWidth) {
                return;
            }

            dispatch(GridsActions.resizeColumn(ownProps.resourceType, column, newWidth));
        },
        onEndResize: () => {
            dispatch(GridsActions.endResizeColumn());
        }
    }
};

const GridContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Grid);

export default GridContainer;