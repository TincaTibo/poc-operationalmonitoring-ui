import React, {PropTypes} from 'react';

import {formatNumber} from '../../config/locale';

const styles = {
    div: {
        display: 'inline-block',
        fontSize: '12px',
        fontFamily: 'Roboto, sans-serif',
        userSelect: 'none',
        position: 'relative',
        top: -3
    }
};

const Counter = (props) => (
    <div style={{
        ...styles.div,
        ...props.style
    }}>
        {props.total > 0 ?
            <span style={{color: 'grey'}}>
                {props.prefix}{formatNumber(props.current)} of {formatNumber(props.total)}{props.suffix}
            </span>
            : props.noRecordText
        }
        {props.selected > 0 &&
            <span>
                &nbsp;/&nbsp;
                <span style={{color: 'blue'}}>
                    {formatNumber(props.selected)} selected
                </span>
            </span>
        }
    </div>
);


Counter.propTypes = {
    total: PropTypes.number,
    current: PropTypes.number,
    selected: PropTypes.number,
    prefix: PropTypes.string,
    suffix: PropTypes.string,
    noRecordText: PropTypes.string,
    style: PropTypes.object,
};

Counter.defaultProps = {
    prefix: '',
    suffix: ' records',
    noRecordText: 'No records found',
};

export default Counter;