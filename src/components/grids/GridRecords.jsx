import React, { PureComponent, PropTypes } from 'react';
import {Table, TableBody, TableRow, TableRowColumn} from 'material-ui/Table';
import _ from 'lodash';

const rowHeight = 30;

const styles = {
    defaultCell: {
        paddingLeft: 0,
        paddingRight: 0,
        height: rowHeight,
    }
};

export default class GridRecords extends PureComponent {

    render(){
        return <Table showCheckboxes={false}
                      selectable={false}
                      wrapperStyle={{
                          border: undefined,
                          overflowY: 'hidden',
                          overflowX: 'hidden',
                          height: 'inherit',
                      }}
                      bodyStyle={{
                          overflowX: 'auto',
                          paddingLeft: 5,
                          paddingRight: 5,
                      }}
        >
            <TableBody displayRowCheckbox={false}
                       showRowHover={false}
                       ref={value => {
                           this.props.setScrollDiv(value);
                       }}
            >
                {this.props.items.map((item) =>
                    <TableRow key={item['@id']}
                              style={{
                                  height: rowHeight,
                                  cursor: this.props.onOpen ? 'pointer' : 'default',
                                  backgroundColor: this.props.selected === item['@id'] ?
                                      'lightGrey' : '#fff',
                                  border: undefined
                              }}
                              onTouchTap={() => {this.props.onOpen && this.props.onOpen(item['@id'])}}
                              ref={value => {
                                  if(this.props.selected === item['@id']){
                                      this.props.setSelected(value);
                                  }
                              }}
                    >
                        {this.props.columns.map(c => {
                            if(c.visible){
                                return <TableRowColumn key={c.title}
                                                       style={_.assign({}, styles.defaultCell, c.style)}
                                >
                                    <div style={{
                                        paddingLeft: 5,
                                        paddingRight: 5,
                                    }}
                                    >
                                    {c.render(item)}
                                    </div>
                                </TableRowColumn>
                            }
                        })}
                    </TableRow>
                )}
            </TableBody>
        </Table>
    }

    static propTypes = {
        columns: PropTypes.array.isRequired,
        items: PropTypes.array.isRequired,
        selected: PropTypes.string,

        onOpen: PropTypes.func.isRequired,
        setSelected: PropTypes.func.isRequired,
        setScrollDiv: PropTypes.func.isRequired,
    };
}