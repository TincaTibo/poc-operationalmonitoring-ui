import {connect} from 'react-redux';
import SettingsGlobalTab from './SettingsGlobalTab';
import {UpdateViewActions, WhispererActions, NetworkMapActions} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        dataRefresh: state.main.autoRefreshActive,
        dataRefreshRate: state.main.autoRefreshDelay,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onActiveDataRefresh: (bool) => {
            dispatch(UpdateViewActions.autoRefresh(bool));
        },
        onSetDataRefreshRate: (rate) => {
            dispatch(UpdateViewActions.setRefreshDelay(rate));
        },
    }
};

const SettingsGlobalTabContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingsGlobalTab);

export default SettingsGlobalTabContainer;