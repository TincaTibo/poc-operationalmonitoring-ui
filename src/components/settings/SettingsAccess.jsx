import React, {PropTypes} from 'react';
import SettingsIcon from 'material-ui/svg-icons/action/settings';
import ToolTip from 'rc-tooltip';
import '../tips/tipDark.css';
import Button from 'material-ui/FlatButton/index';

const styles={
    button: {
        width: 30,
        height: 30,
        minWidth: 30,
        padding: 0,
        margin: 5,
        lineHeight: '30px'
    },
};

const SettingsButton = (props) => (
    <ToolTip
        placement={'bottomLeft'}
        prefixCls={'rc-tooltip-dark'}
        overlay={'Settings'}
        mouseEnterDelay={0.5}
        destroyTooltipOnHide={true}
    >
        <Button
            onTouchTap={props.onClick}
            icon={
                <SettingsIcon
                    color='#FFFFFF'
                    style={{
                        transform: props.refreshing ? 'rotate(360deg)' : undefined,
                        transition: props.refreshing ? '2s' : undefined,
                    }}
                />
            }
            style={styles.button}
            hoverColor="#DFDFDF"
        />
    </ToolTip>
);

SettingsButton.propTypes = {
    onClick: PropTypes.func.isRequired
};

export default SettingsButton;