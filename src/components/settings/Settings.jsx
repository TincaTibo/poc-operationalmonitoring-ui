import React, { Component, PropTypes } from 'react';

import Layout from '../details/ResourceWithTabsContainer';
import SettingsGlobalTabContainer from './SettingsGlobalTabContainer';

const styles = {
};

export default class Settings extends Component {

    renderTitle = () => {
        return 'Main settings';
    };

    render() {
        return <Layout title={this.renderTitle}
                       propsToWait={[]}
                       activeTab={this.props.activeTab}
                       onChangeActiveTab={this.props.onChangeTab}
        >
            <SettingsGlobalTabContainer
                tabTitle="Global"
            />
        </Layout>
    }

    static propTypes = {
        activeTab: PropTypes.number,
        onChangeTab: PropTypes.func.isRequired,
    };
}
