import React, { PureComponent, PropTypes } from 'react';
import Subheader from 'material-ui/Subheader';

import SelectField from '../editableFields/SelectField';
import BoolField from '../editableFields/BoolField';

export default class SettingsGlobalTab extends PureComponent {

    constructor(props){
        super(props);
        this.state = {
        }
    }

    componentWillReceiveProps(nextProps){

    }

    render() {
        return <div style={{
            width: '100%',
            height: '100%',
            overflowY : 'auto'
        }}>
            {this.renderRefreshSettings()}
        </div>
    }

    renderRefreshSettings(){
        return (
            <div>
                <Subheader>Automatic refresh of new data</Subheader>
                <div style={{paddingLeft: '16px'}}>
                    <BoolField
                        header = 'Active'
                        ownValue = {this.props.dataRefresh}
                        onSelect = { bool => {
                            this.props.onActiveDataRefresh(bool);
                        }}
                        editable
                        editMode = {true}
                    />
                    <SelectField
                        header = 'Refresh rate'
                        newValue = {this.props.dataRefreshRate.toString()}
                        options = {['10', '20', '30', '45', '60', '300']}
                        optionsLabels = {[10, 20, 30, 45, 60, 300].map(d => {
                            if(d < 60){
                                return `${d} seconds`
                            }
                            else{
                                return `${d/60} minute${d/60>1 ? 's':''}`
                            }
                        })}
                        onSelect = { option => {
                            this.props.onSetDataRefreshRate(parseInt(option));
                        }}
                        editable
                        editMode = {true}
                    />
                </div>
            </div>
        )
    }

    static propTypes = {
        dataRefresh: PropTypes.bool,
        dataRefreshRate: PropTypes.number,
        onActiveDataRefresh: PropTypes.func,
        onSetDataRefreshRate: PropTypes.func,
    };
}
