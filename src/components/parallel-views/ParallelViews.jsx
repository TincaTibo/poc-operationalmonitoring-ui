import React, { PropTypes } from 'react';
import Button from "material-ui/FlatButton/index";

const styles = {
    bar: {
        width: '100%',
        height: 50,
        borderBottom: '1px solid #d1d8de'
    },
    colors: {
        active: '#2d95e0',
        inactive: '#083454',
        border: '#d1d8de'
    },
    view: {
        display: 'inline-block',
        marginLeft: 20
    },
    label: {
        display: 'inline-block',
        lineHeight: '14px',
        fontSize: '14px',
        fontFamily: 'Roboto, sans-serif',
        marginLeft: 5
    }
};

const ParallelViews = (props) => {

    return (
        <div
            style={{
                ...styles.bar,
                ...props.style
            }}
        >
            {props.viewList.map(view => (
                <div
                    key={view.code}
                    style={styles.view}
                >
                    <div
                        style={{
                            transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                            color: props.active === view.code ? styles.colors.active : styles.colors.inactive,
                            cursor: props.active === view.code ? 'default' : 'pointer',
                            marginLeft: 5,
                            marginRight: 5,
                            marginBottom: 5
                        }}
                        onClick={() => { props.onSwitchView({activeView: props.active, nextView: view.code}) }}
                    >
                        {view.icon &&
                            <view.icon
                                style={{
                                    width: 18,
                                    height: 18,
                                    position: 'relative',
                                    top: 3
                                }}
                                color={props.active === view.code ? styles.colors.active : styles.colors.inactive}
                            />
                        }
                        <div style={styles.label}>
                            {view.label}
                        </div>
                    </div>
                    <hr
                        style={{
                            borderTop: 'none',
                            borderLeft: 'none',
                            borderRight: 'none',
                            borderBottom: `2px solid ${styles.colors.active}`,
                            margin: 0,
                            position: 'relative',
                            top: 2,
                            width: '100%',
                            transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                            transform: props.active === view.code ? 'scaleX(1)' : 'scaleX(0)'
                        }}
                    />
                </div>
            ))}
        </div>
    );

};

ParallelViews.propTypes = {
    viewList: PropTypes.array,
    active: PropTypes.string,
    style: PropTypes.object,
    onSwitchView: PropTypes.func.isRequired
};

export default ParallelViews;