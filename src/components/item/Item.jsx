import React, { Component, PropTypes } from 'react';
import 'react-tab-panel/index.css';

import Layout from '../details/ResourceWithTabsContainer';
import JsonTab from '../details/JsonTab';

export default class Item extends Component {

    renderTitle = () => {
        return `${(this.props.item.name && this.props.item.name['x-default']) || this.props.item.alternateName}`;
    };

    render() {
        return (
            <Layout
                title={this.renderTitle}
                propsToWait={[this.props.item]}
                activeTab={this.props.activeTab}
                onChangeActiveTab={this.props.onChangeTab}
            >
                <JsonTab
                    tabTitle='Source'
                    object={this.props.item}
                    width={this.props.width}
                />
            </Layout>
        )
    }

    static propTypes = {
        item: PropTypes.object,
        activeTab: PropTypes.number,
        width: PropTypes.number,

        onChangeTab: PropTypes.func.isRequired,
    };

    static defaultProps = {
    };
}