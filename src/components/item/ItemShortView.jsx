import React, {Component, PropTypes} from 'react';

const styles = {
    main:{
        display: 'inline-block',
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,
        height: 13,
        fontSize: 13,
        textAlign: 'left',
        whiteSpace: 'nowrap',
        color: '#000',
        textOverflow: 'ellipsis',
        cursor: 'pointer',
        fontFamily: 'Roboto, sans-serif',
    },
    hover:{
        textDecoration: 'underline',
        textDecorationColor: 'lightGrey',
    },
};

export default class ItemShortView extends Component {

    constructor(props){
        super(props);
        this.state = {
            hover: false,
        };
    }

    toggleHover = () => {
        this.setState({hover: !this.state.hover});
    };

    componentDidMount(){
        if(!this.props.whispererItem){
            this.props.onLoad(this.props.item)
        }
    }

    render(){
        const Component = this.props.node;
        return (
            <Component
                style={{...styles.main, ...(this.state.hover ? styles.hover : {}), ...this.props.style}}
                onMouseEnter={this.toggleHover}
                onMouseLeave={this.toggleHover}
                onTouchTap={(event) => {
                    event.preventDefault();
                    event.stopPropagation();

                    if(this.props.itemItem){
                        this.props.onOpen(this.props.item)
                    }
                }}
            >
                {this.props.itemItem ?
                    this.props.itemItem.name ?
                        this.props.itemItem.name['x-default']
                        : this.props.itemItem.alternateName
                    : this.props.item
                }
            </Component>
        );
    }

    static propTypes = {
        style: PropTypes.object,
        itemItem: PropTypes.object,
        item: PropTypes.string.isRequired,
        node: PropTypes.string,

        onOpen: PropTypes.func.isRequired,
        onLoad: PropTypes.func.isRequired
    };

    static defaultProps = {
        node: 'div'
    };
}