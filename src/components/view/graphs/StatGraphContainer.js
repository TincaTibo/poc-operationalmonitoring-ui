import {connect} from 'react-redux';

import StatGraph from './StatGraph';
import {CommonViewActions} from '../../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        start: state.time.timeSpan.start,
        stop: state.time.timeSpan.stop,
        width: ownProps.width || 600,
        height: ownProps.height || 250,
        data: state.view[ownProps.view][ownProps.chart] && state.view[ownProps.view][ownProps.chart].data,
        loadingStatus: state.view[ownProps.view][ownProps.chart] && state.view[ownProps.view][ownProps.chart].loadingStatus,
        title: ownProps.title,
        definition: ownProps.definition,
        legendLayout: ownProps.legendLayout,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onLoadData: () => {
            dispatch(CommonViewActions.requestLoadChart({view: ownProps.view, chart: ownProps.chart}));
        }
    }
};

const StatGraphContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(StatGraph);

export default StatGraphContainer;