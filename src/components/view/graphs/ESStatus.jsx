import React, { Component, PropTypes } from 'react';
import LinearProgress from 'material-ui/LinearProgress';

import ServerGauge from './ServerGauge';

import Colors from '../../../config/colors';

const marginLeft = 3;
const marginRight = 5;
const marginTop = 0;
const marginBottom = 0;
const width = 20;

const styles = {
};

export default class ESStatus extends Component{
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.getData(this.props);
    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.start.isSame(this.props.start) || !nextProps.stop.isSame(this.props.stop)){
            this.getData(nextProps);
        }

        if(nextProps.data !== this.props.data){
            this.setState({
                data: nextProps.data
            });
        }
    }

    getData(props){
        props.onLoadData();
    }

    render() {
        return  (
            <div
                style={{
                    display: 'inline-block',
                    ...this.props.style,
                }}
            >
                {this.props.data && this.renderCpuGauges()}
                {this.props.data && this.renderHeapGauges()}
                {this.props.data && this.renderDiskGauges()}
                {
                    this.props.loadingStatus === 'PENDING' &&
                    this.renderPending()
                }
                {
                    this.props.loadingStatus === 'ERROR' &&
                    this.renderError()
                }
            </div>
        )
    }

    renderPending(){
        return (
            <div style={{
                position: 'relative',
                bottom: marginBottom,
                height: 0,
                left: marginLeft,
                width: `calc(100% - ${marginLeft} - ${marginRight})`,
            }}>
                <LinearProgress mode="indeterminate" />
            </div>
        );
    }

    renderError(){
        return (
            <div style={{
                position: 'relative',
                bottom: marginBottom + 18,
                height: 0,
                left: marginLeft + 5,
                width: `calc(100% - ${marginLeft} - ${marginRight})`,
                color: Colors.error,
                fontSize: '13px',
                fontFamily: 'Roboto, sans-serif',
            }}>
                Error while fetching data!
            </div>
        )
    }

    renderCpuGauges(){
        return (
            <div>
                {Object.getOwnPropertyNames(this.props.data).sort().map(node => (
                    <span
                        key={node}
                    >
                        <ServerGauge
                            node = {node}
                            stats = {this.props.data[node].cpu}
                            total = {1}
                            legend = ''
                            formatToolTip = '.0%'
                            style={{
                                marginLeft: 3
                            }}
                        />
                    </span>
                ))}
            </div>
        );
    }

    renderHeapGauges(){
        return (
            <div>
                {Object.getOwnPropertyNames(this.props.data).sort().map(node => (
                    <span
                        key={node}
                    >
                        <ServerGauge
                            node = {node}
                            stats = {this.props.data[node].heap}
                            total = {this.props.data[node].heapTotal}
                            legend = 'MB'
                            formatToolTip = ',d'
                            style={{
                                marginLeft: 3
                            }}
                        />
                    </span>
                ))}
            </div>
        );
    }

    renderDiskGauges(){
        return (
            <div>
                {Object.getOwnPropertyNames(this.props.data).sort().map(node => (
                    <span
                        key={node}
                    >
                        <ServerGauge
                            node = {node}
                            stats = {this.props.data[node].disk}
                            total = {this.props.data[node].diskTotal}
                            legend = 'MB'
                            formatToolTip = ',d'
                            style={{
                                marginLeft: 3
                            }}
                        />
                    </span>
                ))}
            </div>
        );
    }

    static propTypes = {
        start: PropTypes.object,
        stop: PropTypes.object,
        chart: PropTypes.string,
        data: PropTypes.object,
        loadingStatus: PropTypes.string,
        onLoadData: PropTypes.func,
    };

    static defaultProps = {
    }
}