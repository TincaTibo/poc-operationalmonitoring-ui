import {connect} from 'react-redux';

import BarGraph from './BarGraph';
import {CommonViewActions, TimeActions, ViewActions} from '../../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        start: state.time.timeSpan.start,
        stop: state.time.timeSpan.stop,
        width: ownProps.width || 600,
        height: ownProps.height || 250,
        data: state.view[ownProps.view][ownProps.chart] && state.view[ownProps.view][ownProps.chart].data,
        loadingStatus: state.view[ownProps.view][ownProps.chart] && state.view[ownProps.view][ownProps.chart].loadingStatus,
        title: ownProps.title,
        layout: ownProps.layout,
        definition: ownProps.definition,
        legendLayout: ownProps.legendLayout,
        enrichLegendWithStats: ownProps.enrichLegendWithStats,
        barCategoryGap: ownProps.barCategoryGap,
        barGap: ownProps.barGap,
        stacked: ownProps.stacked,
        startZoom: state.view.zoom.start,
        stopZoom: state.view.zoom.stop,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onLoadData: ({interval}) => {
            dispatch(CommonViewActions.requestLoadChart({view: ownProps.view, chart: ownProps.chart, interval}));
        },
        onStartZoom: ({time}) => {
            dispatch(ViewActions.setStartZoom({time}));
        },
        onStopZoom: ({time}) => {
            dispatch(ViewActions.setStopZoom({time}));
        },
        onZoom: ({start, stop}) => {
            dispatch(ViewActions.setStartZoom({time: null}));
            dispatch(ViewActions.setStopZoom({time: null}));
            dispatch(TimeActions.setCustomRange(start, stop));
        }
    }
};

const BarGraphContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(BarGraph);

export default BarGraphContainer;