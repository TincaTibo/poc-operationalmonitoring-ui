import React, { Component, PropTypes } from 'react';
import {BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar, Label, Text, ReferenceArea} from 'recharts';
import moment from 'moment';
import {scaleTime} from 'd3';
import _ from 'lodash';
import LinearProgress from 'material-ui/LinearProgress';

import '../../tips/tipDark.css';

import {formatNumber, multiFormat, SHORT_TIME_FORMAT_SECOND} from '../../../config/locale';
import Colors from '../../../config/colors';

const marginLeft = 0;
const marginRight = 0;
const marginTop = 0;
const marginBottom = 0;
const spaceBetweenTicks = 70;
const pointsBetweenTicks = 4;

const styles = {
    legend: {
        fontSize: 13,
        fontFamily: 'Roboto, sans-serif',
        color: Colors.mediumDark
    },
    title: {
        fontSize: 15,
        fontFamily: 'Roboto, sans-serif',
        fill: Colors.medium
    },
    tooltip:{
        label:{
            fontFamily: 'Roboto, sans-serif',
            fontSize: 13.5,
            fontWeight: undefined,
            lineHeight: '14px',
            color: Colors.mediumDark,
            paddingBottom: 5,
        },
        item: {
            fontFamily: 'Roboto, sans-serif',
            fontSize: 13,
            lineHeight: '13px',
            paddingLeft: 5,
            paddingTop: 1,
        }
    }
};

export default class BarGraph extends Component{
    constructor(props){
        super(props);

        this.graphWidth = props.width - marginLeft - marginRight;
        this.graphHeight = props.height - marginTop - marginBottom;
        this.xAxis = scaleTime()
            .domain([this.props.start, this.props.stop])
            .range([0, this.graphWidth]);
        this.widthOfLastUpdate = null;

        this.state = {
            data: []
        }
    }

    componentWillMount(){
        this.getData(this.props);
    }

    componentWillReceiveProps(nextProps){
        //Change of width (big): update item
        if(nextProps.width !== this.props.width){
            this.getData(nextProps, this.widthOfLastUpdate && Math.abs(nextProps.width - this.widthOfLastUpdate) > 30);
        }

        if(!nextProps.start.isSame(this.props.start) || !nextProps.stop.isSame(this.props.stop)){
            this.getData(nextProps);
        }

        if(nextProps.data !== this.props.data){
            this.setState({
                data: nextProps.data
            });
        }
    }

    getData(props, shouldReload = true){
        this.graphWidth = props.width - marginLeft - marginRight;
        const areaWidth = this.graphWidth - 130;

        if(props.start && props.stop){
            this.xAxis
                .domain([props.start, props.stop])
                .range([0, areaWidth]);

            this.ticks = this.xAxis.ticks(_.floor(areaWidth / spaceBetweenTicks));
            const intervalInMs = _.round(moment(this.ticks[1]).diff(moment(this.ticks[0]))/pointsBetweenTicks);
            const interval = Math.round(intervalInMs/(60*1e3)) || 1; // in Min
            if(shouldReload && interval){
                this.widthOfLastUpdate = props.width;
                props.onLoadData({interval});
            }
        }
    }

    render() {
        return  (
            <div
                style={{
                    display: 'inline-block',
                    backgroundColor: 'white',
                    borderRadius: 5,
                    marginRight: this.props.margin
                }}
            >
                <BarChart
                    width={this.graphWidth}
                    height={this.graphHeight}
                    data={this.state.data && this.state.data.items}
                    margin={{ top: 35, right: 10, left: 10, bottom: 5 }}
                    syncId="AppsViewSyncId"
                    barCategoryGap={this.props.barCategoryGap || 2}
                    barGap={this.props.barGap || 5}
                >
                    <text
                        x={10}
                        y={15}
                        textAnchor="left"
                        style={styles.title}
                    >
                        {this.props.title}
                    </text>
                    <CartesianGrid
                        strokeDasharray="3 3"
                    />
                    <XAxis
                        dataKey="time"
                        type="category"
                        domain={[this.props.start.toISOString(), this.props.stop.toISOString()]}
                        tickFormatter={time => multiFormat(moment(time))}
                        minTickGap={20}
                        fontSize={13}
                        fontFamily="Roboto, sans-serif"
                    />
                    {this.props.definition.yAxes && this.props.definition.yAxes.map(yAxis => (
                        <YAxis
                            key={yAxis.id}
                            yAxisId={yAxis.id}
                            orientation={yAxis.orientation}
                            tickFormatter={formatNumber}
                            fontSize={12}
                            fontFamily="Roboto, sans-serif"
                        >
                            <Label
                                angle={-90}
                                position={yAxis.orientation === 'left' ? 'insideLeft' : 'insideRight'}
                                content={CustomText}
                                fontSize={13}
                                fontFamily="Roboto, sans-serif"
                                fill={Colors.medium}
                            >
                                {yAxis.legend || yAxis.metrics.map(m => _.upperFirst(m.key)).join(',')}
                            </Label>
                        </YAxis>
                    ))}
                    <Tooltip
                        labelStyle={styles.tooltip.label}
                        itemStyle={styles.tooltip.item}
                        labelFormatter={time => `${moment(time).format(SHORT_TIME_FORMAT_SECOND)} -> ${moment(time).add(this.props.data.interval,'minutes').format(SHORT_TIME_FORMAT_SECOND)}`}
                        formatter={(value, name, props) => (formatNumber(value))}
                        wrapperStyle={{
                            maxHeight: 145,
                            overflow: 'hidden',
                            borderBottom: '1px solid lightGrey'
                        }}
                        itemSorter={this.props.tooltipItemSorter}
                    />
                    {!this.props.noLegend &&
                    <Legend
                        layout={this.props.legendLayout}
                        formatter={(value, entry, i) => this.enrichLegend(value, entry, i)}
                    />
                    }
                    {_.flatten(this.props.definition.yAxes.map(
                        y => y.metrics ?
                            y.metrics.map(
                                metric => ({
                                    dataKey: metric.key,
                                    description: metric.description,
                                    yAxisId: y.id,
                                    unit: y.unit
                                })
                            )
                            :
                            this.props.data ? this.props.data.metrics.map(
                                metric => ({
                                    dataKey: metric,
                                    description: metric,
                                    yAxisId: y.id,
                                    unit: y.unit
                                })
                            ) : []
                    )).map((metric,i) => (
                        <Bar
                            key={metric.dataKey}
                            name={metric.description}
                            dataKey={metric.dataKey}
                            yAxisId={metric.yAxisId}
                            fill={Colors.seriesColors[i % Colors.seriesColors.length]}
                            stackId={this.props.stacked ? 'stack-' + metric.yAxisId : undefined}
                            unit={metric.unit}
                            onClick={(data, index) => {console.log(data); console.log(index);}}
                        />
                    ))}
                    {
                        this.props.startZoom && this.props.stopZoom &&
                            <ReferenceArea
                                yAxisId={this.props.definition.yAxes[0].id}
                                x1={this.props.startZoom}
                                x2={this.props.stopZoom}
                                stroke="#00BCD4"
                                strokeOpacity={1}
                                fill="#00BCD4"
                                fillOpacity={0.2}
                            />
                    }
                </BarChart>
                {
                    this.props.loadingStatus === 'PENDING' &&
                    this.renderPending()
                }
                {
                    this.props.loadingStatus === 'ERROR' &&
                    this.renderError()
                }
            </div>
        )
    }

    renderPending(){
        return (
            <div style={{
                position: 'relative',
                bottom: marginBottom,
                height: 0,
                left: marginLeft,
                width: this.props.width - marginLeft - marginRight,
            }}>
                <LinearProgress mode="indeterminate" />
            </div>
        );
    }

    renderError(){
        return (
            <div style={{
                position: 'relative',
                bottom: marginBottom + 18,
                height: 0,
                left: marginLeft + 5,
                width: this.props.width - marginLeft - marginRight,
                color: Colors.error,
                fontSize: '13px',
                fontFamily: 'Roboto, sans-serif',
            }}>
                Error while fetching data!
            </div>
        )
    }

    enrichLegend(value, entry, i){
        return (
            <span style={styles.legend}>
                <span style={{display: 'table-cell', width: this.props.enrichLegendWithStats ? 60 : undefined}}>{_.upperFirst(value)}</span>
                {this.props.enrichLegendWithStats &&
                    <span>
                        Avg:
                        <span style={{display: 'table-cell', width:80,  textAlign: 'right', paddingRight: 10}}>
                            {this.state.data && this.state.data.stats && this.state.data.stats[value] ? formatNumber(this.state.data.stats[value].avg) : 0} /min
                            &nbsp;
                        </span>
                        Max:
                        <span style={{display: 'table-cell', width:80, textAlign: 'right'}}>
                            {this.state.data && this.state.data.stats && this.state.data.stats[value] ? formatNumber(this.state.data.stats[value].max) : 0} /min
                        </span>
                    </span>
                }
            </span>
        );
    }

    static propTypes = {
        start: PropTypes.object,
        stop: PropTypes.object,
        layout: PropTypes.string,
        title: PropTypes.string,
        definition: PropTypes.object,
        legendLayout: PropTypes.string,
        stacked: PropTypes.bool,
        enrichLegendWithStats: PropTypes.bool,
        noLegend: PropTypes.bool,
        barCategoryGap: PropTypes.any,
        barGap: PropTypes.any,
        data: PropTypes.shape({
            stats: PropTypes.object,
            items: PropTypes.array,
            interval: PropTypes.number
        }),
        tooltipItemSorter: PropTypes.func,
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        style: PropTypes.object,
        loadingStatus: PropTypes.string,
        startZoom: PropTypes.string,
        stopZoom: PropTypes.string,
        onLoadData: PropTypes.func,
        onStartZoom: PropTypes.func,
        onStopZoom: PropTypes.func,
        onZoom: PropTypes.func,
    };

    static defaultProps = {
        enrichLegendWithStats: false,
        stacked: false,
    }
}

const CustomText = (props) => {
    return (
        <Text
            {...props}
            {...getAttrsOfCartesianLabel(props)}
        >
            {props.children}
        </Text>
    );
};

const getAttrsOfCartesianLabel = (props) => {
    let viewBox = props.viewBox,
        offset = props.offset,
        position = props.position;
    let x = viewBox.x,
        y = viewBox.y,
        width = viewBox.width,
        height = viewBox.height;

    let sign = height >= 0 ? 1 : -1;

    if (position === 'top') {
        return {
            x: x + width / 2,
            y: y - sign * offset,
            textAnchor: 'middle',
            verticalAnchor: 'end'
        };
    }

    if (position === 'bottom') {
        return {
            x: x + width / 2,
            y: y + height + sign * offset,
            textAnchor: 'middle',
            verticalAnchor: 'start'
        };
    }

    if (position === 'left') {
        return {
            x: x - offset,
            y: y + height / 2,
            textAnchor: 'end',
            verticalAnchor: 'middle'
        };
    }

    if (position === 'right') {
        return {
            x: x + width + offset,
            y: y + height / 2,
            textAnchor: 'start',
            verticalAnchor: 'middle'
        };
    }

    if (position === 'insideLeft') {
        return {
            x: x + offset,
            y: y + height / 2,
            textAnchor: 'middle',
            verticalAnchor: 'middle'
        };
    }

    if (position === 'insideRight') {
        return {
            x: x + width - offset,
            y: y + height / 2,
            textAnchor: 'middle',
            verticalAnchor: 'middle'
        };
    }

    if (position === 'insideTop') {
        return {
            x: x + width / 2,
            y: y + sign * offset,
            textAnchor: 'middle',
            verticalAnchor: 'start'
        };
    }

    if (position === 'insideBottom') {
        return {
            x: x + width / 2,
            y: y + height - sign * offset,
            textAnchor: 'middle',
            verticalAnchor: 'end'
        };
    }

    if (position === 'insideTopLeft') {
        return {
            x: x + offset,
            y: y + sign * offset,
            textAnchor: 'start',
            verticalAnchor: 'start'
        };
    }

    if (position === 'insideTopRight') {
        return {
            x: x + width - offset,
            y: y + sign * offset,
            textAnchor: 'end',
            verticalAnchor: 'start'
        };
    }

    if (position === 'insideBottomLeft') {
        return {
            x: x + offset,
            y: y + height - sign * offset,
            textAnchor: 'start',
            verticalAnchor: 'end'
        };
    }

    if (position === 'insideBottomRight') {
        return {
            x: x + width - offset,
            y: y + height - sign * offset,
            textAnchor: 'end',
            verticalAnchor: 'end'
        };
    }

    return {
        x: x + width / 2,
        y: y + height / 2,
        textAnchor: 'middle',
        verticalAnchor: 'middle'
    };
};