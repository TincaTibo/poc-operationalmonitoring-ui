import React, { Component, PropTypes } from 'react';
import {ScatterChart, CartesianGrid, XAxis, YAxis, ZAxis, Tooltip, Legend, Scatter, Label, Text} from 'recharts';
import moment from 'moment';
import {scaleTime} from 'd3';
import _ from 'lodash';
import LinearProgress from 'material-ui/LinearProgress';

import '../../tips/tipDark.css';

import {formatNumber, multiFormat, SHORT_TIME_FORMAT_SECOND} from '../../../config/locale';
import Colors from '../../../config/colors';

const marginLeft = 0;
const marginRight = 0;
const marginTop = 0;
const marginBottom = 0;
const spaceBetweenTicks = 70;
const pointsBetweenTicks = 7;
const msByDays = 1000 * 24* 60 * 60;

const styles = {
    legend: {
        fontSize: 13,
        fontFamily: 'Roboto, sans-serif',
        color: Colors.mediumDark
    },
    title: {
        fontSize: 15,
        fontFamily: 'Roboto, sans-serif',
        fill: Colors.medium
    },
    tooltip:{
        label:{
            fontFamily: 'Roboto, sans-serif',
            fontSize: 13.5,
            fontWeight: undefined,
            lineHeight: '14px',
            color: Colors.mediumDark,
            paddingBottom: 5,
        },
        item: {
            fontFamily: 'Roboto, sans-serif',
            fontSize: 13,
            lineHeight: '13px',
            paddingLeft: 5,
            paddingTop: 1,
        }
    }
};

export default class ScatterGraph extends Component{
    constructor(props){
        super(props);

        this.graphWidth = props.width - marginLeft - marginRight;
        this.graphHeight = props.height - marginTop - marginBottom;
        this.xAxis = scaleTime()
            .domain([this.props.start, this.props.stop])
            .range([0, this.graphWidth]);
        this.widthOfLastUpdate = null;

        this.state = {
            data: []
        }
    }

    componentWillMount(){
        this.getData(this.props);
    }

    componentWillReceiveProps(nextProps){
        //Change of width (big): update item
        if(nextProps.width !== this.props.width){
            this.getData(nextProps, this.widthOfLastUpdate && Math.abs(nextProps.width - this.widthOfLastUpdate) > 30);
        }

        if(!nextProps.start.isSame(this.props.start) || !nextProps.stop.isSame(this.props.stop)){
            this.getData(nextProps);
        }

        if(nextProps.data !== this.props.data){
            this.setState({
                data: nextProps.data
            });
        }
    }

    getData(props, shouldReload = true){
        this.graphWidth = props.width - marginLeft - marginRight;
        const areaWidth = this.graphWidth - 130;

        if(props.start && props.stop){
            this.xAxis
                .domain([props.start, props.stop])
                .range([0, areaWidth]);

            this.ticks = this.xAxis.ticks(_.floor(areaWidth / spaceBetweenTicks));
            const intervalInMs = _.round(moment(this.ticks[1]).diff(moment(this.ticks[0]))/pointsBetweenTicks);
            const interval = Math.round(intervalInMs/(60*1e3)) || 1; // in Min
            if(shouldReload && interval){
                this.widthOfLastUpdate = props.width;
                props.onLoadData({interval});
            }
        }
    }

    render() {
        const yAxis = this.props.definition.yAxis;
        const zAxis = this.props.definition.zAxis;
        const days = _.uniq(this.state.data && this.state.data.items && this.state.data.items.map(i => i.time));
        const min = days[0] || 0;
        const max = days.length ? days[days.length-1] : 0;
        return  (
            <div
                style={{
                    display: 'inline-block'
                }}
            >
                <ScatterChart
                    width={this.graphWidth}
                    height={this.graphHeight}
                    margin={{ top: 35, right: 5, left: 5, bottom: 5 }}
                >
                    <text
                        x={10}
                        y={15}
                        textAnchor="left"
                        style={styles.title}
                    >
                        {this.props.title}
                    </text>
                    <CartesianGrid
                        strokeDasharray="3 3"
                    />
                    <XAxis
                        dataKey="time"
                        name="Date"
                        type="number"
                        padding={{ left: 10 , right: 10 }}
                        domain={[min, max]}
                        ticks={days.length < 10 ? days :  days.filter((day, index) => (index % (Math.round(days.length/5)) === 0))}
                        tickFormatter={time => moment(time).format('DD/MM')}
                        fontSize={13}
                        fontFamily="Roboto, sans-serif"
                    />
                    <YAxis
                        dataKey={yAxis.key}
                        name={yAxis.description}
                        unit={yAxis.unit}
                        fontSize={12}
                        fontFamily="Roboto, sans-serif"
                    />
                    <ZAxis
                        dataKey={zAxis.key}
                        name={zAxis.description}
                        unit={zAxis.unit}
                        range={[10,60]}
                        fontSize={12}
                        fontFamily="Roboto, sans-serif"
                    />
                    <Tooltip
                        itemStyle={styles.tooltip.item}
                        formatter={(value, name, props) => {
                            if(name === 'Date'){
                                return moment(value).format('DD/MM')
                            }
                            else{
                                return formatNumber(value)
                            }
                        }}
                        wrapperStyle={{
                            maxHeight: 145,
                            overflow: 'hidden',
                            borderBottom: '1px solid lightGrey'
                        }}
                        itemSorter={this.props.tooltipItemSorter}
                    />
                    <Scatter
                        data={this.state.data && this.state.data.items && this.state.data.items.filter(i => i[zAxis.key] > 0)}
                        fill={Colors.seriesColors[0]}
                    />
                </ScatterChart>
                {
                    this.props.loadingStatus === 'PENDING' &&
                    this.renderPending()
                }
                {
                    this.props.loadingStatus === 'ERROR' &&
                    this.renderError()
                }
            </div>
        )
    }

    renderPending(){
        return (
            <div style={{
                position: 'relative',
                bottom: marginBottom,
                height: 0,
                left: marginLeft,
                width: this.props.width - marginLeft - marginRight,
            }}>
                <LinearProgress mode="indeterminate" />
            </div>
        );
    }

    renderError(){
        return (
            <div style={{
                position: 'relative',
                bottom: marginBottom + 18,
                height: 0,
                left: marginLeft + 5,
                width: this.props.width - marginLeft - marginRight,
                color: Colors.error,
                fontSize: '13px',
                fontFamily: 'Roboto, sans-serif',
            }}>
                Error while fetching data!
            </div>
        )
    }

    enrichLegend(value, entry, i){
        return (
            <span style={styles.legend}>
                <span style={{display: 'table-cell', width: this.props.enrichLegendWithStats ? 60 : undefined}}>{_.upperFirst(value)}</span>
                {this.props.enrichLegendWithStats &&
                    <span>
                        Avg:
                        <span style={{display: 'table-cell', width:80,  textAlign: 'right', paddingRight: 10}}>
                            {this.state.data && this.state.data.stats && this.state.data.stats[value] ? formatNumber(this.state.data.stats[value].avg) : 0} /min
                            &nbsp;
                        </span>
                        Max:
                        <span style={{display: 'table-cell', width:80, textAlign: 'right'}}>
                            {this.state.data && this.state.data.stats && this.state.data.stats[value] ? formatNumber(this.state.data.stats[value].max) : 0} /min
                        </span>
                    </span>
                }
            </span>
        );
    }

    static propTypes = {
        start: PropTypes.object,
        stop: PropTypes.object,
        title: PropTypes.string,
        definition: PropTypes.object,
        legendLayout: PropTypes.string,
        enrichLegendWithStats: PropTypes.bool,
        noLegend: PropTypes.bool,
        barCategoryGap: PropTypes.any,
        barGap: PropTypes.any,
        data: PropTypes.shape({
            stats: PropTypes.object,
            items: PropTypes.array,
            interval: PropTypes.number
        }),
        tooltipItemSorter: PropTypes.func,
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        style: PropTypes.object,
        loadingStatus: PropTypes.string,
        onLoadData: PropTypes.func,
    };

    static defaultProps = {
        enrichLegendWithStats: false,
    }
}

const CustomText = (props) => {
    return (
        <Text
            {...props}
            {...getAttrsOfCartesianLabel(props)}
        >
            {props.children}
        </Text>
    );
};

const getAttrsOfCartesianLabel = (props) => {
    let viewBox = props.viewBox,
        offset = props.offset,
        position = props.position;
    let x = viewBox.x,
        y = viewBox.y,
        width = viewBox.width,
        height = viewBox.height;

    let sign = height >= 0 ? 1 : -1;

    if (position === 'top') {
        return {
            x: x + width / 2,
            y: y - sign * offset,
            textAnchor: 'middle',
            verticalAnchor: 'end'
        };
    }

    if (position === 'bottom') {
        return {
            x: x + width / 2,
            y: y + height + sign * offset,
            textAnchor: 'middle',
            verticalAnchor: 'start'
        };
    }

    if (position === 'left') {
        return {
            x: x - offset,
            y: y + height / 2,
            textAnchor: 'end',
            verticalAnchor: 'middle'
        };
    }

    if (position === 'right') {
        return {
            x: x + width + offset,
            y: y + height / 2,
            textAnchor: 'start',
            verticalAnchor: 'middle'
        };
    }

    if (position === 'insideLeft') {
        return {
            x: x + offset,
            y: y + height / 2,
            textAnchor: 'middle',
            verticalAnchor: 'middle'
        };
    }

    if (position === 'insideRight') {
        return {
            x: x + width - offset,
            y: y + height / 2,
            textAnchor: 'middle',
            verticalAnchor: 'middle'
        };
    }

    if (position === 'insideTop') {
        return {
            x: x + width / 2,
            y: y + sign * offset,
            textAnchor: 'middle',
            verticalAnchor: 'start'
        };
    }

    if (position === 'insideBottom') {
        return {
            x: x + width / 2,
            y: y + height - sign * offset,
            textAnchor: 'middle',
            verticalAnchor: 'end'
        };
    }

    if (position === 'insideTopLeft') {
        return {
            x: x + offset,
            y: y + sign * offset,
            textAnchor: 'start',
            verticalAnchor: 'start'
        };
    }

    if (position === 'insideTopRight') {
        return {
            x: x + width - offset,
            y: y + sign * offset,
            textAnchor: 'end',
            verticalAnchor: 'start'
        };
    }

    if (position === 'insideBottomLeft') {
        return {
            x: x + offset,
            y: y + height - sign * offset,
            textAnchor: 'start',
            verticalAnchor: 'end'
        };
    }

    if (position === 'insideBottomRight') {
        return {
            x: x + width - offset,
            y: y + height - sign * offset,
            textAnchor: 'end',
            verticalAnchor: 'end'
        };
    }

    return {
        x: x + width / 2,
        y: y + height / 2,
        textAnchor: 'middle',
        verticalAnchor: 'middle'
    };
};