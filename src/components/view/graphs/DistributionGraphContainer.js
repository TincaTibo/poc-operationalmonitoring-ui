import {connect} from 'react-redux';

import DistributionGraph from './DistributionGraph';
import {CommonViewActions} from '../../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        start: state.time.timeSpan.start,
        stop: state.time.timeSpan.stop,
        width: ownProps.width || 600,
        height: ownProps.height || 250,
        data: state.view[ownProps.view][ownProps.chart] && state.view[ownProps.view][ownProps.chart].data,
        loadingStatus: state.view[ownProps.view][ownProps.chart] && state.view[ownProps.view][ownProps.chart].loadingStatus,
        title: ownProps.title,
        definition: ownProps.definition,
        legendLayout: ownProps.legendLayout,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onLoadData: ({interval}) => {
            dispatch(CommonViewActions.requestLoadChart({view: ownProps.view, chart: ownProps.chart, interval: ownProps.interval}));
        }
    }
};

const DistributionGraphContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DistributionGraph);

export default DistributionGraphContainer;