import React, { Component, PropTypes } from 'react';
import {scaleLinear} from 'd3';
import LinearProgress from 'material-ui/LinearProgress';

import ToolTip from 'rc-tooltip';
import '../../tips/tipDark.css';

import {formatRaw} from '../../../config/locale';
import Colors from '../../../config/colors';

const marginLeft = 3;
const marginRight = 5;
const marginTop = 0;
const marginBottom = 0;
const axisHeight = 10;
const legendWidth = 90;

const styles = {
    legend: {
        fontSize: 13,
        fontFamily: 'Roboto, sans-serif',
        color: Colors.mediumDark,
        textAlign: 'right',
        textAnchor: 'end',
        userSelect: 'none'
    },
    tooltip:{
        label:{
            display:'inline-block',
            fontFamily: 'Roboto, sans-serif',
            fontSize: 13.5,
            lineHeight: '14px',
            color: 'white',
            width: 60,
            paddingBottom: 2
        },
        item: {
            display:'inline-block',
            fontFamily: 'Roboto, sans-serif',
            fontSize: 13,
            lineHeight: '13px',
            textAlign: 'right',
            width: 100
        }
    },
    axis: {
        axisStyle: {
            fill: 'none',
            stroke: Colors.medium,
            strokeWidth: 1,
            strokeOpacity: 1
        },
        arrowStyle: {
            fill: Colors.medium,
            fillOpacity: 1,
            stroke: Colors.medium,
            strokeWidth: 1,
            strokeOpacity: 1
        },
        limitMarkerStyle : {
            fill: 'none',
            stroke: Colors.medium,
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1
        },
        axisTextStyle:{
            fontSize: 8,
            fontFamily: 'Roboto, sans-serif',
            fill: Colors.medium,
            textAlign: 'center',
            textAnchor: 'middle',
            userSelect: 'none'
        }
    },
    gauge: {
        minMax: {
            fillOpacity:1,
            fill: '#5599ff',
            stroke: 'none',
            transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        },
        last: {
            fill: '#5599ff',
            stroke: '#003380',
            transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        },
        average: {
            fill: '#003380',
            stroke: 'none',
            strokeOpacity: 1,
            strokeWidth: 2,
            transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        }
    }
};

export default class GaugeGraph extends Component{
    constructor(props){
        super(props);

        this.graphWidth = props.width - marginLeft - marginRight;
        this.graphHeight = props.height - marginTop - marginBottom;

        this.state = {
            data: []
        };
        this.xAxis = null;
        this.ticks = null;
    }

    componentWillMount(){
        this.getData(this.props);
    }

    componentWillReceiveProps(nextProps){
        if(!nextProps.start.isSame(this.props.start) || !nextProps.stop.isSame(this.props.stop)){
            this.getData(nextProps);
        }

        if(nextProps.data !== this.props.data){
            this.setState({
                data: nextProps.data
            });
        }
    }

    getData(props){
        props.onLoadData();
    }

    render() {

        this.xAxis = this.props.data && scaleLinear()
            .domain([0, this.props.data.max])
            .range([0, this.graphWidth - legendWidth])
            .nice();

        const extraToolTipProps = {};
        if(!this.props.data){
            extraToolTipProps.visible = false;
        }

        return  (
            <div
                style={{
                    display: 'inline-block',
                    ...this.props.style,
                }}
            >
                <ToolTip
                    placement="bottom"
                    prefixCls={'rc-tooltip-dark'}
                    overlay={this.props.data ? this.renderToolTip() : ''}
                    trigger={['hover']}
                    mouseEnterDelay={0.75}
                    destroyTooltipOnHide={true}
                    {...extraToolTipProps}
                >
                    <svg
                        id={`gauge-${this.props.chart}`}
                        width={this.props.width}
                        height={this.props.height}
                    >
                        <g transform={`translate(${marginLeft},${marginTop})`}>
                            {this.props.data && this.renderGauge()}
                            {this.props.data && this.renderLegend()}
                            {this.renderVerticalAxis()}
                            {this.renderAxis()}
                        </g>
                    </svg>
                </ToolTip>
                {
                    this.props.loadingStatus === 'PENDING' &&
                    this.renderPending()
                }
                {
                    this.props.loadingStatus === 'ERROR' &&
                    this.renderError()
                }
            </div>
        )
    }

    renderPending(){
        return (
            <div style={{
                position: 'relative',
                bottom: marginBottom,
                height: 0,
                left: marginLeft,
                width: this.props.width - marginLeft - marginRight,
            }}>
                <LinearProgress mode="indeterminate" />
            </div>
        );
    }

    renderError(){
        return (
            <div style={{
                position: 'relative',
                bottom: marginBottom + 18,
                height: 0,
                left: marginLeft + 5,
                width: this.props.width - marginLeft - marginRight,
                color: Colors.error,
                fontSize: '13px',
                fontFamily: 'Roboto, sans-serif',
            }}>
                Error while fetching data!
            </div>
        )
    }

    renderAxis(){
        const marks = this.xAxis && this.xAxis.ticks(3);

        return <g id="timeLineAxis"
                  transform={`translate(0, ${this.props.height - marginBottom - axisHeight})`}
        >
            {/* Horizontal Axis */}
            <path style={styles.axis.axisStyle} d={`M 0,0 ${this.graphWidth - legendWidth},0`}/>
            <path style={styles.axis.arrowStyle} d={`m ${this.graphWidth - legendWidth},-3 3,3 -3,3 Z`}/>

            {/* Marks */}
            {
                marks && marks.map((m, index) => (
                        <g key={m.toString()}>
                            <path style={styles.axis.limitMarkerStyle} d='M 0,0 0,3'
                                  transform={`translate(${this.xAxis(m)},0)`}
                            />
                            <text y={10}
                                  x={0}
                                  style={styles.axis.axisTextStyle}
                                  transform={`translate(${this.xAxis(m)},0)`}>
                                {formatRaw(m, this.props.format)}
                            </text>
                        </g>
                    )
                )
            }
        </g>;
    }

    renderVerticalAxis(){
        const axisYPos = this.props.height - marginBottom - axisHeight;
        const maxHeight = this.props.height - marginTop - marginBottom - axisHeight;

        return (
            <g id="timeLineVerticalAxis"
               transform={`translate(0, ${axisYPos})`}
            >
                <path style={styles.axis.axisStyle} d={`M 0,0 0,${-maxHeight}`}/>
            </g>
        );
    }

    renderGauge(){
        const barHeight = this.props.height - marginTop - marginBottom - axisHeight;

        return (
            <g transform={`translate(0, ${this.props.height - marginBottom - axisHeight})`}>
                {/*Min-Max*/}
                <rect
                    style={{
                        ...styles.gauge.minMax,
                        fill: this.props.minMaxColor
                    }}
                    y={-barHeight+2}
                    x={this.xAxis(this.props.data.min)}
                    width={this.xAxis(this.props.data.max - this.props.data.min)}
                    height={barHeight-4}
                />
                {/*Average*/}
                <path
                    style={{
                        ...styles.gauge.average,
                        stroke: this.props.averageColor
                    }}
                    d={`m ${this.xAxis(this.props.data.average)},0 0,${-barHeight}`}
                />
                {/*Last*/}
                <path
                    transform={`translate(${this.xAxis(this.props.data.last)},${-barHeight + 5})`}
                    style={{
                        ...styles.gauge.last,
                        fill: this.props.minMaxColor,
                        stroke: this.props.averageColor
                    }}
                    d={`m 0,0 -5,-5 10,0 Z`}
                />
            </g>
        );
    }

    renderLegend(){
        return (
            <text y={0}
                  x={0}
                  style={styles.legend}
                  transform={`translate(${this.graphWidth},${this.props.height - marginBottom - axisHeight})`}
            >
                {formatRaw(this.props.data.average, this.props.format) + this.props.legend}
            </text>
        );
    }

    renderMetricToolTip(metric){
        return formatRaw(metric, this.props.formatToolTip) + this.props.legend;
    }

    renderToolTip(){
        return (
            <div
                style={{
                    userSelect: 'none'
                }}
            >
                <div style={styles.tooltip.label}>
                    Min:
                </div>
                <div style={styles.tooltip.item}>
                    {this.renderMetricToolTip(this.props.data.min)}
                </div>
                <br/>
                <div style={styles.tooltip.label}>
                    Average:
                </div>
                <div style={styles.tooltip.item}>
                    {this.renderMetricToolTip(this.props.data.average)}
                </div>
                <br/>
                <div style={styles.tooltip.label}>
                    Last:
                </div>
                <div style={styles.tooltip.item}>
                    {this.renderMetricToolTip(this.props.data.last)}
                </div>
                <br/>
                <div style={styles.tooltip.label}>
                    Max:
                </div>
                <div style={styles.tooltip.item}>
                    {this.renderMetricToolTip(this.props.data.max)}
                </div>
            </div>
        );
    }

    static propTypes = {
        start: PropTypes.object,
        stop: PropTypes.object,
        chart: PropTypes.string,
        data: PropTypes.shape({
            min: PropTypes.number,
            average: PropTypes.number,
            last: PropTypes.number,
            max: PropTypes.number,
        }),
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        loadingStatus: PropTypes.string,
        onLoadData: PropTypes.func,
        averageColor: PropTypes.string,
        lastColor: PropTypes.string,
        maxColor: PropTypes.string,
        legend: PropTypes.string,
        format: PropTypes.string,
        formatToolTip: PropTypes.string,
    };

    static defaultProps = {
    }
}