import React, { Component, PropTypes } from 'react';
import {scaleLinear} from 'd3';

import ToolTip from 'rc-tooltip';
import '../../tips/tipDark.css';

import {formatRaw} from '../../../config/locale';
import Colors from '../../../config/colors';

const height = 20;
const width = 20;

const styles = {
    legend: {
        fontSize: 13,
        fontFamily: 'Roboto, sans-serif',
        color: Colors.mediumDark,
        textAlign: 'right',
        textAnchor: 'end',
        userSelect: 'none'
    },
    tooltip:{
        title:{
            display:'block',
            fontFamily: 'Roboto, sans-serif',
            fontSize: 13.5,
            color: 'white',
            width: 160,
            paddingBottom: 4
        },
        label:{
            display:'inline-block',
            fontFamily: 'Roboto, sans-serif',
            fontSize: 13.5,
            lineHeight: '14px',
            color: 'white',
            width: 90,
            paddingBottom: 2
        },
        item: {
            display:'inline-block',
            fontFamily: 'Roboto, sans-serif',
            fontSize: 13,
            lineHeight: '13px',
            textAlign: 'right',
            width: 70
        }
    },
    axis: {
        axisStyle: {
            fill: 'none',
            stroke: Colors.medium,
            strokeWidth: 1,
            strokeOpacity: 1
        },
        arrowStyle: {
            fill: Colors.medium,
            fillOpacity: 1,
            stroke: Colors.medium,
            strokeWidth: 1,
            strokeOpacity: 1
        },
        limitMarkerStyle : {
            fill: 'none',
            stroke: Colors.medium,
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1
        },
        axisTextStyle:{
            fontSize: 8,
            fontFamily: 'Roboto, sans-serif',
            fill: Colors.medium,
            textAlign: 'center',
            textAnchor: 'middle',
            userSelect: 'none'
        }
    },
    gauge: {
        minMax: {
            fillOpacity:1,
            fill: '#5599ff',
            stroke: 'none',
            transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        },
        last: {
            fill: '#5599ff',
            stroke: '#003380',
            transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        },
        average: {
            fill: '#003380',
            stroke: 'none',
            strokeOpacity: 1,
            strokeWidth: 2,
            transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        }
    }
};

export default class ServerGauge extends Component{
    constructor(props){
        super(props);
        this.yAxis = null;
    }

    render() {
        this.yAxis = this.props.total && scaleLinear()
            .domain([0, this.props.total])
            .range([0, height]);

        const extraToolTipProps = {};
        if(!this.props.stats){
            extraToolTipProps.visible = false;
        }

        return  (
            <div
                style={{
                    display: 'inline-block',
                    ...this.props.style,
                }}
            >
                <ToolTip
                    placement="top"
                    prefixCls={'rc-tooltip-dark'}
                    overlay={this.props.stats ? this.renderToolTip() : ''}
                    trigger={['hover']}
                    mouseEnterDelay={0.75}
                    destroyTooltipOnHide={true}
                    {...extraToolTipProps}
                >
                    <svg
                        width={width}
                        height={height}
                    >
                        {this.renderGauge()}
                    </svg>
                </ToolTip>
            </div>
        )
    }

    renderGauge(){
        return (
            <g>
                <rect
                    x={0}
                    y={0}
                    rx={5}
                    ry={3.5}
                    width={width}
                    height={height}
                    style={{
                        fill: 'white',
                    }}
                />
                <rect
                    x={0}
                    y={this.yAxis(this.props.total - this.props.stats.average)}
                    rx={5}
                    ry={3.5}
                    width={width}
                    height={this.yAxis(this.props.stats.average)}
                    style={{
                        fill: this.props.stats.max/this.props.total <= 0.75 ? Colors.serverOK : Colors.serverKO,
                    }}
                />
                <rect
                    x={0}
                    y={0}
                    rx={5}
                    ry={3.5}
                    width={width}
                    height={height}
                    style={{
                        fill: 'none',
                        stroke: Colors.mediumDark,
                        strokeWidth: 2
                    }}
                />
            </g>
        );
    }

    renderMetricToolTip(metric){
        return formatRaw(metric, this.props.formatToolTip) + this.props.legend;
    }

    renderToolTip(){
        return (
            <div
                style={{
                    userSelect: 'none'
                }}
            >
                <div style={styles.tooltip.title}>
                    Node: {this.props.node}
                </div>
                <br/>
                <div style={styles.tooltip.label}>
                    Min:
                </div>
                <div style={styles.tooltip.item}>
                    {this.renderMetricToolTip(this.props.stats.min)}
                </div>
                <br/>
                <div style={styles.tooltip.label}>
                    Average:
                </div>
                <div style={styles.tooltip.item}>
                    {this.renderMetricToolTip(this.props.stats.average)}
                </div>
                <br/>
                <div style={styles.tooltip.label}>
                    Last:
                </div>
                <div style={styles.tooltip.item}>
                    {this.renderMetricToolTip(this.props.stats.last)}
                </div>
                <br/>
                <div style={styles.tooltip.label}>
                    Max:
                </div>
                <div style={styles.tooltip.item}>
                    {this.renderMetricToolTip(this.props.stats.max)}
                </div>
                <br/>
                <br/>
                <div style={styles.tooltip.label}>
                    Max available:
                </div>
                <div style={styles.tooltip.item}>
                    {this.renderMetricToolTip(this.props.total)}
                </div>
            </div>
        );
    }

    static propTypes = {
        node: PropTypes.string,
        stats: PropTypes.shape({
            min: PropTypes.number,
            average: PropTypes.number,
            last: PropTypes.number,
            max: PropTypes.number,
        }),
        total: PropTypes.number,
        legend: PropTypes.string,
        formatToolTip: PropTypes.string,
        style: PropTypes.object,
    };

    static defaultProps = {
    }
}