import {connect} from 'react-redux';

import GaugeGraph from './GaugeGraph';
import {MainViewActions, CommonViewActions} from '../../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        start: state.time.timeSpan.start,
        stop: state.time.timeSpan.stop,
        width: ownProps.width || 272,
        height: ownProps.height || 25,
        data: state.view[ownProps.view][ownProps.chart] && state.view[ownProps.view][ownProps.chart].data,
        loadingStatus: state.view[ownProps.view][ownProps.chart] && state.view[ownProps.view][ownProps.chart].loadingStatus,
        averageColor: ownProps.averageColor,
        minMaxColor: ownProps.minMaxColor,
        legend: ownProps.legend,
        decimals: ownProps.decimals,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onLoadData: () => {
            dispatch(CommonViewActions.requestLoadChart({view: ownProps.view, chart: ownProps.chart}));
        },
    }
};

const GaugeGraphContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(GaugeGraph);

export default GaugeGraphContainer;