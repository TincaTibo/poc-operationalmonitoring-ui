import {connect} from 'react-redux';

import ESStatus from './ESStatus';
import {MainViewActions, CommonViewActions} from '../../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        start: state.time.timeSpan.start,
        stop: state.time.timeSpan.stop,
        data: state.view[ownProps.view][ownProps.chart] && state.view[ownProps.view][ownProps.chart].data,
        loadingStatus: state.view[ownProps.view][ownProps.chart] && state.view[ownProps.view][ownProps.chart].loadingStatus,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onLoadData: () => {
            dispatch(CommonViewActions.requestLoadChart({view: ownProps.view, chart: ownProps.chart}));
        },
    }
};

const ESStatusContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ESStatus);

export default ESStatusContainer;