import React, {PureComponent, PropTypes} from 'react';

import BarGraphContainer from './graphs/BarGraphContainer';

export default class AppsView extends PureComponent {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{
                width: this.props.width
            }}>
                <BarGraphContainer
                    view = 'apps'
                    chart = "errorsInLogs"
                    title = "Status FAIL"
                    layout = "vertical"
                    width = {this.props.width > 1200 ? this.props.width / 2 - 5 : this.props.width - 20 }
                    margin = {this.props.width > 1200 ? 10 : 0 }
                    legendLayout = "horizontal"
                    stacked
                    definition = {{
                        yAxes: [
                            {
                                id: 'left-logsErrors',
                                orientation: 'left',
                                legend: 'Status count'
                            }
                        ]
                    }}
                />
                <BarGraphContainer
                    view = 'apps'
                    chart = "warningsInLogs"
                    title = "Status WARN"
                    layout = "vertical"
                    legendLayout = "horizontal"
                    width = {this.props.width > 1200 ? this.props.width / 2 - 5 : this.props.width - 20 }
                    stacked
                    definition = {{
                        yAxes: [
                            {
                                id: 'left-logsWarnings',
                                orientation: 'left',
                                legend: 'Status count'
                            }
                        ]
                    }}
                />
            </div>
        );
    }

    static propTypes = {
        width: PropTypes.number
    };
}