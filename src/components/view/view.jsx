import React, {PureComponent, PropTypes} from 'react';

import AppsView from '../../containers/view/AppsViewContainer';
import ServersView from '../../containers/view/ServersViewContainer';

const styles = {};

export default class View extends PureComponent {

    render() {
        switch (this.props.view) {
            case 'apps':
                return <AppsView width={this.props.width}/>;
            case 'servers':
                return <ServersView width={this.props.width} height={this.props.height}/>;
            default:
                return '';
        }
    }

    static propTypes = {
        view: PropTypes.string,
        width: PropTypes.number,
    };
};