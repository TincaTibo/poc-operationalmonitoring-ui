import React, {PureComponent, PropTypes} from 'react';

import LogsGridContainer from '../../containers/grids/LogsGridContainer';

export default class ServersView extends PureComponent {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <LogsGridContainer
                    width = {this.props.width}
                    height = {this.props.height}
                />
            </div>
        );
    }

    static propTypes = {};

    static defaultProps = {}
}