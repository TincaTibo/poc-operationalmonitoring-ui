import React, {Component, PropTypes} from 'react';

const styles = {
    main:{
        display: 'inline-block',
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,
        height: 13,
        fontSize: 13,
        textAlign: 'left',
        whiteSpace: 'nowrap',
        color: '#000',
        textOverflow: 'ellipsis',
        cursor: 'pointer',
        fontFamily: 'Roboto, sans-serif',
    },
    hover:{
        textDecoration: 'underline',
        textDecorationColor: 'lightGrey',
    },
};

export default class Link extends Component {

    constructor(props){
        super(props);
        this.state = {
            hover: false,
        };
    }

    toggleHover = () => {
        this.setState({hover: !this.state.hover});
    };

    render(){
        return (
            <span
                style={{...styles.main, ...(this.state.hover ? styles.hover : {}), ...this.props.style}}
                onMouseEnter={this.toggleHover}
                onMouseLeave={this.toggleHover}
                onTouchTap={() => {
                    if(this.props.item){
                        this.props.onOpen(this.props.resourceType, this.props.item)
                    }
                }}
            >
                {this.props.item ? this.props.item : null}
            </span>
        );
    }

    static propTypes = {
        style: PropTypes.object,
        item: PropTypes.string,
        resourceType: PropTypes.string,
        onOpen: PropTypes.func.isRequired
    };

    static defaultProps = {
        field: '@id'
    };
}
