import {connect} from 'react-redux';

import Link from './Link';
import {DetailsActions} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        item: ownProps.item,
        resourceType: ownProps.resourceType,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onOpen: (resourceType, id) => {
            dispatch(DetailsActions.requestOpenDetails(resourceType, id));
        },
    }
};

const LinkContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Link);

export default LinkContainer;