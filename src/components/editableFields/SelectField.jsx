import React, {PropTypes} from 'react';

import Colors from '../../config/colors';
import SingleSelect from '../selects/SingleSelect';

const styles = {
    fields:{
        fieldHeaderStyle : {
            fontWeight: 'bold',
            fontSize: '12px',
            marginRight: '16px',
            width: '180px',
            display: 'table-cell'
        },
        fieldValueStyle: {
            fontFamily: 'Monospace',
            fontSize: '12px',
            width: '250px',
            textAlign: 'left',
            display: 'table-cell'
        }
    }
};

const SelectField = props => {
    if((props.showAdvanced && props.advanced) || !props.advanced){
        return (
            <span>
                <span style={styles.fields.fieldHeaderStyle}>
                    {'\u00a0'.repeat(props.indent)}
                    {props.header}:&nbsp;
                </span>
                <span style={styles.fields.fieldValueStyle}>
                    {'\u00a0'.repeat(props.indent)}
                    {props.editMode && props.editable ?
                        renderEditor(props) :
                        renderValue(props)
                    }
                </span>
                <br/>
            </span>
        );
    }
    else{
        return <span/>;
    }
};

function renderValue(props){
    return getValue(props);
}

function getValue({newValue, ownValue, configValue}){
    if(newValue !== undefined && newValue !== null){
        return newValue;
    }
    else if(ownValue !== undefined){
        return ownValue;
    }
    else{
        return configValue;
    }
}

function getColor({newValue, ownValue, configValue}){
    if(newValue !== undefined
        && newValue !== null
        && newValue !== (ownValue !== undefined ? ownValue : configValue)){
        return Colors.info;
    }
    else if(ownValue !== undefined){
        return 'black';
    }
    else{
        return Colors.optionSelected;
    }
}

function renderEditor(props){
    return (
        <SingleSelect
            options = {props.options.map((o,idx) => ({
                value: o,
                label: props.optionsLabels ? props.optionsLabels[idx] : o
            }))}
            value = {getValue(props)}
            name = {`select-${props.header}`}
            onSelect={selected => {
                selected && props.onSelect(selected.value)
            }}
            clearable={false}
        />
    );
}


SelectField.propTypes = {
    header: PropTypes.string.isRequired,
    indent: PropTypes.number,
    ownValue: PropTypes.string,
    configValue: PropTypes.string,
    newValue: PropTypes.string,
    options: PropTypes.array,
    optionsLabels: PropTypes.array,
    advanced: PropTypes.bool,
    editable: PropTypes.bool,
    showAdvanced: PropTypes.bool,
    editMode: PropTypes.bool,
    onSelect: PropTypes.func,
};

SelectField.defaultProps = {
    indent: 0,
    advanced: false,
    editable: false,
    options: [],
    optionsLabel: null,
    showAdvanced: false,
    editMode: false,
    onSelect: () => {}
};

export default SelectField;