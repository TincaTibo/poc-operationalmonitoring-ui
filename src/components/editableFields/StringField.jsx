import React, {PropTypes, PureComponent} from 'react';
import TextField from 'material-ui/TextField';
import _ from 'lodash';
import Joi from 'joi';

import Colors from '../../config/colors';
import Error from '../tips/Error';

const styles = {
    fields:{
        fieldHeaderStyle : {
            fontWeight: 'bold',
            fontSize: '12px',
            marginRight: '16px',
            width: '180px',
            display: 'table-cell'
        },
        fieldValueStyle: {
            fontFamily: 'Monospace',
            fontSize: '12px',
            width: '250px',
            textAlign: 'left',
            display: 'table-cell'
        }
    },
    edit:{
        bottom: 2,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,
        height: 14,
        fontSize: '12px',
        textAlign: 'left',
        whiteSpace: 'nowrap',
        fontFamily: 'Monospace',
        width: 250
    },
    textArea:{
        fontFamily: 'Monospace',
        width: 250,
        lineHeight: '14px',
        marginTop: 5,
        top: 5,
    },
    hover:{
        textDecoration: 'underline',
        textDecorationColor: 'lightGrey',
    },
    underline:{
        bottom: -8
    },
    underlineMulti:{
        bottom: 10
    },
};

export default class StringField extends PureComponent{
    constructor(props){
        super(props);
        this.editField = null;
        this.id = _.random(0,90000000).toString();

        this.state = {
            checked: false,
            valid: null,
            error: null,
            hover: false,
        };
        this.start = this.props.editMode && this.props.standAlone; //flag to focus on activating edit mode if single comp
    }

    componentWillReceiveProps(nextProps){
        if(this.props.editMode !== nextProps.editMode && !nextProps.editMode){
            this.setState({
                checked: false,
                valid: null,
                error: null,
            });
        }
        if(this.props.editMode !== nextProps.editMode && nextProps.editMode && this.props.standAlone){
            this.start = true;
        }
    }

    render(){
        if(((this.props.showAdvanced && this.props.advanced) //display if: advanced prop and showadvanced active
                || !this.props.advanced )                    //or not showadvanced active
            //and not (prop to hide when empty and not editMode and empty)
            && (this.props.editMode || !this.props.hideIfEmpty || this.getValue())
          ){
            return (
                <span>
                <span style={styles.fields.fieldHeaderStyle}>
                    {'\u00a0'.repeat(this.props.indent)}
                    {this.props.header}:&nbsp;
                </span>
                    {this.props.editMode && this.props.editable ?
                        this.renderEditor() : this.renderValue()
                    }
                    <br/>
            </span>
            );
        }
        else{
            return <span/>;
        }
    }

    renderValue(){
        return (
            <span
                style={{
                    ...styles.fields.fieldValueStyle,
                    color: this.getTextColor(),
                    cursor: this.props.onEditMode ? 'pointer' : 'text', //pointer if can trigger editMode
                    ...(this.state.hover ? styles.hover : {})
                }}
                onClick={(event) => {
                    //Get to edit mode if callback given
                    if(this.props.onEditMode && this.props.editable && !this.props.editMode){
                        this.props.onEditMode(true, this.getValue());
                        event.stopPropagation();
                    }
                }}
                onMouseEnter={this.toggleHover}
                onMouseLeave={this.toggleHover}
            >
                {'\u00a0'.repeat(this.props.indent)}
                {this.props.display(this.getValue())}
            </span>
        );
    }

    toggleHover = () => {
        if(this.props.onEditMode){
            this.setState({hover: !this.state.hover})
        }
    };

    componentDidUpdate(){
        if(this.props.editMode && this.start){
            this.editField.focus();
            this.start = false;
        }
    }

    componentDidMount(){
        if(this.props.editMode && this.start){
            this.editField.focus();
        }
    }

    getTextColor(){
        if(this.props.newValue !== null && this.props.newValue !== undefined){
            if(this.state.checked){
                if(this.state.valid){
                    if(this.props.newValue !== this.props.ownValue){
                        return Colors.info;
                    }
                    else{
                        return 'black';
                    }
                }
                else{
                    return Colors.error;
                }
            }
            else{
                if(this.props.newValue !== this.props.ownValue){
                    return Colors.working;
                }
                else{
                    return 'black';
                }
            }
        }
        else if(this.props.ownValue !== undefined){
            return 'black';
        }
        else{
            return Colors.optionSelected
        }
    }

    renderEditor(){
        return (
            <div style={{display: 'inline-block'}}>
                <TextField
                    ref={(ref) => { this.editField = ref }}
                    id={ this.id }
                    style={{
                        ...styles.edit,
                        ...this.props.style
                    }}
                    inputStyle={{
                        color: this.getTextColor()
                    }}
                    textareaStyle = {{
                        ...styles.textArea,
                        color: this.getTextColor()
                    }}
                    onKeyDown={this.keyDown}
                    onBlur={this.blur}
                    onChange={this.handleChange}
                    onTouchTap={(event) => {
                        event.stopPropagation();
                        document.getElementById(this.id).focus();
                    }}
                    value={this.getValue()}
                    underlineStyle={this.props.multiLine ? styles.underlineMulti : styles.underline}
                    underlineFocusStyle={this.props.multiLine ? styles.underlineMulti : styles.underline}
                    multiLine={this.props.multiLine}
                    rows={this.props.multiLine ? 2 : undefined}
                    rowsMax={this.props.multiLine ? 3 : undefined}
                    type={this.props.password ? 'password' : 'text'}
                />
                {this.state.checked && !this.state.valid &&
                    <div style={{
                        position: 'relative',
                        display: 'inline-block',
                        width: 0,
                        height: 0,
                        left: 5,
                        top: 2
                    }}>
                        <Error
                            tip = {this.state.error}
                            placement="top"
                        />
                    </div>
                }
            </div>
        )
    }

    getValue(){
        if(this.props.newValue !== undefined && this.props.newValue !== null){
            return this.props.newValue.toString();
        }
        else if(!this.props.editMode && this.props.ownValue !== undefined){
            return this.props.ownValue.toString();
        }
        else if(this.props.configValue !== undefined){
            return this.props.configValue.toString();
        }
        else{
            return undefined;
        }
    }

    blur = () => {
        this.editField.blur();
        this.checkField(this.props);
        //Back to display mode if callback given
        this.props.onEditMode && this.props.onEditMode(false);
    };

    checkField(props){
        if(props.schema){
            const value = this.getValue();
            const validationError = Joi.validate(value, props.schema, {
                skipFunctions: true
            }).error;

            if(validationError){
                console.log(validationError.message);
                this.setState({
                    checked: true,
                    valid: false,
                    error: props.parsingError || validationError.message,
                });
            }
            else{
                this.setState({
                    checked: true,
                    valid: true,
                    error: null,
                });
            }
        }
    }

    keyDown = (event) => {
        switch(event.key){
            case 'Enter':
                this.blur();

                //Ask for save field if callback given
                this.props.onSaveField && this.props.onSaveField(this.getValue());

                break;
            case 'Escape':
                this.blur();
                break;
        }
    };

    handleChange = (event, newValue) => {
        this.props.onUpdateField(newValue);
    };

    static propTypes = {
        header: PropTypes.string.isRequired,
        newValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        ownValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        configValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        indent: PropTypes.number,
        advanced: PropTypes.bool,
        editable: PropTypes.bool,
        showAdvanced: PropTypes.bool,
        editMode: PropTypes.bool,
        multiLine: PropTypes.bool,
        standAlone: PropTypes.bool,
        hideIfEmpty: PropTypes.bool,
        password: PropTypes.bool,
        rows: PropTypes.number,
        display: PropTypes.func,
        schema: PropTypes.any,
        parsingError: PropTypes.string,
        onEditMode: PropTypes.func,
        onUpdateField: PropTypes.func,
        onSaveField: PropTypes.func,
    };

    static defaultProps = {
        indent: 0,
        advanced: false,
        editable: false,
        hideIfEmpty: false,
        showAdvanced: false,
        editMode: false,
        multiLine: false,
        standAlone: false,
        password: false,
        rowsMax: 3,
        display: item => item,
        onUpdateField: () => {},
    };
};
