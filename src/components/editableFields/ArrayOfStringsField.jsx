import React, {PropTypes, PureComponent} from 'react';
import TextField from 'material-ui/TextField';
import _ from 'lodash';
import Joi from 'joi';
import IconButton from 'material-ui/IconButton';
import AddIcon from 'material-ui/svg-icons/content/add-circle-outline';
import RemoveIcon from 'material-ui/svg-icons/content/remove-circle-outline';

import Colors from '../../config/colors';
import Error from '../tips/Error';

const styles = {
    fields:{
        fieldHeaderStyle : {
            fontWeight: 'bold',
            fontSize: '12px',
            width: 180,
            display: 'inline-block',
            verticalAlign: 'top'
        },
        fieldValueStyle: {
            fontFamily: 'Monospace',
            fontSize: '12px',
            width: 250,
            textAlign: 'left',
            display: 'inline-block'
        }
    },
    button:{
        border: '10px',
        display: 'inline-block',
        cursor: 'pointer',
        margin: 0,
        padding: 0,
        position: 'relative',
        overflow: 'visible',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        width: 19,
        height: 19,
        left: 18,
        top: 2
    },
    checkedIcon: {
        width: 17,
        height: 17,
        position: 'relative',
        top: 3
    },
    edit:{
        bottom: 2,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,
        height: 14,
        fontSize: 12,
        textAlign: 'left',
        whiteSpace: 'nowrap',
        fontFamily: 'Monospace',
        width: 200
    },
    underline:{
        bottom: -8
    },
    icon:{
        width: 17,
        height: 17,
        position: 'relative'
    },
    actionDiv:{
        display:'inline-block',
        position: 'relative',
        width: 0,
        height:0,
    }
};

export default class ArrayOfStringsField extends PureComponent{

    constructor(props){
        super(props);
        this.state = {
            modified: false,
            checked: [],
            valid: [],
            error: []
        };
        this.editFields = [];
        this.id = _.random(0,90000000).toString();
    }

    componentWillReceiveProps(nextProps){
        if(this.props.editMode !== nextProps.editMode && !nextProps.editMode) {
            this.setState({
                modified: false,
                checked: [],
                valid: [],
                error: []
            });
        }
    }

    render() {
        if((this.props.showAdvanced && this.props.advanced) || !this.props.advanced){
            return (
                <div>
                    <div style={styles.fields.fieldHeaderStyle}>
                        {'\u00a0'.repeat(this.props.indent)}
                        {this.props.header}:&nbsp;
                    </div>
                    <div style={{
                        maxHeight: 130,
                        overflowY: 'auto',
                        paddingBottom: 3,
                        display: 'inline-block',
                        overflowX: 'visible',
                        width: this.props.maxWidth ?
                            this.props.maxWidth
                            - 190
                            : styles.fields.fieldValueStyle.width
                    }}>
                    {this.props.editMode && this.props.editable ?
                        this.renderEditor() :
                        this.renderValue()
                    }
                    </div>
                </div>
            );
        }
        else{
            return <span/>;
        }
    }

    renderValue(){
        const value = this.getValue();
        return (
            <span
                style={{
                    ...styles.fields.fieldValueStyle
                }}
            >
            {value.length ? value.map((v,i) =>
                (
                    <span
                        key={v}
                    >
                        {'\u00a0'.repeat(this.props.indent)}
                        {this.props.display(v)}
                        {i !== value.length - 1 && <br/>}
                    </span>
                )
            ) : '[]'}
        </span>
        );
    }

    getValue(){
        return this.props.newValue || this.props.ownValue || this.props.configValue;
    }

    getColor(index) {
        if(this.props.newValue !== null && this.props.newValue !== undefined){
            if(this.state.checked[index]){
                if(this.state.valid[index] && !this.props.onSaveField){ //when save is done onBlur
                    return Colors.info;
                }
                else if(this.state.valid[index] && this.props.onSaveField){
                    return 'black';
                }
                else{
                    return Colors.error;
                }
            }
            else{
                if(this.state.modified){
                    return Colors.working;
                }
                else{
                    return 'black';
                }
            }
        }
        else if(this.props.ownValue !== undefined){
            return 'black';
        }
        else{
            return Colors.optionSelected
        }
    }

    renderEditor(){
        const value = this.getValue();
        return (
            <span
                style={{
                    ...styles.fields.fieldValueStyle,
                    width: '100%'
                }}
            >
            {value.length ? value.map((val,index) =>
                (
                    <span
                        key={index}
                    >
                        {'\u00a0'.repeat(this.props.indent)}
                        {this.renderTextField(index, val)}
                        <div style={styles.actionDiv}>
                            <IconButton
                                style={styles.button}
                                onTouchTap={() => {
                                    this.onRemove(index)
                                }}
                                iconStyle={styles.icon}
                            >
                                <RemoveIcon
                                    color='#000'
                                    hoverColor='#64C4E0'
                                />
                            </IconButton>
                        </div>
                        {index === value.length-1 &&
                        <div style={styles.actionDiv}>
                            <IconButton
                                style={{
                                    ...styles.button,
                                    left: 36
                                }}
                                onTouchTap={() => {
                                    this.onAdd()
                                }}
                                iconStyle={styles.icon}
                            >
                                <AddIcon
                                    color='#000'
                                    hoverColor='#64C4E0'
                                />
                            </IconButton>
                        </div>
                        }
                        {index !== value.length-1 && <br/>}
                    </span>
                )
            ) :
                <span>
                    []
                    <div style={styles.actionDiv}>
                        <IconButton
                            style={{
                                ...styles.button,
                                left: 30
                            }}
                            onTouchTap={() => {
                                this.onAdd()
                            }}
                            iconStyle={styles.icon}
                        >
                            <AddIcon
                                color='#000'
                                hoverColor='#64C4E0'
                            />
                        </IconButton>
                    </div>
                </span>
            }
        </span>
        );
    }

    renderTextField(index, value){
        const id = this.id + '-' + index;
        return (
            <div style={{display: 'inline-block'}}>
                <TextField
                    ref={(ref) => { this.editFields[id] = ref }}
                    id={ id }
                    style={{
                        ...styles.edit,
                        width: this.props.maxWidth ?
                            this.props.maxWidth - 190 - 70
                            : styles.edit.width - 25,
                    }}
                    inputStyle={{
                        color: this.getColor(index)
                    }}
                    onKeyDown={(event) => this.keyDown(event, id, index, value)}
                    onBlur={(event) => this.blur(id, index, value)}
                    onChange={(event, newValue) => this.handleChange(index, newValue)}
                    onTouchTap={(event) => {
                        event.stopPropagation();
                        document.getElementById(id).focus();
                    }}
                    value={value}
                    underlineStyle={styles.underline}
                    underlineFocusStyle={styles.underline}
                />
                {this.state.checked[index] && !this.state.valid[index] &&
                <div style={{
                    position: 'relative',
                    display: 'inline-block',
                    width: 0,
                    height: 0,
                    left: 5,
                    top: 2
                }}>
                    <Error
                        tip = {this.state.error[index]}
                        placement="top"
                    />
                </div>
                }
            </div>
        )
    }

    blur = (id, index, value) => {
        this.editFields[id].blur();
        this.checkField(this.props, index, value);

        if(this.props.onSaveField){
            this.props.onSaveField(this.getValue());
            this.setState({
                modified: false
            });
        }
    };

    checkField(props, index, value){
        if(props.schema){
            const validationError = Joi.validate(value, props.schema, {
                skipFunctions: true
            }).error;

            if(validationError){
                this.setState({
                    checked: {
                        ...this.state.checked,
                        [index]: true
                    },
                    valid: {
                        ...this.state.checked,
                        [index]: false
                    },
                    error: {
                        ...this.state.checked,
                        [index]: props.parsingError || validationError.message,
                    }
                });
            }
            else{
                this.setState({
                    checked: {
                        ...this.state.checked,
                        [index]: true
                    },
                    valid: {
                        ...this.state.checked,
                        [index]: true
                    },
                    error: {
                        ...this.state.checked,
                        [index]: null,
                    }
                });
            }
        }
    }

    keyDown = (event, id, index, value) => {
        switch(event.key){
            case 'Enter':
                this.blur(id, index, value);
                break;
            case 'Escape':
                this.blur(id, index, value);
                break;
        }
    };

    handleChange = (index, newValue) => {
        let fieldValue = [...this.getValue()];
        fieldValue[index] = newValue;
        this.setState({
            modified: true
        });
        this.props.onUpdateField(fieldValue);
    };

    onAdd(){
        let fieldValue = [...this.getValue()];
        fieldValue.push(null);

        this.props.onUpdateField(fieldValue);
    }

    onRemove(index){
        let fieldValue = [...this.getValue()];
        fieldValue.splice(index, 1);

        this.props.onUpdateField(fieldValue);

        if(this.props.onSaveField){
            this.props.onSaveField(fieldValue);
            this.setState({
                modified: false
            });
        }
    }

    static propTypes = {
        header: PropTypes.string.isRequired,
        ownValue: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
        configValue: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
        indent: PropTypes.number,
        advanced: PropTypes.bool,
        editable: PropTypes.bool,
        showAdvanced: PropTypes.bool,
        editMode: PropTypes.bool,
        display: PropTypes.func,
        parsingError: PropTypes.string,
        onSaveField: PropTypes.func,
        onUpdateField: PropTypes.func,
        maxWidth: PropTypes.number,
    };

    static defaultProps = {
        indent: 0,
        advanced: false,
        editable: false,
        showAdvanced: false,
        editMode: false,
        display: item => item
    };
}