import React, {PropTypes, PureComponent} from 'react';
import TextField from 'material-ui/TextField';
import _ from 'lodash';
import Joi from 'joi';
import IconButton from 'material-ui/IconButton';
import AddIcon from 'material-ui/svg-icons/content/add-circle-outline';
import RemoveIcon from 'material-ui/svg-icons/content/remove-circle-outline';

import Colors from '../../config/colors';
import Error from '../tips/Error';

import BoolField from '../editableFields/BoolField';

const styles = {
    fields:{
        fieldHeaderStyle : {
            fontWeight: 'bold',
            fontSize: '12px',
            marginRight: 16,
            width: 220,
            display: 'table-cell'
        },
        fieldValueStyle: {
            fontFamily: 'Monospace',
            fontSize: '12px',
            width: 290,
            textAlign: 'left',
            display: 'table-cell'
        }
    },
    button:{
        border: '10px',
        display: 'inline-block',
        cursor: 'pointer',
        margin: 0,
        padding: 0,
        position: 'relative',
        overflow: 'visible',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        width: 19,
        height: 19,
        left: 18,
        top: 2
    },
    checkedIcon: {
        width: 17,
        height: 17,
        position: 'relative',
        top: 3
    },
    edit:{
        bottom: 2,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 10,
        height: 14,
        fontSize: 12,
        textAlign: 'left',
        whiteSpace: 'nowrap',
        fontFamily: 'Monospace',
        width: 210
    },
    underline:{
        bottom: -8
    },
    icon:{
        width: 17,
        height: 17,
        position: 'relative'
    },
    actionDiv:{
        display:'inline-block',
        position: 'relative',
        width: 0,
        height:0,
    },
    rights:{
        headers:{
            fontWeight: 'bold',
            fontSize: '12px',
        },
        title:{
            width: 220,
            display: 'table-cell',
            fontSize: '12px',
        },
        option:{
            display: 'table-cell',
            textAlign: 'left'
        }
    }
};

export default class ArrayOfStringsFieldAndOptions extends PureComponent{

    constructor(props){
        super(props);
        this.state = {
            modified: false,
            checked: [],
            valid: [],
            error: []
        };
        this.editFields = [];
        this.id = _.random(0,90000000).toString();
    }

    componentWillReceiveProps(nextProps){
        if(this.props.editMode !== nextProps.editMode && !nextProps.editMode) {
            this.setState({
                modified: false,
                checked: [],
                valid: [],
                error: []
            });
        }
    }

    render() {
        if((this.props.showAdvanced && this.props.advanced) || !this.props.advanced){
            return (
                <span>
                    {this.props.header &&
                        <span style={styles.fields.fieldHeaderStyle}>
                            {'\u00a0'.repeat(this.props.indent)}
                            {this.props.header}:&nbsp;
                        </span>
                    }
                    {this.renderHeaders()}
                    {this.props.editMode && this.props.editable ?
                        this.renderEditor() :
                        this.renderValue()
                    }
                    <br/>
                </span>
            );
        }
        else{
            return <span/>;
        }
    }

    renderHeaders(){
        return (
            <span style={styles.rights.headers}>
                <span style={styles.rights.title}>
                    {this.props.fields && this.props.fields.title && this.props.fields.title.header}
                </span>
                {
                    this.props.fields && this.props.fields.options && this.props.fields.options.map(o => (
                        <span style={{
                            ...styles.rights.option,
                            width: styles.fields.fieldValueStyle.width / this.props.fields.options.length
                        }}>
                                {o.header}
                        </span>
                    ))
                }
                <br/>
            </span>
        )
    }

    renderValue(){
        const value = this.getValue();
        return (
            <span>
            {value.length ? value.map((val,index) =>
                (<span
                    key={index}
                >
                    <span
                        style={{
                            ...styles.rights.title,
                            fontFamily: 'Monospace',
                        }}
                    >
                        {val && val.title && val.title.label}
                    </span>
                    <span
                        style={styles.fields.fieldValueStyle}
                    >
                        {val && val.options && this.props.fields && this.props.fields.options && this.props.fields.options.map((o,i) =>
                            (
                                <span
                                    style={{
                                        ...styles.rights.option,
                                        width: styles.fields.fieldValueStyle.width / this.props.fields.options.length
                                    }}
                                    key={o.key}
                                >
                                    <BoolField
                                        key={o.key}
                                        ownValue = {val.options[o.key] || false}
                                        valueStyle={{
                                            ...styles.rights.option,
                                            width: styles.fields.fieldValueStyle.width / this.props.fields.options.length
                                        }}
                                        editable = {false}
                                        newline = {false}
                                    />
                                </span>
                            )
                        )}
                    </span>
                </span>)
            ) :
                <span>
                    []
                </span>
            }
            </span>
        );
    }

    getValue(){
        return this.props.newValue || this.props.ownValue || this.props.configValue;
    }

    getColor(index) {
        if(this.props.newValue !== null && this.props.newValue !== undefined){
            if(this.state.checked[index]){
                if(this.state.valid[index]){
                    return Colors.info;
                }
                else{
                    return Colors.error;
                }
            }
            else{
                if(this.state.modified){
                    return Colors.working;
                }
                else{
                    return 'black';
                }
            }
        }
        else if(this.props.ownValue !== undefined){
            return 'black';
        }
        else{
            return Colors.optionSelected
        }
    }

    renderEditor(){
        const value = this.getValue();
        return (
            <span>
            {value.length ? value.map((val,index) =>
                (<span
                        key={index}
                    >
                        <span
                            style={{
                                ...styles.rights.title,
                                fontFamily: 'Monospace',
                            }}
                        >
                            { val && val.title && val.title.editable ?
                                this.renderTextField(index, val.title.label)
                                : val.title.label
                            }
                        </span>
                        <span
                            style={styles.fields.fieldValueStyle}
                        >
                        {val && val.options && this.props.fields && this.props.fields.options && this.props.fields.options.map((o,i) =>
                            (<BoolField
                                key={o.key}
                                ownValue = {val.options[o.key] || false}
                                valueStyle={{
                                    ...styles.rights.option,
                                    width: styles.fields.fieldValueStyle.width / this.props.fields.options.length
                                }}
                                onSelect = { bool => {
                                    const newValue = _.cloneDeep(value);
                                    newValue[index].options[o.key] = bool;
                                    this.setState({
                                        modified: true
                                    });
                                    this.props.onUpdateField(newValue);
                                }}
                                editable = {o.editable}
                                editMode = {this.props.editMode}
                                newline = {false}
                            />)
                        )}
                        </span>
                        {this.props.addOrRemove &&
                            <div style={styles.actionDiv}>
                                <div style={styles.actionDiv}>
                                    <IconButton
                                        style={styles.button}
                                        onTouchTap={() => {
                                            this.onRemove(index)
                                        }}
                                        iconStyle={styles.icon}
                                    >
                                        <RemoveIcon
                                            color='#000'
                                            hoverColor='#64C4E0'
                                        />
                                    </IconButton>
                                </div>
                                <div style={styles.actionDiv}>
                                    {index === value.length-1 &&
                                    <IconButton
                                        style={{
                                            ...styles.button,
                                            left: 36
                                        }}
                                        onTouchTap={() => {
                                            this.onAdd()
                                        }}
                                        iconStyle={styles.icon}
                                    >
                                        <AddIcon
                                            color='#000'
                                            hoverColor='#64C4E0'
                                        />
                                    </IconButton>
                                    }
                                </div>
                            </div>
                        }
                    </span>)
            ) :
                <span>
                    []
                    {this.props.addOrRemove &&
                        <div style={styles.actionDiv}>
                            <IconButton
                                style={{
                                    ...styles.button,
                                    left: 30
                                }}
                                onTouchTap={() => {
                                    this.onAdd()
                                }}
                                iconStyle={styles.icon}
                            >
                                <AddIcon
                                    color='#000'
                                    hoverColor='#64C4E0'
                                />
                            </IconButton>
                        </div>
                    }
                </span>
            }
        </span>
        );
    }

    renderTextField(index, value){
        const id = this.id + '-' + index;
        return (
            <div style={{display: 'inline-block'}}>
                <TextField
                    ref={(ref) => { this.editFields[id] = ref }}
                    id={ id }
                    style={styles.edit}
                    inputStyle={{
                        color: this.getColor(index)
                    }}
                    onKeyDown={(event) => this.keyDown(event, id, index, value)}
                    onBlur={(event) => this.blur(id, index, value)}
                    onChange={(event, newValue) => this.handleChange(index, newValue)}
                    onTouchTap={(event) => {
                        event.stopPropagation();
                        document.getElementById(id).focus();
                    }}
                    value={value}
                    underlineStyle={styles.underline}
                    underlineFocusStyle={styles.underline}
                />
                {this.state.checked[index] && !this.state.valid[index] &&
                <div style={{
                    position: 'relative',
                    display: 'inline-block',
                    width: 0,
                    height: 0,
                    left: 5,
                    top: 2
                }}>
                    <Error
                        tip = {this.state.error[index]}
                        placement="top"
                    />
                </div>
                }
            </div>
        )
    }

    blur = (id, index, value) => {
        this.editFields[id].blur();
        this.checkField(this.props, index, value);
    };

    checkField(props, index, value){
        if(props.titleSchema){
            const validationError = Joi.validate(value, props.titleSchema, {
                skipFunctions: true
            }).error;

            if(validationError){
                this.setState({
                    checked: {
                        ...this.state.checked,
                        [index]: true
                    },
                    valid: {
                        ...this.state.checked,
                        [index]: false
                    },
                    error: {
                        ...this.state.checked,
                        [index]: props.parsingError || validationError.message,
                    }
                });
            }
            else{
                this.setState({
                    checked: {
                        ...this.state.checked,
                        [index]: true
                    },
                    valid: {
                        ...this.state.checked,
                        [index]: true
                    },
                    error: {
                        ...this.state.checked,
                        [index]: null,
                    }
                });
            }
        }
    }

    keyDown = (event, id, index, value) => {
        switch(event.key){
            case 'Enter':
                this.blur(id, index, value);
                break;
            case 'Escape':
                this.blur(id, index, value);
                break;
        }
    };

    handleChange = (index, value) => {
        const newValue = _.cloneDeep(this.getValue());
        newValue[index].title.label = value;
        this.setState({
            modified: true
        });
        this.props.onUpdateField(newValue);
    };

    onAdd(){
        let fieldValue = [...this.getValue()];
        fieldValue.push({
            title: {
                label: null,
                editable: true
            },
            options: {}
        });

        this.props.onUpdateField(fieldValue);
    }

    onRemove(index){
        let fieldValue = [...this.getValue()];
        fieldValue.splice(index, 1);

        this.props.onUpdateField(fieldValue);
    }

    static propTypes = {
        header: PropTypes.string,
        ownValue: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
        configValue: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
        indent: PropTypes.number,
        advanced: PropTypes.bool,
        editable: PropTypes.bool,
        showAdvanced: PropTypes.bool,
        editMode: PropTypes.bool,
        display: PropTypes.func,
        fields: PropTypes.object,
        parsingError: PropTypes.string,
        onUpdateField: PropTypes.function,
        titleSchema: PropTypes.object,
        addOrRemove: PropTypes.bool,
    };

    static defaultProps = {
        indent: 0,
        advanced: false,
        editable: false,
        showAdvanced: false,
        editMode: false,
        display: item => item
    };
}