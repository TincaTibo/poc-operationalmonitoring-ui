import React, {PropTypes} from 'react';
import CheckedIcon from 'material-ui/svg-icons/toggle/check-box';
import UnCheckedIcon from 'material-ui/svg-icons/toggle/check-box-outline-blank';
import Checkbox from 'material-ui/Checkbox';

import Colors from '../../config/colors';

const styles = {
    checkbox: {
        marginBottom: 0,
        position: 'relative',
        top: 3
    },
    fields:{
        fieldHeaderStyle : {
            fontWeight: 'bold',
            fontSize: '12px',
            marginRight: '16px',
            width: '180px',
            display: 'table-cell'
        },
        fieldValueStyle: {
            fontFamily: 'Monospace',
            fontSize: '12px',
            width: '250px',
            textAlign: 'left',
            display: 'table-cell'
        }
    },
    checkedIcon: {
        width: 17,
        height: 17,
        position: 'relative',
        top: 3
    },
    checkBoxIcon: {
        width: 17,
        height: 17,
    }
};

const BoolField = props => {
    if((props.showAdvanced && props.advanced) || !props.advanced){
        return (
            <span style={props.header ? {} : {
                ...styles.fields.fieldValueStyle,
                ...props.valueStyle
            }}>
                {props.header &&
                    <span style={{
                        ...styles.fields.fieldHeaderStyle,
                        ...props.headerStyle
                    }}>
                        {'\u00a0'.repeat(props.indent)}
                        {props.header}:&nbsp;
                    </span>
                }
                <span style={{
                    ...styles.fields.fieldValueStyle,
                    ...props.valueStyle
                }}>
                    {'\u00a0'.repeat(props.indent)}
                    {props.editMode && props.editable ?
                        renderEditor(props) :
                        renderValue(props)
                    }
                </span>
                {props.newline && <br/>}
            </span>
        );
    }
    else{
        return <span/>;
    }
};

function renderValue(props){
    return getValue(props) ?
        <CheckedIcon
            style={{
                ...styles.checkedIcon,
                fill: getColor(props)
            }}/> :
        <UnCheckedIcon
            style={{
                ...styles.checkedIcon,
                fill: getColor(props)
            }}/>;
}

function getValue({newValue, ownValue, configValue}){
    if(newValue !== undefined && newValue !== null){
        return newValue;
    }
    else if(ownValue !== undefined){
        return ownValue;
    }
    else{
        return configValue;
    }
}

function getColor({newValue, ownValue, configValue}){
    if(newValue !== undefined
        && newValue !== null
        && newValue !== (ownValue !== undefined ? ownValue : configValue)){
        return Colors.info;
    }
    else if(ownValue !== undefined){
        return 'black';
    }
    else{
        return Colors.optionSelected;
    }
}

function renderEditor(props){
    return (
        <Checkbox
            style={styles.checkbox}
            checked={getValue(props)}
            iconStyle={{
                ...styles.checkBoxIcon,
                fill: getColor(props)
            }}
            onCheck={(event, bool) => {
                props.onSelect(bool);
            }}
        />
    );
}


BoolField.propTypes = {
    indent: PropTypes.number,
    ownValue: PropTypes.bool,
    configValue: PropTypes.bool,
    newValue: PropTypes.bool,
    advanced: PropTypes.bool,
    editable: PropTypes.bool,
    showAdvanced: PropTypes.bool,
    editMode: PropTypes.bool,
    onSelect: PropTypes.func,
    headerStyle: PropTypes.object,
    valueStyle: PropTypes.object,
    newline: PropTypes.bool,
};

BoolField.defaultProps = {
    indent: 0,
    newline: true,
    advanced: false,
    editable: false,
    showAdvanced: false,
    editMode: false,
    onSelect: () => {},
    headerStyle: {},
    valueStyle: {},
};

export default BoolField;