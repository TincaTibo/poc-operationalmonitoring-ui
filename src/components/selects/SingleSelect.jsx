import React, {Component, PropTypes} from 'react';
import Select from 'react-select';

import './singleselect.css';

const styles = {
    select:{
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0
    }};

export default class SingleAutoSelect extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if(this.props.options.length === 1){
            this.props.onSelect(this.props.options[0]);
        }
    }

    render() {
        return (
            <div
                id={`div-select-${this.props.name}`}
                style={{...styles.select, ...this.props.style}}>
                <Select
                    className="single"
                    name={this.props.name}
                    value={this.props.value}
                    multi={false}
                    options={this.props.options}
                    placeholder={this.props.placeholder}
                    onChange={this.props.onSelect}
                    menuBuffer={100}
                    clearable={this.props.clearable}
                    disabled={this.props.disabled}
                />
            </div>
        );
    }

    static propTypes = {
        name: PropTypes.string.isRequired,
        options: PropTypes.array,
        value: PropTypes.string,
        placeholder: PropTypes.any,
        clearable: PropTypes.bool,
        style: PropTypes.object,
        disabled: PropTypes.bool,

        onSelect: PropTypes.func
    };

    static defaultProps = {
        clearable: true,
    }
}