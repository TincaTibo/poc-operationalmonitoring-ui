import React, {PropTypes} from 'react';
import { Creatable } from 'react-select';
import './multiselect.css';

const styles = {
    select:{
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 10
    }
};


const EditableSelect = props => (
    <div
        id={`div-select-${props.name}`}
        style={{...styles.select, ...props.style}}
    >
        <Creatable
            name={props.name}
            className={props.background === 'white' ? 'white' : null}
            value={props.value}
            multi={true}
            options={props.options}
            placeholder={props.placeholder}
            onOpen={props.onLoad}
            onChange={props.onSelect}
            isLoading={props.requesting}
            menuBuffer={100}
            disabled={props.disabled}

            isValidNewOption={props.onValidateInput}
            promptTextCreator={props.onPromptText}
            newOptionCreator={props.onCreateInput}
            onNewOptionClick={props.onNewOptionClick}
        />
    </div>
);

EditableSelect.propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.array,
    value: PropTypes.string,
    requesting: PropTypes.bool,
    placeholder: PropTypes.any,
    disabled: PropTypes.bool,
    background: PropTypes.string,

    onLoad: PropTypes.func,
    onSelect: PropTypes.func,
    onValidateInput: PropTypes.func,
    onPromptText: PropTypes.func,
    onCreateInput: PropTypes.func,
    onNewOptionClick: PropTypes.func,
};

export default EditableSelect;