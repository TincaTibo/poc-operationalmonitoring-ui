import React, { PropTypes, PureComponent } from 'react';

import Select from 'react-select';
import './multiselect.css';

const styles = {
    select:{
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 10
    }
};

export default class MultiSelect extends PureComponent {
    constructor(props){
        super(props);
        this.select = null;
    }

    componentDidMount(){
        if(this.props.focusOnMount){
            this.select.focus();
        }
    }

    render(){
        return (
            <div
                id={`div-select-${this.props.name}`}
                style={{...styles.select, ...this.props.style}}>
                <Select
                    ref={(value) => this.select = value}
                    name={this.props.name}
                    className={this.props.background === 'white' ? 'white' : null}
                    value={this.props.value}
                    multi={true}
                    closeOnSelect={false}
                    options={this.props.options}
                    placeholder={this.props.placeholder}
                    onOpen={this.props.onLoad}
                    onChange={this.props.onSelect}
                    isLoading={this.props.requesting}
                    menuBuffer={100}
                    disabled={this.props.disabled}
                    openOnFocus={this.props.focusOnMount}
                />
            </div>
        );
    }

    static propTypes = {
        name: PropTypes.string.isRequired,
        options: PropTypes.array,
        value: PropTypes.string,
        requesting: PropTypes.bool,
        placeholder: PropTypes.any,
        disabled: PropTypes.bool,
        background: PropTypes.string,
        focusOnMount: PropTypes.bool,

        onLoad: PropTypes.func,
        onSelect: PropTypes.func
    };
}