import React, { Component, PropTypes } from 'react';
import IconButton from 'material-ui/IconButton';
import SearchIcon from 'material-ui/svg-icons/action/search';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import SaveIcon from 'material-ui/svg-icons/content/save';
import UndoIcon from 'material-ui/svg-icons/content/undo';
import Button from 'material-ui/FlatButton';
import Error from '../tips/Error';
import ToolTip from 'rc-tooltip';
import '../tips/tipDark.css';

import LuceneEdit from '../lucene-edit';

const colors= {
    hoverColor: '#64C4E0',
    lowerButton: 'lightGrey'
};

const styles = {
    searchField:{
        width: 500,
        height: 15,
    },
    iconButton:{
        height: 30,
        position: 'relative',
        top: 3,
        padding: 0,
        marginLeft: 5,
        width: 25
    },
    button: {
        display: 'inline-block',
        width: 30,
        height: 30,
        minWidth: 30,
        padding: 0,
        marginLeft: 5,
        position: 'relative',
        top: -2,
        textAlign: 'center'
    },
};

export default class Search extends Component {

    constructor(props) {
        super(props);

        this.state = {
            query: this.props.query,
        };
    }


    handleChange = newValue => {
        this.setState({
            query: newValue,
        });
    };

    componentWillReceiveProps(nextProps){
        if(this.state.query !== nextProps.query){
            this.setState({query: nextProps.query});
        }
    }

    render() {
        return (
            <div
                style={this.props.style}
            >
                <div style={{
                    backgroundColor: 'white',
                    borderRadius: 5,
                    paddingBottom: 3,
                    display: 'inline-block'
                }}>
                    {/* Search button */}
                    <ToolTip
                        placement={'bottom'}
                        prefixCls={'rc-tooltip-dark'}
                        overlay={'Search'}
                        mouseEnterDelay={0.5}
                        destroyTooltipOnHide={true}
                    >
                        <IconButton
                            style={styles.iconButton}
                            onTouchTap={() => {this.props.onSearch(this.state.query)}}
                        >
                            <SearchIcon
                                hoverColor={colors.hoverColor}
                            />
                        </IconButton>
                    </ToolTip>
                    <div
                        style={{
                            display: 'inline-block',
                            marginLeft: 0,
                            width: this.props.width - 280
                        }}
                    >
                        <LuceneEdit
                            id={`LuceneEdit-${this.props.view}`}
                            value={this.state.query}
                            style={{
                                ...styles.searchField,
                                width: '100%'
                            }}
                            width={this.props.width - 245}
                            fontSize={12}
                            view={this.props.view}
                            onValid={this.props.onSearch}
                            onChange={this.handleChange}
                            onFocus={() => {
                                this.setState({focused: true});
                            }}
                            onBlur={() => this.setState({
                                focused: false,
                                query: this.props.query
                            })}
                        />
                        {
                            this.state.query === '' &&
                            <div style={{
                                width: 0,
                                height: 0,
                                position: 'absolute',
                                top: 11,
                                left: 35,
                                color: 'lightgray',
                                fontFamily: 'monospace',
                                pointerEvents: 'none'
                            }}>
                                    <span>
                                        Filter&nbsp;results...
                                    </span>
                            </div>
                        }
                    </div>
                    {/* Error icon */}
                    {this.props.error &&
                    this.state.query === this.props.query &&
                    <div style={{
                        display: 'inline-block',
                    }}>
                        <Error
                            tip={this.props.error}
                            placement='bottom'
                        />
                    </div>
                    }
                </div>
                {/* Clear button */}
                <ToolTip
                    placement={'bottom'}
                    prefixCls={'rc-tooltip-dark'}
                    overlay={'Clear'}
                    mouseEnterDelay={0.5}
                    destroyTooltipOnHide={true}
                >
                    <Button
                        icon={
                            <ClearIcon
                                color='#FFFFFF'
                                style={{
                                    height: 25,
                                    width: 25,
                                    position: 'relative',
                                    top: -3
                                }}
                            />
                        }
                        onTouchTap={() => {
                            this.setState({
                                query: ''
                            });
                            this.props.onClear();
                        }}
                        style={styles.button}
                        backgroundColor={'#ff5031'}
                        hoverColor={'#ff8e7a'}
                    />
                </ToolTip>
                {/* If history, show undo button (browser back) */}
                <ToolTip
                    placement={'bottom'}
                    prefixCls={'rc-tooltip-dark'}
                    overlay={'Undo'}
                    mouseEnterDelay={0.5}
                    destroyTooltipOnHide={true}
                >
                    <Button
                        icon={
                            <UndoIcon
                                color='#FFFFFF'
                                style={{
                                    height: 25,
                                    width: 25,
                                    position: 'relative',
                                    top: -3
                                }}
                            />
                        }
                        onTouchTap={this.props.onUndo}
                        style={styles.button}
                        backgroundColor={this.props.historySteps > 0 ? '#16ff36' : '#bfbfbf'}
                        hoverColor={'#97ff91'}
                        disabled={this.props.historySteps === 0}
                    />
                </ToolTip>
                {/* Save */}
                <ToolTip
                    placement={'bottom'}
                    prefixCls={'rc-tooltip-dark'}
                    overlay={'Save query'}
                    mouseEnterDelay={0.5}
                    destroyTooltipOnHide={true}
                >
                    <Button
                        icon={
                            <SaveIcon
                                color='#FFFFFF'
                                style={{
                                    height: 25,
                                    width: 25,
                                    position: 'relative',
                                    top: -3
                                }}
                            />
                        }
                        onTouchTap={() => {}}
                        style={styles.button}
                        backgroundColor={(this.props.query && !this.props.error) ? '#16ff36' : '#bfbfbf'}
                        hoverColor={'#97ff91'}
                        disabled={!this.props.query || this.props.error}
                    />
                </ToolTip>
                <Button
                    label={'SEARCH'}
                    labelStyle={{
                        color: 'white',
                        lineHeight: '13px',
                        fontSize: '13px',
                        fontFamily: 'Roboto, sans-serif',
                        position: 'relative',
                        top: -3
                    }}
                    onTouchTap={this.props.onSearch}
                    style={{
                        ...styles.button,
                        width: 100
                    }}
                    backgroundColor={'#2d95e0'}
                    hoverColor={'#a3bee0'}
                />
            </div>
        );
    }

    static propTypes = {
        style: PropTypes.object,
        query: PropTypes.string,
        error: PropTypes.string,
        historySteps: PropTypes.number,
        view: PropTypes.string,
        width: PropTypes.number,
        onSearch: PropTypes.func.isRequired,
        onClear: PropTypes.func.isRequired,
        onUndo: PropTypes.func.isRequired,
    };

    static defaultProps = {
        onSearch: ()=>{},
        query: '',
    }
}