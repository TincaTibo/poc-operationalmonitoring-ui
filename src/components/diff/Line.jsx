import React, {PropTypes} from 'react';
import {TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {diffChars, diffWords, diffLines} from 'diff';

const styles = {
    labelCellStyle: {
        paddingLeft: 5,
        paddingRight: 5,
        height: 24,
        textAlign: 'left',
        width: 140,
        minWidth: 140,
        fontFamily: 'Roboto, sans-serif',
        textOverflow: 'ellipsis',
        whiteSpace: 'unset',
        fontSize: '13px'
    },
    valueCellStyle: {
        paddingLeft: 5,
        paddingRight: 5,
        height: 24,
        textAlign: 'left',
        minWidth: 170,
        overflow: 'unset',
        wordBreak: 'break-word',
        whiteSpace: 'unset'
    },
};

const Line = props => {
    const origin = props.items && props.items.length && props.value ?
        props.value(props.items[0])
        : undefined;

    return (
        <TableRow style={{
            height: '30px'
        }}>
            <TableHeaderColumn
                style={styles.labelCellStyle}
            >
                {props.header}
            </TableHeaderColumn>
            {props.items.map(item => (
                    <TableRowColumn
                        key={item['@id']}
                        style={{
                            ...styles.valueCellStyle,
                            ...props.style,
                            width: props.cellWidth,
                            backgroundColor: props.diffBackgroundColor && props.value ?
                                props.value(item) !== origin ?
                                    '#f5da9a'
                                    : undefined
                                : undefined
                        }}
                    >
                        { renderItem(props, item, origin) }
                    </TableRowColumn>
                )
            )}
        </TableRow>
    );
};

Line.propTypes = {
    header: PropTypes.string,
    items: PropTypes.array,
    render: PropTypes.func,
    value: PropTypes.func,
    diffBackgroundColor: PropTypes.bool,
    diff: PropTypes.string,
    style: PropTypes.object,
    cellWidth: PropTypes.number,
};

Line.defaultProps = {
    cellWidth: 170,
};

function renderItem(props, item, origin){
    if(props.render){
        return props.render(item)
    }
    else if(props.value){
        if(props.diff && origin && props.value(item)){
            let diff;
            switch(props.diff){
                case 'CHARS':
                    diff = diffChars(origin+'', props.value(item)+'');
                    break;
                case 'WORDS':
                    diff = diffWords(origin+'', props.value(item)+'');
                    break;
                case 'LINES':
                    diff = diffLines(origin+'', props.value(item)+'');
                    break;
            }
            return diff.map((part, idx) => {
                // green for additions, red for deletions
                // grey for common parts
                const color = part.added ?
                    'green'
                    : part.removed ?
                        'red'
                        : 'black';
                return (
                    <span
                        key={idx}
                        style={{
                            color,
                            textDecoration: part.removed ? 'line-through' : undefined
                        }}>
                        {part.value}
                    </span>
                );
            });
        }
        else{
            return props.value(item)
        }
    }
    else{
        console.error('Can\'t render value');
        return '';
    }
}

export default Line;