import React, {Component, PropTypes} from 'react';
import Subheader from 'material-ui/Subheader';
import {Table, TableBody} from 'material-ui/Table';

const styles = {
};

export default class Section extends Component {

    constructor(props) {
        super(props);

        this.state = {};
    }

    componentWillReceiveProps(nextProps) {
    }

    render() {
        return (
            <div>
                {this.props.header &&
                <Subheader>
                    {this.props.header}
                </Subheader>
                }
                <Table fixedHeader={false}
                       showCheckboxes={false}
                       selectable={false}
                       style={{
                           display: 'inline-block'
                       }}
                       wrapperStyle={{
                           overflowY: undefined,
                           overflowX: undefined,
                       }}
                       bodyStyle={{
                           overflowX: undefined,
                           overflowY: undefined,
                       }}
                >
                    <TableBody displayRowCheckbox={false}
                               showRowHover={false}
                    >
                        {this.props.children}
                    </TableBody>
                </Table>
            </div>
        );
    }

    static propTypes = {
        header: PropTypes.string,
    };

    static defaultProps = {}
}