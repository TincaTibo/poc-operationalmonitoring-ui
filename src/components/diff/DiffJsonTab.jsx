import React, { PureComponent, PropTypes } from 'react';
import {diffLines} from 'diff';
import Checkbox from 'material-ui/Checkbox';
import _ from 'lodash';

import Section from './Section';
import Line from './Line';

const styles = {
    radioButtonStyle:{
        display: 'inline-flex',
        width: 'auto',
        marginLeft: 0,
        marginRight: 30,
        fontFamily: 'Roboto, sans-serif',
        fontSize: '13px'
    },
    checkbox: {
        marginBottom: 0,
    },
    checkBoxIcon: {
        width: 17,
        height: 17
    }
};

export default class DiffJsonTab extends PureComponent {

    constructor(props){
        super(props);

        const checked = props.selected.slice(0,2).map(log => log['@id']);
        this.state = {
            checked,
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.selected !== this.props.selected && nextProps.selected){
            const checked = nextProps.selected.slice(0,2).map(log => log['@id']);
            this.setState({
                checked,
            });
        }
    }

    render() {
        const diff = this.props.selected
            && this.state.checked.length === 2 ?
                diffLines(
                    this.format(_.find(this.props.selected,{'@id': this.state.checked[0]})),
                    this.format(_.find(this.props.selected,{'@id': this.state.checked[1]}))
                )
            : [];

        return (
            <div style={{
                width: '100%',
                height: '100%',
                overflow : 'auto'
            }}>
                <div
                    style={{
                        width: '100%',
                        overflowX: 'auto',
                        paddingLeft: 10,
                        paddingTop: 10
                    }}
                >
                    <Section>
                        <Line
                            header='Id'
                            items={this.props.selected}
                            value={log => log['@id']}
                            cellWidth={this.props.cellWidth}
                        />
                        {this.props.selected.length > 2 &&
                            <Line
                                header='Compare content'
                                items={this.props.selected}
                                diffBackgroundColor
                                value={log => JSON.stringify(log)}
                                render={log => {
                                    return (
                                        <Checkbox
                                            style={styles.checkbox}
                                            iconStyle={styles.checkBoxIcon}
                                            checked={this.state.checked.indexOf(log['@id'])>-1}
                                            onCheck={(event, checked) => {
                                                if(checked){
                                                    if(this.state.checked.length >= 1){
                                                        this.setState({
                                                            checked: [
                                                                this.state.checked[0],
                                                                log['@id']
                                                            ]
                                                        })
                                                    }
                                                    else {
                                                        this.setState({
                                                            checked: [
                                                                log['@id']
                                                            ]
                                                        })
                                                    }
                                                }
                                                else{
                                                    const checked = [...this.state.checked];
                                                    checked.splice(this.state.checked.indexOf(log['@id']),1);
                                                    this.setState({
                                                        checked
                                                    })
                                                }
                                                event.stopPropagation();
                                            }}
                                        />
                                    );
                                }}
                                cellWidth={this.props.cellWidth}
                            />
                        }
                    </Section>
                </div>
                <div
                    style={{
                        paddingTop: 10,
                        paddingLeft: 10,
                        paddingRight: 10,
                        width: '100%',
                        height: `calc(100% - ${this.props.selected.length > 2 ? 75 : 45}px)`,
                        overflowX: 'unset',
                        overflowY: 'auto',
                        wordBreak: 'break-all',
                        whiteSpace: 'pre-wrap',
                        fontFamily: 'Monaco, Menlo, "Ubuntu Mono", Consolas, source-code-pro, monospace',
                        fontSize: '12px',
                    }}
                >
                    {diff.map((part, idx) => {
                        // green for additions, red for deletions
                        // grey for common parts
                        const color = part.added ?
                            'green'
                            : part.removed ?
                                'red'
                                : 'black';
                        return (
                            <span
                                key={idx}
                                style={{
                                    color,
                                    textDecoration: part.removed ? 'line-through' : undefined
                                }}>
                                {part.value}
                            </span>
                        );
                    })}
                </div>
            </div>
        )
    }

    format(source){
        return JSON.stringify(source, null, 4);
    }

    static propTypes = {
        selected: PropTypes.array,
        cellWidth: PropTypes.number,
    };
}

