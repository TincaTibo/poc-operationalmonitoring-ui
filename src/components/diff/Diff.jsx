import React, { Component, PropTypes } from 'react';

import Layout from '../details/ResourceWithTabsContainer';
import DiffLogGlobalTabContainer from './LOG/DiffLogGlobalTabContainer';
import DiffJsonTabContainer from './DiffJsonTabContainer';

const styles = {
};

export default class Diff extends Component {

    renderTitle = () => {
        return `Compare ${this.props.count} ${this.props.resourceType}${this.props.count>1?'s':''}`;
    };

    componentWillReceiveProps(nextProps){
        if(nextProps.view !== this.props.view){
            nextProps.onChangeTab(0);
        }
    }

    render(){
        let cellWidth = (this.props.width - 140 - 10 - 25 ) / this.props.count;
        cellWidth = cellWidth < 170 ? 170 : cellWidth;

        switch(this.props.view){
            case 'LOG':
                return (
                    <Layout title={this.renderTitle}
                            propsToWait={[]}
                            activeTab={this.props.activeTab}
                            onChangeActiveTab={this.props.onChangeTab}
                    >
                        <DiffLogGlobalTabContainer
                            tabTitle = 'Global'
                            cellWidth = {cellWidth}
                        />
                        <DiffJsonTabContainer
                            tabTitle = 'Source diff'
                            cellWidth = {cellWidth}
                        />
                    </Layout>
                );
            default:
                return (
                    <Layout title={this.renderTitle}
                            propsToWait={[]}
                            activeTab={this.props.activeTab}
                            onChangeActiveTab={this.props.onChangeTab}
                    >
                        <DiffJsonTabContainer
                            tabTitle = 'Source diff'
                            cellWidth = {cellWidth}
                        />
                    </Layout>
                );
        }
    }

    static propTypes = {
        id: PropTypes.string.isRequired,
        activeTab: PropTypes.number,
        view: PropTypes.string,
        resourceType: PropTypes.string,
        count: PropTypes.number,
        onChangeTab: PropTypes.func.isRequired,
    };
}
