import {connect} from 'react-redux';
import DiffLogGlobalTab from './DiffLogGlobalTab';

const mapStateToProps = (state, ownProps) => {
    return {
        selected: state.details.links && state.details.links.selected,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const DiffLogGlobalTabContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DiffLogGlobalTab);

export default DiffLogGlobalTabContainer;