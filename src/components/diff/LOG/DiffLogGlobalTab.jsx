import React, { PropTypes } from 'react';

import Section from '../Section';
import Line from '../Line';

import {DATETIME_FORMAT_MINUTE} from '../../../config/locale';
import ItemShortViewContainer from '../../../containers/view/ItemShortViewContainer';
import _ from 'lodash';

const styles = {
};

const DiffLogGlobalTab = (props) => {
    return (
        <div style={{
            width: '100%',
            height: '100%',
            overflow : 'auto',
            paddingLeft: 10,
            paddingTop: 10
        }}>
            <Section>
                <Line
                    header='Id'
                    items={props.selected}
                    value={log => log['@id']}
                    cellWidth={props.cellWidth}
                />
            </Section>
            <Section header="Device">
                <Line
                    header='Park'
                    items={props.selected}
                    diffBackgroundColor
                    value={log => <ItemShortViewContainer item={log.park}/>}
                    cellWidth={props.cellWidth}
                />
                <Line
                    header='Terminal'
                    items={props.selected}
                    diffBackgroundColor
                    value={log => <ItemShortViewContainer item={log.pos}/>}
                    cellWidth={props.cellWidth}
                />
            </Section>
            <Section header="Status">
                <Line
                    header='Event ID'
                    items={props.selected}
                    diffBackgroundColor
                    diff="CHARS"
                    value={log => log.eventName}
                    cellWidth={props.cellWidth}
                />
                <Line
                    header='Event Status'
                    items={props.selected}
                    diffBackgroundColor
                    diff="WORDS"
                    value={log => log.eventStatus && log.eventStatus.replace('sp:','')}
                    cellWidth={props.cellWidth}
                />
                <Line
                    header='Event Type'
                    items={props.selected}
                    diffBackgroundColor
                    diff="WORDS"
                    value={log => log.eventType && log.eventType.replace('sp:','')}
                    cellWidth={props.cellWidth}
                />
                <Line
                    header='Family'
                    items={props.selected}
                    diffBackgroundColor
                    diff="WORDS"
                    value={log => log.peripheralType && log.peripheralType.replace('sp:','')}
                    cellWidth={props.cellWidth}
                />
            </Section>
            <Section header="Dates">
                <Line
                    header='Start date'
                    items={props.selected}
                    diffBackgroundColor
                    diff="CHARS"
                    value={log => log.startTime.format(DATETIME_FORMAT_MINUTE)}
                    cellWidth={props.cellWidth}
                />
                <Line
                    header='End date'
                    items={props.selected}
                    diffBackgroundColor
                    diff="CHARS"
                    value={log => log.endTime && log.endTime.format(DATETIME_FORMAT_MINUTE)}
                    cellWidth={props.cellWidth}
                />
                <Line
                    header='Update date'
                    items={props.selected}
                    diffBackgroundColor
                    diff="CHARS"
                    value={log => log.updateDateTime && log.updateDateTime.format(DATETIME_FORMAT_MINUTE)}
                    cellWidth={props.cellWidth}
                />
            </Section>
        </div>
    )
};

function renderOneToMany(items, render){
    if(items){
        const  max = items.length-1;
        if(_.isArray(items) && items.length){
            return (
                <span>
                    {items.map((item, idx) => (
                        <span key={item}>
                            {render ? render(item) : item}
                            {idx < max && <br/>}
                        </span>
                    ))}
                </span>
            )
        }
        else if(_.isString(items)){
            return render ? render(items) : items;
        }
        else {
            return '';
        }
    }
    else {
        return '';
    }
}

DiffLogGlobalTab.propTypes = {
    selected: PropTypes.array,
    cellWidth: PropTypes.number,
};

function renderLogLevel(level){
    switch(level){
        case 10: return 'TRACE';
        case 20: return 'DEBUG';
        case 30: return 'INFO';
        case 40: return 'WARNING';
        case 50: return 'ERROR';
        case 60: return 'FATAL';
    }
}

export default DiffLogGlobalTab;

