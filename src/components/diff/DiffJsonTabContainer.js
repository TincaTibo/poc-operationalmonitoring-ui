import {connect} from 'react-redux';
import DiffJsonTab from './DiffJsonTab';

const mapStateToProps = (state, ownProps) => {
    return {
        selected: state.details.links && state.details.links.selected,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const DiffJsonTabContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DiffJsonTab);

export default DiffJsonTabContainer;