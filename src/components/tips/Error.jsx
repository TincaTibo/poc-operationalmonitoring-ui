import React, { PropTypes } from 'react';
import ErrorIcon from 'material-ui/svg-icons/alert/error';
import ToolTip from 'rc-tooltip';
import './tipError.css';

const styles={
    icon:{
        width: 14,
        height: 14,
        padding: 0,
    },
};

const Error = props => (
    <ToolTip
        placement={props.placement || 'right'}
        prefixCls='rc-tooltip-error'
        overlay={props.tip}
        destroyTooltipOnHide={true}
    >
        <ErrorIcon
            style={styles.icon}
            color={props.color || '#f65d5c'}
            hoverColor={props.hoverColor || '#ff9b9e'}
            viewBox='0 0 25 25'
        />
    </ToolTip>
);

Error.propTypes = {
    placement: PropTypes.string,
    tip: PropTypes.any,
    color: PropTypes.string,
    hoverColor: PropTypes.string
};

export default Error;