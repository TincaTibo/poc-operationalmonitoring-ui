import React, { PropTypes } from 'react';
import InfoIcon from 'material-ui/svg-icons/action/info';
import ToolTip from 'rc-tooltip';
import './tipInfo.css';

const styles={
    icon:{
        width: 14,
        height: 14,
        padding: 0,
    },
};

const Tip = props => (
    <ToolTip
        placement='right'
        prefixCls='rc-tooltip-info'
        overlay={props.tip}
        destroyTooltipOnHide={true}
    >
        <InfoIcon
            style={styles.icon}
            color={props.color || '#FFFFFF'}
            hoverColor={props.hoverColor || '#55C046'}
            viewBox='0 0 25 25'
        />
    </ToolTip>
);

Tip.propTypes = {
    tip: PropTypes.string,
    color: PropTypes.string,
    hoverColor: PropTypes.string
};

export default Tip;