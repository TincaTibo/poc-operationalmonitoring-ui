import { connect } from 'react-redux';
import ResourceWithNameAndBreadCrumb from './ResourceWithNameAndBreadCrumb';

const mapStateToProps = (state, ownProps) => {
    return {
        id: state.details.id,
        errorMessage: state.details.links && state.details.links.errorMessage,
        hasHistory: state.details.history.length > 1,
        propsToWait: ownProps.propsToWait,
        children: ownProps.children
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        title: ownProps.title
    }
};

const ResourceWithNameAndBreadCrumbContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ResourceWithNameAndBreadCrumb);

export default ResourceWithNameAndBreadCrumbContainer;