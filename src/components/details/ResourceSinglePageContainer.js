import { connect } from 'react-redux';
import ResourceSinglePage from './ResourceSinglePage';

const mapStateToProps = (state, ownProps) => {
    return {
        hasHistory: state.details.history.length > 1,
        propsToWait: ownProps.propsToWait,
        children: ownProps.children
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        title: ownProps.title
    }
};

const ResourceSinglePageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ResourceSinglePage);

export default ResourceSinglePageContainer;