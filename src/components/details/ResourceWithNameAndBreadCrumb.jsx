import React, { Component, PropTypes } from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import IconButton from 'material-ui/IconButton';
import DownloadIcon from 'material-ui/svg-icons/file/file-download';

import BreadcrumbContainer from '../bread-crumb/BreadCrumbContainer';

const ResourceWithNameAndBreadCrumb = (props) => {
        //TODO: format as an error
        if (props.errorMessage) {
            return <span>{props.errorMessage}</span>
        }

        if (props.propsToWait && !props.propsToWait.every(v => (v !== undefined && v !== null))) {

            return <div style={{
                height: '100%',
                width: '100%'
            }}>
                <RefreshIndicator
                    size={40}
                    left={10}
                    top={0}
                    status="loading"
                    style={{
                        display: 'inline-block',
                        position: 'relative',
                        margin: 'auto'
                    }}
                />
            </div>
        }

        return <div style={{
            height: '100%',
            width: '100%'
        }}>
            {/* Title of Panel */}
            <div style={{
                height: '20px',
                borderBottom: '1px solid lightGrey',
                paddingLeft: '10px',
                paddingTop: '6px',
                paddingBottom: '1px',
                width: 'calc(100% - 10px)',
                fontSize: '14px'
            }}>
                {props.title()}
             </div>
            {/* Breadcrumb */}
            {props.hasHistory &&
            <div style={{
                height: '17px',
                borderBottom: '1px solid lightGrey',
                paddingLeft: '10px',
                paddingTop: '3px',
                paddingBottom: '2px',
                width: 'calc(100% - 10px)'
            }}>
                <BreadcrumbContainer/>
            </div>
            }
            {props.children}
        </div>
};

ResourceWithNameAndBreadCrumb.propTypes = {
    id: PropTypes.string.isRequired,
    errorMessage: PropTypes.string,
    hasHistory: PropTypes.bool.isRequired,
    propsToWait: PropTypes.array,

    title: PropTypes.func.isRequired,
    onDownload: PropTypes.func
};

ResourceWithNameAndBreadCrumb.defaultProps = {
};

export default ResourceWithNameAndBreadCrumb;