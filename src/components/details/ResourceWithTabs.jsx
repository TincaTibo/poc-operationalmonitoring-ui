import React, { Component, PropTypes } from 'react';
import TabPanel, { TabBody, TabStrip } from 'react-tab-panel';
import 'react-tab-panel/index.css';

import Layout from './ResourceWithNameAndBreadcrumbContainer';

export default class ResourceWithTabs extends Component {

    constructor(props){
        super(props);

        this.state = {
            activeTab: 0,
        };
    }

    handleActiveTab = (index) => {
        if(this.props.onChangeActiveTab){
            this.props.onChangeActiveTab(index);
        }
        else{
            this.setState({activeTab: index});
        }
    };

    render() {
        return <Layout
            title={this.props.title}
            propsToWait={this.props.propsToWait}
            onDownload={this.props.onDownload}
        >
            <TabPanel
                tabPosition="top"
                activeIndex={this.props.activeTab || this.state.activeTab}
                onActivate={this.handleActiveTab}
                style={{
                    height: `calc(100% - ${this.props.hasHistory ? 51 : 28}px)`,
                    width:'100%'
                }}
            >
                <TabBody
                    style={{
                        height: '100%',
                        width:'100%',
                        padding: 0,
                        backgroundColor: 'white',
                        overflowY: 'auto'
                    }}
                >
                    {this.props.children}
                </TabBody>
                <TabStrip/>
            </TabPanel>
        </Layout>
    }

    static propTypes = {
        title: PropTypes.func.isRequired,
        propsToWait: PropTypes.array,
        activeTab: PropTypes.number,
        hasHistory: PropTypes.bool,
        onChangeActiveTab: PropTypes.func,
        onDownload: PropTypes.func,
    };

    static defaultProps = {
        hasHistory: false
    };
}