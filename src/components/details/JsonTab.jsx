import React, { PureComponent, PropTypes } from 'react';
import * as ace from 'brace';
import 'brace/mode/json';
import 'brace/ext/searchbox';
import Sha1 from 'sha1';

export default class JsonTab extends PureComponent {
    constructor(props){
        super(props);
        this.text = JSON.stringify(props.object, null, 4) || '';
        this.sha1 = Sha1(this.text);
    }

    componentWillReceiveProps(nextProps){
        const text = JSON.stringify(nextProps.object, null, 4);
        const sha1 = Sha1(text);
        if(sha1 !== this.sha1){
            this.sha1 = sha1;
            this.text = text;
            this.updateText();
        }
        if(this.props.width !== nextProps.width){
            const editor = ace.edit(`editor-${this.props.id}`);
            editor.resize();
        }
    }

    shouldComponentUpdate(){
        return false;
    }

    render() {
        return <div
            id={`editor-${this.props.id}`}
            style={{
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                ...this.props.style,
            }}
        >
            {this.text}
        </div>
    }

    componentDidMount(){
        this.activateEditor();
    }

    activateEditor(){
        const editor = ace.edit(`editor-${this.props.id}`);
        editor.getSession().setMode("ace/mode/json");
        editor.setReadOnly(true);
        editor.setHighlightActiveLine(false);
        editor.getSession().setUseWrapMode(true);
        editor.setShowPrintMargin(false);
        editor.setShowInvisibles(false);
    }

    updateText(){
        const editor = ace.edit(`editor-${this.props.id}`);
        editor.setValue(this.text);
        editor.gotoLine(0,0,false);
    }

    static propTypes = {
        object: PropTypes.object,
        style: PropTypes.object,
        id: PropTypes.string,
        width: PropTypes.number,
    };

    static defaultProps = {
        id: ''
    }
}


