import React, { Component, PropTypes } from 'react';
import Drawer from 'material-ui/Drawer';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import PinIcon from '../icons/Pin';
import {select, drag, event} from 'd3';
import ToolTip from 'rc-tooltip';
import '../tips/tipDark.css';

import Colors from '../../config/colors';

const styles = {
    button: {
        border: '10px',
        display: 'inline-block',
        cursor: 'pointer',
        margin: 0,
        padding: 0,
        position: 'relative',
        overflow: 'visible',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        width: 35,
        height: 25,
    }
};

export default class DetailsDrawer extends Component {

    constructor(props){
        super(props);
        this.resizeHandle = null;
    }

    componentDidMount(){
        let selector = select(this.resizeHandle);

        //associate a drag behavior
        selector.call(drag()
            .container(document.getElementById('root'))
            .on('drag',() => {
                this.props.resizeDrawer(event.dx);
            })
            .on('end',() => {
                this.props.onEndResizeDrawer();
            })
        )
    }

    renderButtons(){
        return(
            <div id='drawerButtons' style={{
                position: 'absolute',
                right:'10px',
                top:'0px',
                zIndex:1400
            }}>
                <ToolTip
                    placement={'bottomLeft'}
                    prefixCls={'rc-tooltip-dark'}
                    overlay={this.props.pinned ? 'Unpin drawer' : 'Pin drawer'}
                    mouseEnterDelay={0.5}
                    destroyTooltipOnHide={true}
                >
                    <IconButton
                        style={{
                            ...styles.button,
                            transform: this.props.pinned && 'rotate(45deg)'
                        }}
                        onTouchTap={() => this.props.onPinDrawer(!this.props.pinned)}
                    >
                        <PinIcon
                            hoverColor={Colors.iconHover}
                        />
                    </IconButton>
                </ToolTip>
                <ToolTip
                    placement={'bottomLeft'}
                    prefixCls={'rc-tooltip-dark'}
                    overlay={'Close'}
                    mouseEnterDelay={0.5}
                    destroyTooltipOnHide={true}
                >
                    <IconButton
                        style={styles.button}
                        onTouchTap={this.props.closeDrawer}
                    >
                        <CloseIcon
                            hoverColor={Colors.iconHover}
                        />
                    </IconButton>
                </ToolTip>
            </div>
        );
    }

    render() {
        return (
            <Drawer open={this.props.open}
                    docked={true}
                    onRequestChange={(open) => {
                        if(!open){
                            this.props.closeDrawer();
                        }
                    }}
                    openSecondary={true}
                    width={this.props.width}
                    zDepth={this.props.pinned ? 0 : 2}
                    containerStyle={{
                        borderLeft: this.props.pinned && 'solid 1px lightGrey',
                        overflow: 'hidden'
                    }}
            >
                <div id='drawerResizeHandle'
                     ref={value => this.resizeHandle = value}
                     style={{
                         position: 'absolute',
                         top:'0px',
                         height: '100%',
                         width:'4px',
                         backgroundColor:'transparent',
                         zIndex:1400,
                         cursor: 'col-resize',
                         textAlign: 'center'
                     }}
                >
                </div>
                {this.renderButtons()}
                {this.props.children}
            </Drawer>
        );
    }

    static propTypes = {
        open: PropTypes.bool,
        pinned: PropTypes.bool,
        closeDrawer: PropTypes.func.isRequired,
        resizeDrawer: PropTypes.func.isRequired,
        onEndResizeDrawer: PropTypes.func.isRequired,
        onPinDrawer: PropTypes.func.isRequired,
        width: PropTypes.number
    };

    static defaultProps = {
        open: false,
        width: 600
    };
}
