import React, { Component, PropTypes } from 'react';

import Layout from './ResourceWithNameAndBreadcrumbContainer';

const ResourceSinglePage = (props) => (
    <Layout
            title={props.title}
            propsToWait={props.propsToWait}
        >
            <div style={{
                    height: `calc(100% - ${props.hasHistory ? 51 : 28}px)`,
                    width: '100%'
                }}
                >
                {props.children}
            </div>
        </Layout>
);

ResourceSinglePage.propTypes = {
    title: PropTypes.func.isRequired,
    propsToWait: PropTypes.array,
    hasHistory: PropTypes.bool
};

ResourceSinglePage.defaultProps = {
    hasHistory: false
};

export default ResourceSinglePage;