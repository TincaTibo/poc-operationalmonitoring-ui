import { connect } from 'react-redux';
import ResourceWithTabs from './ResourceWithTabs';

const mapStateToProps = (state, ownProps) => {
    return {
        hasHistory: state.details.history.length > 1,
        propsToWait: ownProps.propsToWait,
        activeTab: ownProps.activeTab,
        children: ownProps.children
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        title: ownProps.title,
        onChangeActiveTab: ownProps.onChangeActiveTab
    }
};

const ResourceWithTabsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ResourceWithTabs);

export default ResourceWithTabsContainer;