ace.define("ace/theme/spider_dark",["require","exports","module","ace/lib/dom"], function(acequire, exports, module) {

    exports.isDark = true;
    exports.cssClass = "ace-spider-dark";
    exports.cssText = ".ace-spider-dark .ace_gutter {\
background: #272727;\
color: #CCC\
}\
.ace-spider-dark .ace_print-margin {\
width: 1px;\
background: #272727\
}\
.ace-spider-dark {\
background-color: #555960;\
color: #FFFFFF\
}\
.ace-spider-dark .ace_constant.ace_other,\
.ace-spider-dark .ace_cursor {\
color: #FFFFFF\
}\
.ace-spider-dark .ace_marker-layer .ace_selection {\
background: #2b2b2b\
}\
.ace-spider-dark.ace_multiselect .ace_selection.ace_start {\
box-shadow: 0 0 3px 0px #2D2D2D;\
}\
.ace-spider-dark .ace_marker-layer .ace_step {\
background: rgb(102, 82, 0)\
}\
.ace-spider-dark .ace_marker-layer .ace_bracket {\
margin: -1px 0 0 -1px;\
border: 1px solid #6A6A6A\
}\
.ace-tomorrow-night-bright .ace_stack {\
background: rgb(66, 90, 44)\
}\
.ace-spider-dark .ace_marker-layer .ace_active-line {\
background: #393939\
}\
.ace-spider-dark .ace_gutter-active-line {\
background-color: #393939\
}\
.ace-spider-dark .ace_marker-layer .ace_selected-word {\
border: 1px solid #2b2b2b\
}\
.ace-spider-dark .ace_invisible {\
color: #6A6A6A\
}\
.ace-spider-dark .ace_keyword,\
.ace-spider-dark .ace_meta,\
.ace-spider-dark .ace_storage,\
.ace-spider-dark .ace_storage.ace_type,\
.ace-spider-dark .ace_support.ace_type {\
color: #f77fab\
}\
.ace-spider-dark .ace_keyword.ace_operator {\
color: #66CCCC\
}\
.ace-spider-dark .ace_constant.ace_character,\
.ace-spider-dark .ace_constant.ace_language,\
.ace-spider-dark .ace_constant.ace_numeric,\
.ace-spider-dark .ace_keyword.ace_other.ace_unit,\
.ace-spider-dark .ace_support.ace_constant,\
.ace-spider-dark .ace_variable.ace_parameter {\
color: #F99157\
}\
.ace-spider-dark .ace_invalid {\
color: #CDCDCD;\
background-color: #F2777A\
}\
.ace-spider-dark .ace_invalid.ace_deprecated {\
color: #CDCDCD;\
background-color: #CC99CC\
}\
.ace-spider-dark .ace_fold {\
background-color: #6699CC;\
border-color: #CCCCCC\
}\
.ace-spider-dark .ace_entity.ace_name.ace_function,\
.ace-spider-dark .ace_support.ace_function,\
.ace-spider-dark .ace_variable {\
color: #6699CC\
}\
.ace-spider-dark .ace_support.ace_class,\
.ace-spider-dark .ace_support.ace_type {\
color: #FFCC66\
}\
.ace-spider-dark .ace_heading,\
.ace-spider-dark .ace_markup.ace_heading,\
.ace-spider-dark .ace_string {\
color: #99CC99\
}\
.ace-spider-dark .ace_comment {\
color: #999999\
}\
.ace-spider-dark .ace_entity.ace_name.ace_tag,\
.ace-spider-dark .ace_entity.ace_other.ace_attribute-name,\
.ace-spider-dark .ace_meta.ace_tag,\
.ace-spider-dark .ace_variable {\
color: #F2777A\
}\
.ace-spider-dark .ace_indent-guide {\
background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEklEQVQImWPQ09NrYAgMjP4PAAtGAwchHMyAAAAAAElFTkSuQmCC) right repeat-y\
}";

    var dom = acequire("../lib/dom");
    dom.importCssString(exports.cssText, exports.cssClass);
});
