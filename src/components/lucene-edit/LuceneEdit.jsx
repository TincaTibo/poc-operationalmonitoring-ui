import React, { PureComponent, PropTypes } from 'react';
import * as ace from 'brace';
import 'brace/mode/lucene';
import 'brace/snippets/lucene';
import 'brace/ext/language_tools';
import './AceSpiderDark';

import getSnippets from '../../config/snippets';

let view;
const langTools = ace.acequire('ace/ext/language_tools');
const spiderCompleter = {
    getCompletions: function(editor, session, pos, prefix, callback) {
        if (prefix.length === 0) { callback(null, []); return }

        const snippets = getSnippets(view);

        const pref = prefix.toLowerCase();
        callback(null, snippets.map(snippet => {
            const idx = snippet.word.toLowerCase().indexOf(pref);
            if(idx > -1){
                return {name: snippet.word, value: snippet.word, score: idx, meta: snippet.meta}
            }
            else{
                return null;
            }
        }).filter(res => res !== null));
    }
};
langTools.addCompleter(spiderCompleter);

export default class LuceneEdit extends PureComponent {
    constructor(props){
        super(props);
        this.state = {
            value: props.value
        };
        view = props.view;
    }

    componentWillReceiveProps(nextProps){
        const editor = ace.edit(this.props.id);
        if(nextProps.value !== this.state.value){
            this.updateText(nextProps.value);
        }
        if(this.props.width !== nextProps.width){
            editor.resize();
        }
        if(view !== nextProps.view){
            view = nextProps.view;
        }
    }

    shouldComponentUpdate(){
        return false;
    }

    render() {
        return (
            <div
                id={this.props.id}
                style={{
                    ...this.props.style,
                }}
            >
                {this.props.value}
            </div>
        )
    }

    componentDidMount(){
        this.activateEditor();
        this.updateText(this.props.value); //needed for indentedSoftWrap: false to work...
    }

    activateEditor(){
        const editor = ace.edit(this.props.id);
        editor.getSession().setMode('ace/mode/lucene');
        editor.setReadOnly(false);
        editor.setHighlightActiveLine(false);
        if(this.props.multiLine){
            editor.getSession().setUseWrapMode(true);
        }
        editor.setShowPrintMargin(false);
        editor.setShowInvisibles(false);
        editor.renderer.setShowGutter(false);
        if(this.props.fontSize){
            editor.setFontSize(this.props.fontSize);
        }
        editor.getSession().setOption('indentedSoftWrap', false);
        editor.renderer.$cursorLayer.element.style.display = 'none';
        /* Avoid message:
         * Automatically scrolling cursor into view after selection change this will be disabled in the next version set editor.$blockScrolling = Infinity to disable this message
         */
        editor.$blockScrolling = Infinity;
        editor.setOptions({
            enableBasicAutocompletion: false,
            enableSnippets: false,
            enableLiveAutocompletion: true
        });
        if(this.props.multiLine){
            editor.getSession().setUseWrapMode(true);
            editor.commands.addCommand({
                name: 'newLine',
                bindKey: {win: 'Shift-Enter',  mac: 'Shift-Enter'},
                exec: () => {
                    editor.splitLine();
                }
            });
        }
        if(this.props.fontSize){
            editor.setFontSize(this.props.fontSize);
        }
        if(this.props.theme){
            editor.setTheme(this.props.theme);
        }

        editor.commands.addCommand({
            name: 'valid',
            bindKey: {win: 'Enter',  mac: 'Enter'},
            exec: () => {
                this.props.onValid(editor.getValue());
                editor.blur();
            }
        });
        editor.commands.addCommand({
            name: 'cancel',
            bindKey: {win: 'Escape',  mac: 'Escape'},
            exec: () => {
                editor.blur();
            }
        });
        editor.session.on('change', delta => {
            // delta.start, delta.end, delta.lines, delta.action
            const value = editor.session.getValue();
            this.props.onChange(value);
            this.setState({value});
        });
        editor.on('blur', () => {
            this.props.onBlur && this.props.onBlur();
            editor.renderer.$cursorLayer.element.style.display = 'none';
        });
        editor.on('focus', () => {
            this.props.onFocus && this.props.onFocus();
            editor.renderer.$cursorLayer.element.style.display = 'block';
        });

        editor.resize();
    }

    updateText(value){
        const editor = ace.edit(this.props.id);
        const range = editor.getSelectionRange();
        editor.session.setValue(value);
        editor.getSelection().setSelectionRange(range);
    }

    static propTypes = {
        id: PropTypes.string,
        value: PropTypes.string,
        style: PropTypes.object,
        width: PropTypes.number,
        view: PropTypes.string,
        multiLine: PropTypes.bool,
        fontSize: PropTypes.number,
        theme: PropTypes.string,
        onValid: PropTypes.func,
        onChange: PropTypes.func,
    };

    static defaultProps = {
        id: ''
    }
}


