import React, { PropTypes } from 'react';
import Divider from 'material-ui/Divider';

import MenuButtonContainer from '../../containers/menu/MenuButtonContainer';
import RefreshButtonContainer from './elements/RefreshButtonContainer';
import SettingsAccessContainer from '../../containers/actions/SettingsAccessContainer';

const styles = {
};

const MenuBar = (props) => (
    <div style={{
        width: '100%',
        height: '100%',
        display: 'block',
        overflowY: 'visible'
    }}>
        <MenuButtonContainer/>
        <Divider/>
        <SettingsAccessContainer/>
        <RefreshButtonContainer/>
        <Divider/>
    </div>
);

MenuBar.propTypes = {
    isAdmin: PropTypes.bool,
    rights: PropTypes.object,
};

export default MenuBar;