import {connect} from 'react-redux';

import RefreshButton from './RefreshButton';
import {UpdateViewActions} from '../../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        refreshing: state.main.refreshing,
        autoRefreshActive: state.main.autoRefreshActive,
        autoRefreshDelay: state.main.autoRefreshDelay,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onRefresh: () => {
            dispatch(UpdateViewActions.updateView({force: true}));
        },
    }
};

const RefreshButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(RefreshButton);

export default RefreshButtonContainer;