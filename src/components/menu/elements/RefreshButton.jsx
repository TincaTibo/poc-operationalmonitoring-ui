import React, { PropTypes } from 'react';
import Button from 'material-ui/FlatButton';
import Icon from 'material-ui/svg-icons/navigation/refresh';
import ToolTip from 'rc-tooltip';
import '../../tips/tipDark.css';

const styles={
    button: {
        width: 30,
        height: 30,
        minWidth: 30,
        padding: 0,
        margin: 5,
        lineHeight: '30px'
    },

};

const RefreshButton = (props) =>(
    <ToolTip
        placement={props.tipPlacement}
        prefixCls={'rc-tooltip-dark'}
        overlay={
            <span>
                Refresh<br/>
                Autorefresh {!props.autoRefreshActive && 'in'}active {props.autoRefreshActive && `every ${props.autoRefreshDelay}s`}
            </span>
        }
        mouseEnterDelay={0.5}
        destroyTooltipOnHide={true}
    >
        <Button
            onTouchTap={props.onRefresh}
            icon={
                <Icon
                    color='#FFFFFF'
                    style={{
                        transform: props.refreshing ? 'rotate(360deg)' : undefined,
                        transition: props.refreshing ? '2s' : undefined,
                    }}
                />
            }
            style={styles.button}
            hoverColor="#DFDFDF"
        />
    </ToolTip>
);

RefreshButton.propTypes = {
    refreshing: PropTypes.bool.isRequired,
    autoRefreshActive: PropTypes.bool.isRequired,
    autoRefreshDelay: PropTypes.number.isRequired,
    tipPlacement: PropTypes.string,

    onRefresh: PropTypes.func.isRequired
};

RefreshButton.defaultProps = {
    tipPlacement: 'right'
};

export default RefreshButton;
