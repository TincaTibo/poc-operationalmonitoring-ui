import React, { PropTypes } from 'react';
import Button from 'material-ui/FlatButton';
import Icon from 'material-ui/svg-icons/navigation/menu';
import ToolTip from 'rc-tooltip';
import '../../tips/tipDark.css';

const style={
    width: 30,
    height: 30,
    padding: 0,
    minWidth: 30,
    margin: 5,
    lineHeight: '30px'
};

const MenuButton = (props) =>(
    <ToolTip
        placement={props.tipPlacement}
        prefixCls={'rc-tooltip-dark'}
        overlay={props.opened ? 'Reduce menu' : 'Open menu'}
        mouseEnterDelay={0.5}
        destroyTooltipOnHide={true}
    >
        <Button
            onTouchTap={() => { props.onMenuOpen(!props.opened) }}
            style={style}
            hoverColor="#DFDFDF"
            icon={<Icon color="#FFFFFF"/>}
        />
    </ToolTip>
);

MenuButton.propTypes = {
    opened: PropTypes.bool,
    onMenuOpen: PropTypes.func.isRequired,
    tipPlacement: PropTypes.string,
    disabled: PropTypes.bool,
};

MenuButton.defaultProps = {
    tipPlacement: 'right',
    disabled: false,
};

export default MenuButton;
