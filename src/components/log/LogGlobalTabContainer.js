import {connect} from 'react-redux';
import LogGlobalTab from './LogGlobalTab';

const mapStateToProps = (state, ownProps) => {

    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const LogGlobalTabContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LogGlobalTab);

export default LogGlobalTabContainer;