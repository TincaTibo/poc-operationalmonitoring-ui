import React, { Component, PropTypes } from 'react';

import Layout from '../details/ResourceWithTabsContainer';
import LogGlobalTabContainer from './LogGlobalTabContainer';
import JsonTab from '../details/JsonTab';

const styles = {
};

export default class Log extends Component {

    renderTitle = () => {
        if(this.props.log){
            return `EventStatus: ${this.props.log['@id']}`;
        }
    };

    render() {
        return <Layout title={this.renderTitle}
                       propsToWait={[this.props.log]}
                       activeTab={this.props.activeTab}
                       onChangeActiveTab={this.props.onChangeTab}
        >
            <LogGlobalTabContainer
                tabTitle='Event Status'
                log={this.props.log}
            />
            <JsonTab
                tabTitle='Source'
                object={this.props.log}
                width={this.props.width}
            />
        </Layout>
    }

    static propTypes = {
        id: PropTypes.string.isRequired,
        log: PropTypes.object,
        activeTab: PropTypes.number,
        width: PropTypes.number,

        onChangeTab: PropTypes.func.isRequired,
    };
}
