import React, { PureComponent, PropTypes } from 'react';
import Subheader from 'material-ui/Subheader';

import {DATETIME_FORMAT_MINUTE} from '../../config/locale';
import _ from 'lodash';
import Link from '../link/LinkContainer';
import ItemShortViewContainer from '../../containers/view/ItemShortViewContainer';

const styles = {
    fields:{
        fieldHeaderStyle : {
            fontWeight: 'bold',
            fontSize: '12px',
            marginRight: '16px',
            width: '180px',
            display: 'table-cell'
        },
        fieldValueStyle: {
            fontFamily: 'Monospace',
            fontSize: '12px',
            width: '200px',
            textAlign: 'left',
            display: 'table-cell'
        },
        link: {
            fontFamily: 'Monospace',
            fontSize: '12px',
            textDecoration: 'underline',
            cursor: 'pointer'
        },
    },
    cells:{
        cellStyle: {
            paddingLeft: '5px',
            paddingRight: '5px',
            height: '24px'
        },
        numberCellStyle: {
            paddingLeft: '5px',
            paddingRight: '5px',
            height: '24px',
            textAlign: 'right'
        },
    },
    buttons:{
        marginTop: 15,
        marginBottom: 0,
        marginLeft: 16,
        marginRight: 6,
    }
};

function renderField(header, value){
    return <span>
            <span style={styles.fields.fieldHeaderStyle}>{header}: </span>
            <span style={styles.fields.fieldValueStyle}>{value}</span>
        </span>
}

export default class LogGlobalTab extends PureComponent {

    constructor(props){
        super(props);
    }

    render() {
        const log = this.props.log;

        return (
            <div style={{
                width: '100%',
                height: '100%',
            }}>
                <div style={{paddingLeft: '16px'}}>
                    <Subheader>Device</Subheader>
                    {renderField('Park', <ItemShortViewContainer item={log.park}/>)}<br/>
                    {renderField('Terminal', <ItemShortViewContainer item={log.pos}/>)}<br/>
                    <Subheader>Status</Subheader>
                    {renderField('Event ID', log.eventName)}<br/>
                    {renderField('Event status', log.eventStatus && log.eventStatus.replace('sp:',''))}<br/>
                    {renderField('Event type', log.eventType && log.eventType.replace('sp:',''))}<br/>
                    {renderField('Family', log.peripheralType && log.peripheralType.replace('sp:',''))}<br/>
                    <Subheader>Dates</Subheader>
                    {renderField('Start date', log.startTime.format(DATETIME_FORMAT_MINUTE))}<br/>
                    {renderField('End date', log.endTime && log.endTime.format(DATETIME_FORMAT_MINUTE))}<br/>
                    {renderField('Update date', log.lastUpdatedDate && log.lastUpdatedDate.format(DATETIME_FORMAT_MINUTE))}<br/>
                </div>
            </div>
        );
    }

    static propTypes = {
        log: PropTypes.object,
    };
}

function renderOneToMany(items, render){
    const max = items.length-1;
    if(items){
        if(_.isArray(items) && items.length){
            return (
                <span>
                    {items.map((item, idx) => (
                        <span key={item}>
                            {render ? render(item) : item}
                            {idx < max && <br/>}
                        </span>
                    ))}
                </span>
            )
        }
        else if(_.isString(items)){
            return render ? render(items) : items;
        }
        else {
            return '';
        }
    }
    else {
        return '';
    }
}