import React, { Component, PropTypes } from 'react';
import {select, event, drag} from 'd3';

import Colors from '../../config/colors';

const styles = {
    cursor: {
        area: {
            fill: '#00BCD4',
            fillOpacity: 0.2,
            stroke: '#00BCD4',
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1,
            cursor: 'move',
            transition: 'fill stroke 0.5s'
        },
        handle: {
            fill: '#ffffff',
            fillOpacity: 1,
            stroke: '#00BCD4',
            strokeWidth: '1px',
            cursor: 'col-resize',
            transition: 'r 0.5s'
        },
        toolTipArea:{
            opacity: 0,
            visibility: 'hidden',
            transition: 'opacity visibility 0.25s'
        },
        toolTip: {
            fill:'#1a1a1a',
            fillOpacity: 0.85,
            fillRule: 'evenodd',
            stroke: '#ffffff',
            strokeWidth: 0.1,
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeMiterlimit: 1.3,
            strokeOpacity:1
        },
        toolTipTextLeft: {
            fontStyle:'normal',
            fontWeight:'normal',
            fontSize:'12.5px',
            lineHeight:'100%',
            fontFamily:'sans-serif',
            letterSpacing:'0px',
            wordSpacing:'0px',
            fill:'#ffffff',
            fillOpacity:'1',
            stroke:'none',
        },
        toolTipTextRight: {
            fontStyle:'normal',
            fontWeight:'normal',
            fontSize:'12.5px',
            lineHeight:'100%',
            fontFamily:'sans-serif',
            letterSpacing:'0px',
            wordSpacing:'0px',
            fill:'#ffffff',
            fillOpacity:'1',
            stroke:'none',
            textAlign: 'end',
            textAnchor: 'end'
        },
        zoom: {
            fill: '#00BCD4',
            fillOpacity : 1,
            stroke: 'none'
        },
        zoomOut: {
            fill: '#ff816b',
            fillOpacity : 1,
            stroke: 'none'
        },
        zoomArea:{
            fill: 'transparent',
            fillOpacity : 1,
            stroke: 'none',
            cursor: 'pointer'
        },
        zoomInArea:{
            opacity: 0,
            visibility: 'hidden',
            transition: 'opacity visibility 0.5s'
        }
    },
    dragOverlay: {
        fill: 'transparent',
        fillOpacity : 1,
        stroke: 'none',
        cursor: 'crosshair'
    }
};

export default class Cursor extends Component {

    constructor(props) {
        super(props);

        this.cursor = null;
        this.zoomInButton = null;
        this.cursorLeftHandle = null;
        this.cursorRightHandle = null;
        this.toolTipLeft = null;
        this.toolTipRight = null;
        this.dragOverlayContainer = null;
        this.dragOverlay = null;
    }

    componentDidMount(){
        this.setResizeCursorEvents();
        this.setDragCursorEvents();
        this.setDrawCursorEvents();
    }

    render() {
        const width = this.props.endPos - this.props.startPos < 1 ? 1 : this.props.endPos - this.props.startPos;
        const height = this.props.height;

        return <g>
            {/* Drag Overlay */}
            <g id="timeLineDragOverlay"
               ref={value => this.dragOverlayContainer = value}
            >
                {/* Horizontal Axis */}
                <rect x={0}
                      y={0}
                      width={this.props.overlayWidth}
                      height={this.props.overlayHeight}
                      style={styles.dragOverlay}
                      ref={value => this.dragOverlay = value}
                      onMouseMove={this.setOverlayCursor.bind(this)}
                      onMouseOver={this.setOverlayCursor.bind(this)}
                />
            </g>
            <g id="timeLineCursor" transform={`translate(${this.props.startPos},0)`}>
                {/* Cursor */}
                <g id='timeLineCursorSelection'>
                    <rect x={0} y={-3} width={width} height={height + 10}
                          ref={value => {this.cursor = value}}
                          style={styles.cursor.area}
                          onMouseOver={() => {this.props.canZoom && !this.props.maxZoom && show(this.zoomInButton)}}
                          onMouseOut={() => {this.props.canZoom && hide(this.zoomInButton)}}
                          onDoubleClick={this.props.canZoom && this.props.zoomIn }
                    />
                    <circle r='4' cx={0} cy={height/2+2}
                            ref={value => {this.cursorLeftHandle = value}}
                            style={{
                                ...styles.cursor.handle,
                                fill: this.props.startIsOutOfView ? Colors.light : styles.cursor.handle.fill,
                                stroke: this.props.startIsOutOfView ? Colors.medium : styles.cursor.handle.stroke
                            }}
                            onMouseOver={onHandleOver}
                            onMouseOut={onHandleOut}
                    />
                    <circle r='4' cx={width} cy={height/2+2}
                            ref={value => {this.cursorRightHandle = value}}
                            style={{
                                ...styles.cursor.handle,
                                fill: this.props.endIsOutOfView ? Colors.light : styles.cursor.handle.fill,
                                stroke: this.props.endIsOutOfView ? Colors.medium : styles.cursor.handle.stroke
                            }}
                            onMouseOver={onHandleOver}
                            onMouseOut={onHandleOut}
                    />
                </g>

                {/* ZoomIn */}
                {this.props.canZoom &&
                    <g id="timeLineZoomInIcon"
                       ref={value => this.zoomInButton = value}
                       style={styles.cursor.zoomInArea}
                       onMouseOver={() => {
                           !this.props.maxZoom && show(this.zoomInButton)
                       }}
                       onMouseOut={() => {
                           hide(this.zoomInButton)
                       }}
                       transform={`translate(${width - 25}, 0)`}>
                        <path
                            d='M 15.5 14 h -0.79 l -0.28 -0.27 C 15.41 12.59 16 11.11 16 9.5 C 16 5.91 13.09 3 9.5 3 S 3 5.91 3 9.5 S 5.91 16 9.5 16 c 1.61 0 3.09 -0.59 4.23 -1.57 l 0.27 0.28 v 0.79 l 5 4.99 L 20.49 19 l -4.99 -5 Z m -6 0 C 7.01 14 5 11.99 5 9.5 S 7.01 5 9.5 5 S 14 7.01 14 9.5 S 11.99 14 9.5 14 Z'
                            style={styles.cursor.zoom}
                        />
                        <path d="M12 10h-2v2H9v-2H7V9h2V7h1v2h2v1z"
                              style={styles.cursor.zoom}/>
                        <rect x={0} y={0} width={23} height={23}
                              style={styles.cursor.zoomArea}
                              onClick={this.props.zoomIn}
                        />
                    </g>
                }

                {/* ToolTip Left */}
                <g id='timeLineToolTipLeft' transform={`translate(0, ${height/2})`}
                   style={styles.cursor.toolTipArea}
                   ref={value => this.toolTipLeft = value}>
                    <path d="M -1.6476054,-7.6340193 H 33.389147 c 0.455375,0 0.821977,0.3969982 0.821977,0.8901307 v 4.6731858 c 0,0.4931324 -0.366602,0.8901307 -0.821977,0.8901307 H 0.9414884 L 0.06024994,-0.00172353 -0.82098845,-1.1805721 H -1.6476054 c -0.455375,0 -0.8219766,-0.3969983 -0.8219766,-0.8901307 v -4.6731858 c 0,-0.4931325 0.3666016,-0.8901307 0.8219766,-0.8901307 z"
                          style={styles.cursor.toolTip}
                          transform={`scale(${_.max([3.5 * height/50, 1.9])})`}
                    />
                    <text x="-2" y={-12.5 * height/50} style={{...styles.cursor.toolTipTextLeft, fontSize: `${_.max([12.5 * height/50, 9])}px`}}>
                        {this.props.startText}
                    </text>
                </g>

                {/* ToolTip Right */}
                <g id='timeLineToolTipRight' transform={`translate(${width}, ${height/2})`}
                   style={styles.cursor.toolTipArea}
                   ref={value => this.toolTipRight = value}>
                    <path d="M -1.6476054,-7.6340193 H 33.389147 c 0.455375,0 0.821977,0.3969982 0.821977,0.8901307 v 4.6731858 c 0,0.4931324 -0.366602,0.8901307 -0.821977,0.8901307 H 0.9414884 L 0.06024994,-0.00172353 -0.82098845,-1.1805721 H -1.6476054 c -0.455375,0 -0.8219766,-0.3969983 -0.8219766,-0.8901307 v -4.6731858 c 0,-0.4931325 0.3666016,-0.8901307 0.8219766,-0.8901307 z"
                          style={styles.cursor.toolTip}
                          transform={`scale(-${_.max([3.5 * height/50, 1.9])},${_.max([3.5 * height/50, 1.9])})`}
                    />
                    <text x="2" y={-12.5 * height/50} style={{...styles.cursor.toolTipTextRight, fontSize: `${_.max([12.5 * height/50, 9])}px`}}>
                        {this.props.stopText}
                    </text>
                </g>
            </g>

            {/* Zoom Out */}
            { this.props.canZoom && this.renderZoomOut() }
        </g>
    }

    renderZoomOut(){
        if(this.props.minZoom){
            return
        }

        return (
            <g id="timeLineZoomOutIcon"
                  transform={`translate(${this.props.overlayWidth - 25}, ${this.props.height - 20})`}>
                <path d='M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14zM7 9h5v1H7z'
                      style={styles.cursor.zoomOut}
                />
                <rect x={0} y={0} width={23} height={23}
                      style={styles.cursor.zoomArea}
                      onClick={this.props.zoomOut}
                />
            </g>
        );
    }

    setResizeCursorEvents(){
        select(this.cursorLeftHandle).call(drag()
            .container(document.getElementById('root'))
            .on('start',() => {
                show(this.toolTipLeft);
            })
            .on('drag', () => {
                this.props.onResizeLeftCursor(event.dx);
            })
            .on('end',() => {
                hide(this.toolTipLeft);
                this.props.onEndResizeCursor();
            })
        );

        select(this.cursorRightHandle).call(drag()
            .container(document.getElementById('root'))
            .on('start',() => {
                show(this.toolTipRight);
            })
            .on('drag', () => {
                this.props.onResizeRightCursor(event.dx);
            })
            .on('end',() => {
                hide(this.toolTipRight);
                this.props.onEndResizeCursor();
            })
        );
    }

    setDragCursorEvents(){
        select(this.cursor).call(drag()
            .container(document.getElementById('root'))
            .on('start',() => {
                if(event.sourceEvent.ctrlKey){
                }
                else {
                    show(this.toolTipLeft);
                    if (this.props.endPos - this.props.startPos > this.props.minWidthToShowToolTip) {
                        show(this.toolTipRight);
                    }
                }
            })
            .on('drag', () => {
                if(event.sourceEvent.ctrlKey){
                    this.props.onMoveDomain(event.dx);
                }
                else {
                    this.props.onDragCursor(event.dx);
                }
            })
            .on('end',() => {
                if(event.sourceEvent.ctrlKey){
                    this.props.onMovedDomain();
                }
                else {
                    hide(this.toolTipLeft);
                    hide(this.toolTipRight);
                    this.props.onEndDragCursor();
                }
            })
        );
    }

    setDrawCursorEvents(){
        select(this.dragOverlay).call(drag()
            .container(this.dragOverlayContainer)
            .on('start',() => {
                if(event.sourceEvent.ctrlKey){
                }
                else{
                    show(this.toolTipLeft);
                    this.props.onStartDrawCursor(event.x);
                }
            })
            .on('drag',() => {
                if(event.sourceEvent.ctrlKey){
                    this.props.onMoveDomain(event.dx);
                }
                else {
                    if (this.props.startPos - this.props.endPos > this.props.minWidthToShowToolTip) {
                        show(this.toolTipRight);
                    }
                    else {
                        hide(this.toolTipRight);
                    }
                    this.props.onDrawCursor(event.dx);
                }
            })
            .on('end',() => {
                if(event.sourceEvent.ctrlKey){
                    this.props.onMovedDomain();
                }
                else {
                    hide(this.toolTipLeft);
                    hide(this.toolTipRight);
                    this.props.onEndCursor();
                }
            })
        );
    }

    setOverlayCursor(event){
        if(event.ctrlKey){
            this.dragOverlay.style.cursor = 'move';
        }
        else{
            this.dragOverlay.style.cursor = 'crosshair';
        }
    }

    static propTypes = {
        startPos: PropTypes.number.isRequired,
        startIsOutOfView: PropTypes.bool,
        endPos: PropTypes.number.isRequired,
        endIsOutOfView: PropTypes.bool,
        height: PropTypes.number.isRequired,
        overlayHeight: PropTypes.number.isRequired,
        overlayWidth: PropTypes.number.isRequired,

        startText: PropTypes.string.isRequired,
        stopText: PropTypes.string.isRequired,
        minWidthToShowToolTip: PropTypes.number.isRequired,

        canZoom: PropTypes.bool.isRequired,
        maxZoom: PropTypes.bool,
        minZoom: PropTypes.bool,

        zoomIn: PropTypes.func,
        zoomOut: PropTypes.func,

        onResizeLeftCursor: PropTypes.func.isRequired,
        onResizeRightCursor: PropTypes.func.isRequired,
        onEndResizeCursor: PropTypes.func.isRequired,

        onDragCursor: PropTypes.func.isRequired,
        onEndDragCursor: PropTypes.func.isRequired,

        onStartDrawCursor: PropTypes.func.isRequired,
        onDrawCursor: PropTypes.func.isRequired,
        onEndCursor: PropTypes.func.isRequired,

        onMoveDomain: PropTypes.func,
        onMovedDomain: PropTypes.func,
    };

    static defaultProps = {
        onMoveDomain: () => {},
        onMovedDomain: () => {}
    }
}

function hide(element){
    element.style.opacity = 0;
    element.style.visibility = 'hidden';
}
function show(element){
    element.style.opacity = 1;
    element.style.visibility = 'visible';
}

function onHandleOver(event){
    event.target.style.r = 8;
}
function onHandleOut(event){
    event.target.style.r = 4;
}