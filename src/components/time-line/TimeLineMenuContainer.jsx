import {connect} from 'react-redux';
import _ from 'lodash';

import TimeLineMenu from './TimeLineMenu';
import { TimeActions} from '../../actions';

import ConfigViews from './ConfigViews';

const mapStateToProps = (state) => {
    const mode = _.find(ConfigViews[state.main.view], {code: state.time.timeLineHisto.mode}) || ConfigViews[state.main.view][0];

    return {
        options: ConfigViews[state.main.view],
        selected: mode,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onSelect: (mode) => {
            dispatch(TimeActions.setHistoOption(mode.code));
        },
    }
};

const TimeLineMenuContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TimeLineMenu);

export default TimeLineMenuContainer;