import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import {scaleTime, scaleLinear, timeDay} from 'd3';
import _ from 'lodash';
import LinearProgress from 'material-ui/LinearProgress';
import co from 'co';
import {delay} from 'redux-saga';
import ToolTip from 'rc-tooltip';

import '../tips/tipDark.css';

import TimeLineMenuContainer from './TimeLineMenuContainer';

import {TIME_FORMAT_SECOND} from '../../config/locale';
import Colors from '../../config/colors';
import Cursor from './Cursor';

const marginLeft = 50;
const marginRight = 20;
const marginTop = 5;
const marginBottom = 5;
const axisHeight = 20;
const spaceBetweenTicks = 70;
const barsBetweenTicks = 7;

const styles = {
    axis: {
        background : {
            fill: '#FFF',
            fillOpacity: 0.8,
            stroke: 'none'
        },
        axisStyle : {
            fill: 'none',
            stroke: Colors.medium,
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1
        },
        nowStyle : {
            fill: Colors.mediumDark,
            stroke: Colors.mediumDark,
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1
        },
        arrowStyle : {
            fill: Colors.medium,
            fillOpacity: 1,
            stroke: Colors.medium,
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1
        },
        limitMarkerStyle : {
            fill: 'none',
            stroke: Colors.medium,
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1
        },
        timeAxisTextStyle : {
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: '10px',
            lineHeight: '125%',
            fontFamily: 'Roboto, sans-serif',
            fill: Colors.medium,
            fillOpacity: 1,
            stroke: 'none',
            textAlign: 'center',
            textAnchor: 'middle',
            userSelect: 'none'
        },
        verticalAxisTextStyle : {
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: '9px',
            lineHeight: '100%',
            fontFamily: 'Roboto, sans-serif',
            fill: Colors.medium,
            fillOpacity: 1,
            stroke: 'none',
            textAlign: 'end',
            textAnchor: 'end',
            userSelect: 'none'
        }
    },
    histo: {
        common:{
            bar:{
                fillOpacity:1,
                fillRule: 'evenodd',
                strokeWidth:'1',
                strokeLinecap:'butt',
                strokeLinejoin:'miter',
                strokeMiterlimit:'1',
                strokeDasharray:'none',
                strokeDashoffset:'0',
                strokeOpacity:'1'
            },
            legend: {
                fontStyle: 'normal',
                fontWeight: 'normal',
                fontSize: '12.5px',
                lineHeight: '125%',
                fontFamily: 'sans-serif',
                letterSpacing: '0px',
                wordSpacing: '0px',
                fillOpacity:1,
                fillRule: 'evenodd',
                stroke:'none',
                textAlign:'end',
                textAnchor:'end',
                userSelect: 'none'
            }
        }
    }
};

export default class TimeLine extends Component{
    constructor(props){
        super(props);

        this.state = {
            domain: props.domains[0],
            start: props.timeSpan.start,
            stop: props.timeSpan.stop,
            maxZoom: false,
            waitForLoad: false,
        };

        this.histoWidth = props.width - marginLeft - marginRight;
        this.xAxis = scaleTime()
            .domain([this.state.domain.min, this.state.domain.max])
            .range([0, this.histoWidth]);
        this.xAxis.clamp(true);
        this.widthOfLastUpdate = null;
        this.resetButton = null;
    }

    componentWillMount(){
        this.getItems(this.props, this.state.domain);
    }

    componentWillReceiveProps(nextProps){
        //Change of width (big): update item
        if(nextProps.width !== this.props.width){
            this.getItems(nextProps, this.state.domain, this.widthOfLastUpdate && Math.abs(nextProps.width - this.widthOfLastUpdate) > 30);
        }

        //Update last domain if min/max changes
        if(nextProps.timeSpan.min !== this.props.timeSpan.min ||
            nextProps.timeSpan.max !== this.props.timeSpan.max){

            const newDomains = [...nextProps.domains];
            newDomains[this.props.domains.length-1] = {
                min: nextProps.timeSpan.min,
                max: nextProps.timeSpan.max,
            };

            this.props.updateDomains(newDomains);
        }

        //If selection changed
        if((nextProps.timeSpan.start !== this.props.timeSpan.start)
            || (nextProps.timeSpan.stop !== this.props.timeSpan.stop)){

            //update domains if selection is outside domains
            let toUpdate = false;
            const newDomains = nextProps.domains.map((domain, index) => {
                if(nextProps.timeSpan.start.isBefore(domain.min) || nextProps.timeSpan.stop.isAfter(domain.max)){
                    toUpdate = true;
                    if(index === nextProps.domains.length-1){
                        //if last domain, increase domain
                        return {
                            min: nextProps.timeSpan.start.isBefore(domain.min) ? nextProps.timeSpan.start : domain.min,
                            max: nextProps.timeSpan.stop.isAfter(domain.max) ? nextProps.timeSpan.stop : domain.max
                        }
                    }
                    else{
                        //if not last domain, shift domain
                        if(nextProps.timeSpan.start.isBefore(domain.min)){
                            return {
                                min: nextProps.timeSpan.start,
                                max: moment(domain.max).subtract(domain.min.diff(nextProps.timeSpan.start))
                            }
                        }
                        else{
                            return {
                                min: moment(domain.min).add(nextProps.timeSpan.stop.diff(domain.max)),
                                max: nextProps.timeSpan.stop
                            }
                        }

                    }
                }
                else{
                    return domain;
                }
            });

            if(toUpdate){
                this.props.updateDomains(newDomains);
            }

            //Update cursor
            this.setState({
                start: nextProps.timeSpan.start,
                stop: nextProps.timeSpan.stop
            });
        }

        //If change in current domain, update items and state
        if(nextProps.domains[0] !== this.props.domains[0]){
            this.setState({
                domain: nextProps.domains[0]
            });
            this.getItems(nextProps, nextProps.domains[0]);
        }

        //If we got new histo change indicator
        if(nextProps.histo !== this.props.histo){
            this.setState({
                waitForLoad: false
            });
        }
    }

    getItems(props, domain, shouldReload = true){
        this.histoWidth = props.width - marginLeft - marginRight;

        this.xAxis
            .domain([domain.min, domain.max])
            .range([0, this.histoWidth]);

        this.ticks = this.xAxis.ticks(_.floor(this.histoWidth / spaceBetweenTicks));

        const intervalMs = _.max([_.round(moment(this.ticks[1]).diff(moment(this.ticks[0]))/barsBetweenTicks), 60000]);

        if(shouldReload && intervalMs){
            this.widthOfLastUpdate = props.width;
            this.setState({
                waitForLoad: true
            });
            props.loadHisto(intervalMs, domain.min, domain.max);
        }
    }

    render() {
        return  (
            <div
                style={this.props.style}
            >
                <svg
                    id="timeLine"
                    width={this.props.width}
                    height={this.props.height}
                    onWheel={this.onWheel}
                >
                    <g transform={`translate(${marginLeft},${marginTop})`}>
                        {
                            this.props.histo.items &&
                                this.renderHisto()
                        }
                        { this.renderAxis() }
                        { this.renderTools() }
                        { this.renderVerticalAxis() }
                        { this.renderHistoLegend() }
                        { this.renderCursor() }
                    </g>
                </svg>
                <div style={{width:0, height:0}}>
                { this.renderMenu() }
                {
                    this.props.loading &&
                    this.renderLoading()
                }
                </div>
            </div>
        )
    }

    renderMenu(){
        return (
            <div style={{
                position: 'relative',
                bottom: 85,
                left: this.props.width - marginRight + 2,
            }}>
                <TimeLineMenuContainer/>
            </div>
        );
    }

    renderLoading(){
        return (
            <div style={{
                position: 'relative',
                bottom: + marginBottom + axisHeight + 25,
                height: 0,
                left: marginLeft,
                width: this.props.width - marginLeft - marginRight,
            }}>
                <LinearProgress mode="indeterminate" />
            </div>
        );
    }

    renderTools() {
        const axisYPos = this.props.height - marginBottom - axisHeight;

        return (
            <g id="timeLineTools"
               transform={`translate(0, ${axisYPos})`}
            >
                {/* Forth and back */}
                <ToolTip
                    placement="top"
                    prefixCls={'rc-tooltip-dark'}
                    overlay={this.props.domains.length === 1 ? 'Extend forward' : 'Slide forward'}
                    mouseEnterDelay={0.5}
                    destroyTooltipOnHide={true}
                >
                    <path
                        id="TimelineForth"
                        style={{
                            ...styles.axis.arrowStyle,
                            cursor: 'pointer',
                        }}
                        onMouseOver={(event) => {
                            event.target.style.fill = Colors.itemFillSelected;
                            event.target.style.stroke = Colors.itemFillSelected;
                        }}
                        onMouseOut={(event) => {
                            event.target.style.fill = styles.axis.arrowStyle.fill;
                            event.target.style.stroke = styles.axis.arrowStyle.stroke;
                        }}
                        onClick={() => this.shiftTimeLine(-120)}
                        d={`m ${this.histoWidth + 9},-5 5,5 0,-5 5,5 -5,5 0,-5 -5,5 Z`}
                    />
                </ToolTip>

                <ToolTip
                    placement="top"
                    prefixCls={'rc-tooltip-dark'}
                    overlay={this.props.domains.length === 1 ? 'Extend backward' : 'Slide backward'}
                    mouseEnterDelay={0.5}
                    destroyTooltipOnHide={true}
                >
                    <path
                        id="TimelineBack"
                        style={{
                            ...styles.axis.arrowStyle,
                            cursor: 'pointer',
                        }}
                        onMouseOver={(event) => {
                            event.target.style.fill = Colors.itemFillSelected;
                            event.target.style.stroke = Colors.itemFillSelected;
                        }}
                        onMouseOut={(event) => {
                            event.target.style.fill = styles.axis.arrowStyle.fill;
                            event.target.style.stroke = styles.axis.arrowStyle.stroke;
                        }}
                        onClick={() => this.shiftTimeLine(120)}
                        d='m -7,-5 -5,5 0,-5 -5,5 5,5 0,-5 5,5 Z'
                    />
                </ToolTip>

                {/* Reset Time button */}
                <ToolTip
                    placement="top"
                    prefixCls={'rc-tooltip-dark'}
                    overlay={'Reset time span'}
                    mouseEnterDelay={0.5}
                    destroyTooltipOnHide={true}
                >
                    <g
                        id="ResetTimeButton"
                        onMouseOver={() => {
                            this.resetButton.style.fill = Colors.itemFillSelected;
                            this.resetButton.style.stroke = Colors.itemFillSelected;
                        }}
                        onMouseOut={() => {
                            this.resetButton.style.fill = styles.axis.arrowStyle.fill;
                            this.resetButton.style.stroke = styles.axis.arrowStyle.stroke;
                        }}
                        onClick={() => {
                            this.props.onResetTime();
                        }}
                        transform={`translate(${this.histoWidth + 4}, -30) scale(0.75)`}
                        style = {{cursor: 'pointer'}}
                    >
                        <rect
                            x ='0'
                            y ='0'
                            height ='24'
                            width ='24'
                            style = {{fill: 'transparent'}}
                        />
                        <path
                            ref={value => this.resetButton = value}
                            style={styles.axis.arrowStyle}
                            d='M5 15H3v4c0 1.1.9 2 2 2h4v-2H5v-4zM5 5h4V3H5c-1.1 0-2 .9-2 2v4h2V5zm14-2h-4v2h4v4h2V5c0-1.1-.9-2-2-2zm0 16h-4v2h4c1.1 0 2-.9 2-2v-4h-2v4zM12 9c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z'
                        />
                    </g>
                </ToolTip>
            </g>
        );
    }

    renderAxis(){
        const axisYPos = this.props.height - marginBottom - axisHeight;
        const marks = this.ticks;
        const now = moment();

        return (
            <g id="timeLineAxis"
               transform={`translate(0, ${axisYPos})`}
            >
                {/* White background */}
                <rect style={styles.axis.background}
                      x="-10" y="0"
                      width={this.histoWidth+20}
                      height={marginBottom + axisHeight}
                />

                {/* Horizontal Axis */}
                <path style={styles.axis.axisStyle} d={`M 0,-6 0,6`}/>
                <path style={styles.axis.axisStyle} d={`M 0,0 ${this.histoWidth},0`}/>
                <path style={styles.axis.arrowStyle} d={`m ${this.histoWidth},-5 5,5 -5,5 Z`}/>

                {/* Marks */}
                {
                    marks.map((m, index) => (
                        <g
                            key={m.toISOString()}
                            transform={`translate(${this.xAxis(m)},0)`}
                        >
                            <path style={styles.axis.limitMarkerStyle} d='M 0,0 0,6'/>
                            <text x={0}
                                  y={15}
                                  style={{
                                      ...styles.axis.timeAxisTextStyle,
                                      fill: timeDay(m) >= m ?
                                          Colors.mediumDark : Colors.medium
                                  }}
                             >
                                {this.props.mode.timeLegend(m)}
                            </text>
                        </g>
                        )
                    )
                }

                {/* Now */}
                { now.isSameOrBefore(this.state.domain.max) && now.isSameOrAfter(this.state.domain.min) &&
                    <path transform={`translate(${this.xAxis(now)},0)`} style={styles.axis.nowStyle} d={`m -3,6 6,0 -3,-6 Z`}/>
                }
            </g>
        );
    }

    renderHisto(){
        const maxHeight = this.props.height - marginBottom - marginTop - axisHeight;
        const intervalHisto = this.xAxis(moment(this.state.domain.min).add(this.props.histo.intervalMs));
        const maxScale = _.max(this.props.histo.items.map(i => i.total));

        const histoScale = scaleLinear()
            .domain([0, maxScale])
            .range([0, maxHeight]);

        return <g id="timeLineHisto" transform={`translate(0, ${this.props.height - marginBottom - axisHeight})`}>
            {/* Histogram */}
            {
                this.props.histo.items.map((item, i) => (
                    item.total > 0
                    && item.time.isSameOrAfter(this.state.domain.min)
                    && item.time.isBefore(this.state.domain.max) &&
                    <g key={i}>
                        {
                            this.props.mode.metrics.colors.map((color,i) => (
                                item.metrics[i] > 0 &&
                                <rect
                                    key={i}
                                    style={{
                                        ...styles.histo.common.bar,
                                        fill: color.fill,
                                        stroke: color.stroke,
                                    }}
                                    x={this.xAxis(item.time)}
                                    y={-histoScale(_.sum(item.metrics.slice(0, i + 1)))}
                                    width={intervalHisto}
                                    height={histoScale(item.metrics[i])}
                                />
                            ))
                        }
                    </g>
                    )
                )
            }
        </g>;
    }

    renderHistoLegend(){
        return <g id="timeLineHistoLegend">
            {/* Histogram Legend */}
            {this.props.mode.metrics.legends.map((leg,i) => (
                <text
                    key={i}
                    x={-5}
                    y={46 - 14*i}
                    style={{
                        ...styles.histo.common.legend,
                        fill: this.props.mode.metrics.colors[i].text
                    }}
                >
                    <tspan>{leg}</tspan>
                </text>
            ))}
        </g>
    }

    renderVerticalAxis(){
        const axisYPos = this.props.height - marginBottom - axisHeight;
        const maxHeight = this.props.height - marginBottom - marginTop - axisHeight;
        const maxScale = this.props.histo.items ? _.max(this.props.histo.items.map(i => i.total)) || 0 : 0;
        const verticalScale = scaleLinear()
            .domain([0, maxScale])
            .range([0, maxHeight]);

        const marks = [maxScale];

        return (
            <g id="timeLineVerticalAxis"
               transform={`translate(0, ${axisYPos})`}
            >
                <path style={styles.axis.axisStyle} d={`M 0,0 0, ${-maxHeight}`}/>
                <path style={styles.axis.arrowStyle} d={`m -2.5,${-maxHeight} 5,0 -2.5,-5 Z`}/>

                {/* Marks */}
                {
                    marks.map((m, index) => (
                            <g
                                key={index}
                                transform={`translate(0,${-verticalScale(m)+5})`}
                            >
                                {m > 0 &&
                                    <text x={-5}
                                          y={-5}
                                          style={styles.axis.verticalAxisTextStyle}
                                    >
                                        {this.props.mode.metrics.render(m)}
                                    </text>
                                }
                            </g>
                        )
                    )
                }

            </g>
        );
    }

    renderCursor(){
        return (
            <Cursor
                startPos={this.xAxis(this.state.start)}
                startIsOutOfView={this.state.start.isBefore(this.state.domain.min)}
                endPos={this.xAxis(this.state.stop)}
                endIsOutOfView={this.state.stop.isAfter(this.state.domain.max)}
                height={this.props.height - marginTop - marginBottom - axisHeight}
                overlayHeight={this.props.height - marginTop}
                overlayWidth={this.histoWidth}
                canZoom={true}
                minZoom={this.props.domains.length === 1}
                maxZoom={this.state.maxZoom}
                startText={moment(this.state.start).second(0).millisecond(0).format(TIME_FORMAT_SECOND)}
                stopText={moment(this.state.stop).second(0).millisecond(0).format(TIME_FORMAT_SECOND)}
                minWidthToShowToolTip={245}
                zoomIn={() => {
                    if(!this.state.maxZoom){
                        this.zoomIn({
                            min: this.state.start,
                            max: this.state.stop
                        })
                    }
                    else{
                        this.props.showMessage('Cannot zoom anymore!');
                    }
                }}
                zoomOut={() => {
                    this.zoomOut()
                }}
                onResizeLeftCursor={(delta) => {
                    const newStart = moment(this.xAxis.invert(this.xAxis(this.state.start) + delta));
                    let newStop = this.state.stop;

                    if (newStart.isSameOrAfter(newStop)) {
                        newStop = moment(this.xAxis.invert(this.xAxis(newStop) + delta));
                    }

                    if ((newStop !== this.state.stop
                         && newStop.isSameOrBefore(this.props.domains[0].max))
                        || newStart.isSameOrAfter(this.props.domains[0].min)) {
                        this.setState({
                            start: newStart,
                            stop: newStop
                        });
                    }
                }}
                onResizeRightCursor={(delta) => {
                    let newStop = moment(this.xAxis.invert(this.xAxis(this.state.stop) + delta));
                    let newStart = this.state.start;

                    if(newStop.isSameOrBefore(newStart)){
                        newStart = moment(this.xAxis.invert(this.xAxis(newStart) + delta));
                    }

                    if (newStop.isSameOrBefore(this.props.domains[0].max) &&
                        newStart.isSameOrAfter(this.props.domains[0].min)){
                        this.setState({
                            start: newStart,
                            stop: newStop
                        });
                    }
                }}
                onEndResizeCursor={() => {
                    this.props.customRange(this.state.start, this.state.stop)
                }}
                onDragCursor={(delta) => {
                    this.xAxis.clamp(false);
                    const newStart = moment(this.xAxis.invert(this.xAxis(this.state.start) + delta));
                    const newStop = moment(newStart).add(this.props.timeAggregation);
                    this.xAxis.clamp(true);

                    this.setState({
                        start: newStart,
                        stop: newStop
                    });
                }}
                onEndDragCursor={() => {
                    this.props.moveStart(this.state.start)
                }}
                onStartDrawCursor={(pos) => {
                    this.setState({
                        start: moment(this.xAxis.invert(pos)),
                        stop: moment(this.xAxis.invert(pos + 1))
                    });
                }}
                onDrawCursor={(delta) => {
                    let newStop = moment(this.xAxis.invert(this.xAxis(this.state.stop) + delta));
                    let newStart = this.state.start;

                    if(newStop.isSameOrBefore(newStart)){
                        newStart = moment(this.xAxis.invert(this.xAxis(newStart) + delta));
                    }


                    if (newStop.isSameOrBefore(this.props.domains[0].max) &&
                        newStart.isSameOrAfter(this.props.domains[0].min)){
                        this.setState({
                            start: newStart,
                            stop: newStop
                        });
                    }
                }}
                onEndCursor={() => {
                    this.props.customRange(this.state.start, this.state.stop);
                }}
                onMoveDomain={this.moveTimeLine}
                onMovedDomain={this.movedTimeLine}
            />
        );
    }

    onWheel = (event) => {
        event.preventDefault();
        event.stopPropagation();

        if(!this.state.waitForLoad){
            if(event.deltaY < -50){
                const baseX = document.getElementById('timeLine').getBoundingClientRect().left;
                const x = event.clientX - baseX - marginLeft;
                this.zoomOnTarget(x);
            }
            else if(event.deltaY > 50){
                this.zoomOut();
            }
        }
    };

    zoomOnTarget = (x) => {
        const [minOrigin, maxOrigin] = this.xAxis.domain();
        const target = moment(this.xAxis.invert(x));

        const min = moment(target).subtract(0.25 * (target.diff(moment(minOrigin))));
        const max = moment(target).add(0.25 * (moment(maxOrigin).diff(target)));

        this.zoomIn({
            min,
            max
        });
    };

    zoomIn = (domain) => {
        if(!this.state.waitForLoad) {
            if(!domain.min.isSame(this.state.domain.min) || !domain.max.isSame(this.state.domain.max)){
                //Maximum zoom?
                const duration = domain.max.diff(domain.min);
                const width = this.histoWidth;
                let maxZoom = false;

                //We stop/recap zoom in when 15px = 1min
                if(duration < (width*60000/15)) {
                    domain.max = moment(domain.min).add(width*60000/15, 'ms');
                    this.props.showMessage('Zoom area was extended: limit of 15 pixels = 1 min.');
                    maxZoom = true;
                }

                const domains = [...this.props.domains];
                domains.unshift(domain);

                this.setState({
                    maxZoom,
                    domain
                });
                this.props.updateDomains(domains);
            }
            else{
                this.props.showMessage('Please change time selection before clicking on zoom ;)');
            }
        }
    };

    zoomOut = () => {
        if(this.props.domains.length > 1 && !this.state.waitForLoad){
            const domains = [...this.props.domains];
            domains.shift();

            this.setState({
                maxZoom: false,
                domain: domains[0]
            });
            this.props.updateDomains(domains);
        }
    };

    moveTimeLine = (delta) => {
        if(!this.state.waitForLoad) {
            this.xAxis.clamp(false);

            let min, max;
            if(this.props.domains.length === 1){
                if(delta < 0){
                    min = this.state.domain.min;
                    max = moment(this.xAxis.invert(this.histoWidth - delta));
                    max = moment.min([max, moment().endOf('day')]); //limit to end of day
                }
                else{
                    min = moment(this.xAxis.invert(-delta));
                    max = this.state.domain.max;
                }
            }
            else{
                min = moment(this.xAxis.invert(-delta));
                max = moment(this.xAxis.invert(this.histoWidth - delta));
            }

            this.xAxis.domain([min, max]);
            this.xAxis.clamp(true);

            this.ticks = this.xAxis.ticks(_.floor(this.histoWidth / spaceBetweenTicks));

            this.setState({
                domain: {
                    min,
                    max,
                }
            });
        }
    };

    movedTimeLine = () => {
        const domains = [...this.props.domains];
        domains.splice(0,1,this.state.domain);

        this.props.updateDomains(domains);
        if(domains.length === 1){
            this.props.refreshTimeSpan(domains[0].min, domains[0].max);
        }
    };

    shiftTimeLine = (delta) => {
        const incr = delta/Math.abs(delta)*8;
        co(function * (){
            for(let i = 0; i < Math.abs(delta); i+=8){
                this.moveTimeLine(incr);
                yield delay(10);
            }
            this.movedTimeLine();
        }.bind(this)).catch(e => console.log(e));
    };

    static propTypes = {
        timeSpan: PropTypes.shape({
            start: PropTypes.instanceOf(moment).isRequired,
            stop: PropTypes.instanceOf(moment).isRequired,
            min: PropTypes.instanceOf(moment).isRequired,
            max: PropTypes.instanceOf(moment).isRequired
        }).isRequired,
        domains: PropTypes.array.isRequired,
        histo: PropTypes.shape({
            items: PropTypes.array,
            intervalMs: PropTypes.number
        }).isRequired,
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        style: PropTypes.object,
        timeAggregation: PropTypes.object.isRequired,
        //functions
        loadHisto: PropTypes.func.isRequired,
        customRange: PropTypes.func.isRequired,
        moveStart: PropTypes.func.isRequired,
        showMessage: PropTypes.func.isRequired,
        updateDomains: PropTypes.func.isRequired,
        refreshTimeSpan: PropTypes.func.isRequired,
        onResetTime: PropTypes.func.isRequired,
        loading: PropTypes.bool,
        mode: PropTypes.object,
    };
}


