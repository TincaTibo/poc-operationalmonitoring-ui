import React from 'react';

import {formatNumber, multiFormat} from '../../config/locale'
import Colors from '../../config/colors';

const config = {
    apps: [
        {
            label: 'Logs',
            code: 'LOG',
            legend: (
                <span>
                    Counts the status over the period.
                </span>
            ),
            timeLegend: multiFormat,
            metrics: {
                config: c => c.poc.searchItems,
                count: 3,
                legends: ['Info', 'Warn', 'Fail'],
                colors: [{
                    fill: Colors.success,
                    stroke: Colors.successStroke,
                    text: Colors.successStroke
                },
                {
                    fill: Colors.clientError,
                    stroke: Colors.clientErrorStroke,
                    text: Colors.clientErrorStroke
                },{
                    fill: Colors.serverError,
                    stroke: Colors.serverErrorStroke,
                    text: Colors.serverErrorStroke
                },],
                render: data => formatNumber(data),
                timeField: 'startTime',
                aggs: {
                    eventName: {
                        terms: {field: 'eventName'}
                    }
                },
                bucketMetrics: (b) => {
                    const res = [0, 0, 0];
                    b.eventName.buckets.forEach(state => {
                        if(state.key.startsWith('WARN')){
                            res[1] += state.doc_count;
                        }
                        else if(state.key.startsWith('FAIL')){
                            res[2] += state.doc_count;
                        }
                        else {
                            res[0] += state.doc_count;
                        }
                    });
                    return res;
                }
            }
        }
    ]
};

config.servers = config.apps;

export default config;
