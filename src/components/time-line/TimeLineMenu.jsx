import React, {PropTypes, Component} from 'react';
import Popover from 'material-ui/Popover';
import Checkbox from 'material-ui/Checkbox';
import Button from 'material-ui/IconButton';
import MenuIcon from 'material-ui/svg-icons/action/settings';
import CheckedRadio from 'material-ui/svg-icons/toggle/radio-button-checked';
import UncheckedRadio from 'material-ui/svg-icons/toggle/radio-button-unchecked';
import ToolTip from 'rc-tooltip';
import '../tips/tipDark.css';

import Colors from '../../config/colors';

const styles = {
    button: {
        padding: 2,
        width: 22,
        height: 22,
    },
    checkbox: {
        marginBottom: 0,
    },
    label:{
        fontSize: '13px',
        lineHeight: '24px'
    },
    options:{
        margin: 0,
        padding: 0,
        fontSize: '13px',
        fontFamily: 'Roboto, sans-serif',
    },
    list: {
        maxHeight: 170,
        overflowY: 'auto'
    },
    icon: {
        width: 18,
        height: 18
    }
};

export default class TimeLineMenu extends Component{
    constructor(props) {
        super(props);

        this.state = {
            open: false
        };
    }

    handleTouchTap = (event) => {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            open: true,
            anchorEl: event.currentTarget,
        });
    };

    handleRequestClose = () => {
        this.setState({
            open: false,
        });
    };

    render() {

        return (
            <ToolTip
                placement={this.props.tipPlacement}
                prefixCls={'rc-tooltip-dark'}
                overlay={'Select metric'}
                mouseEnterDelay={0.5}
                destroyTooltipOnHide={true}
            >
                <div style={this.props.style}>
                    <Button
                        style={styles.button}
                        iconStyle={styles.icon}
                        onTouchTap={this.handleTouchTap}
                    >
                        <MenuIcon
                            color={Colors.medium}
                            hoverColor={Colors.itemFillSelected}
                        />
                    </Button>
                    <Popover
                        open={this.state.open}
                        anchorEl={this.state.anchorEl}
                        anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                        targetOrigin={{horizontal: 'right', vertical: 'top'}}
                        onRequestClose={this.handleRequestClose}
                        useLayerForClickAway={false}
                    >
                        <div style={{
                            margin: '8px'
                        }}>
                            <div style={styles.list}>
                                {this.props.options.map(option =>
                                    <Checkbox
                                        key={option.code}
                                        label={option.label}
                                        style={styles.checkbox}
                                        checked={option.code === this.props.selected.code}
                                        labelStyle={styles.label}
                                        iconStyle={{
                                            ...styles.icon,
                                            marginRight: 7,
                                        }}
                                        onCheck={(event, checked) => {
                                            if(checked){
                                                this.props.onSelect(option);
                                            }
                                        }}
                                        checkedIcon={<CheckedRadio/>}
                                        uncheckedIcon={<UncheckedRadio/>}
                                    />
                                )}
                            </div>
                        </div>
                    </Popover>
                </div>
            </ToolTip>
        );
    }

    static propTypes = {
        options: PropTypes.array.isRequired,
        selected: PropTypes.object.isRequired,
        style: PropTypes.object,
        onSelect: PropTypes.func.isRequired,
        tipPlacement: PropTypes.string,
    };

    static defaultProps = {
        tipPlacement: 'top'
    }
}