import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import moment from 'moment';

export default class SimpleMessage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: this.props.message.length > 0,
            message: this.props.message,
            time: moment()
        };
    }

    handleRequestClose = () => {
        this.setState({
            open: false,
            message: ''
        });
    };

    componentWillReceiveProps(nextProps){
        if(nextProps.time.isAfter(this.state.time)){
            this.setState({
                open: true,
                message: nextProps.message,
                time: nextProps.time
            });
        }
    }

    render() {
        return (
            <Snackbar
                open={this.state.open}
                message={this.state.message}
                autoHideDuration={2000}
                onRequestClose={this.handleRequestClose}
            />
        );
    }
}

SimpleMessage.propTypes = {
    message: React.PropTypes.string.isRequired,
    time: React.PropTypes.instanceOf(moment)
};