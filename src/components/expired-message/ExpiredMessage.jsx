import React from 'react';
import Paper from 'material-ui/Paper';
import uriTemplate from "uri-templates";
import ErrorIcon from 'material-ui/svg-icons/alert/error';

const styles = {
    box:{
        width: 350,
        position: 'absolute',
        left: 20,
        bottom: 32,
        zIndex: 100
    },
    container:{
        padding: 10,
        textAlign: 'center'
    },
    label:{
        color: '#555',
        lineHeight: '12px',
        fontSize: '12px',
        fontFamily: 'Roboto, sans-serif',
    },
};

const ExpiredMessage = ({loginUri}) => (
    <Paper
        style={styles.box}
        zDepth={2}
    >
        <div
            style={styles.container}
        >
            <ErrorIcon
                color={'#ff776b'}
                style={{marginRight: 5, display: 'inline-block'}}
            />
            <div style={{display: 'inline-block', position: 'relative', top: -5}}>
                Your token is expired, please <span style={{color:'blue', 'textDecoration': 'underline', 'cursor': 'pointer'}} onClick={() => redirectTo(loginUri)}>login again</span>!
            </div>
        </div>
    </Paper>
);

function redirectTo(urlTemplate){
    localStorage.removeItem("spider.user");
    localStorage.removeItem("spider.token");
    window.location.replace(
        uriTemplate(urlTemplate)
            .fill({uri: window.location.toString()})
    );
}

export default ExpiredMessage;
