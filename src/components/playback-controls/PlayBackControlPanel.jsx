import React, { PropTypes } from 'react';
import moment from 'moment';

import AggSelection from './AggSelection';
import Now from './Now';
import StepBackward from './StepBackward';
import StepForward from './StepForward';
import TimeSpan from './TimeSpan';

const PlayBackControlPanel = (props) => (
    <div style={props.style}>
        <div>
            <Now onClick={props.onGoToNow}/>
            <StepBackward onClick={props.onStepBackward}/>
            <StepForward onClick={props.onStepForward}/>
            <AggSelection timeAggregation={props.timeAggregation}
                          customTimeAggregation={props.customTimeAggregation}
                          onClick={props.onChangeTimeAggregation}
            />
        </div>
        <div style={{
            position: 'absolute',
            right: 7,
            top: 40
        }}>
            <TimeSpan timeSpan={props.timeSpan}
                      customStart={props.onCustomStart}
                      customEnd={props.onCustomEnd}
                      style={{
                          fontSize: 'small',
                          textAlign: 'right'
                      }}
            />
        </div>
    </div>
);

PlayBackControlPanel.propTypes = {
    timeSpan: PropTypes.shape({
        start: PropTypes.instanceOf(moment).isRequired,
        stop: PropTypes.instanceOf(moment)
    }).isRequired,
    isRunning: PropTypes.bool.isRequired,
    timeAggregation: PropTypes.object.isRequired,
    customTimeAggregation: PropTypes.bool.isRequired,
    style: PropTypes.object,

    onGoToNow: PropTypes.func.isRequired,
    onRun: PropTypes.func.isRequired,
    onStepForward: PropTypes.func.isRequired,
    onStepBackward: PropTypes.func.isRequired,
    onChangeTimeAggregation: PropTypes.func.isRequired,
    onCustomStart: PropTypes.func.isRequired,
    onCustomEnd: PropTypes.func.isRequired
};

export default PlayBackControlPanel;