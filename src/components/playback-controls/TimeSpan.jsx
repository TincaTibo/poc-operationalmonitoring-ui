import React, { PropTypes } from 'react';
import moment from 'moment';
import TimeEditor from './TimeEditor';
import {TIME_FORMAT_SECOND} from '../../config/locale';

const format = TIME_FORMAT_SECOND;
const style = {
    width: 90,
    position: 'relative',
    bottom: 0,
    fontSize: '13px',
    height: 16,
    lineHeight: '16px'
};

const underlineStyle = {
    bottom: '-1px'
};

const TimeSpan = (props) => {

    function validateStartChange(start) {
        if(start.isAfter(props.timeSpan.end)) {
            throw new Error("Start cannot be after end!");
        }
        return true;
    }

    function validateEndChange(end) {
        if(end.isBefore(props.timeSpan.start)) {
            throw new Error("End cannot be before start!");
        }
        return true;
    }

    return (
        <span style={_.merge({},props.style)}>
                <TimeEditor id="TFstart"
                            timeFormat={format}
                            time={props.timeSpan.start}
                            onChange={validateStartChange}
                            onValid={props.customStart}
                            style={style}
                            underlineStyle={underlineStyle}
                />
                <br style={{userSelect: 'none'}}/>
                <TimeEditor id="TFend"
                            timeFormat={format}
                            time={props.timeSpan.stop}
                            onChange={validateEndChange}
                            onValid={props.customEnd}
                            style={style}
                            underlineStyle={underlineStyle}
                />
        </span>
    );
};

TimeSpan.propTypes = {
    timeSpan: PropTypes.shape({
        start: PropTypes.instanceOf(moment).isRequired,
        stop: PropTypes.instanceOf(moment)
    }).isRequired,
    customStart: PropTypes.func.isRequired,
    customEnd: PropTypes.func.isRequired,
    style: PropTypes.object
};

export default TimeSpan;