import React, { Component, PropTypes } from 'react';
import Button from 'material-ui/IconButton';
import Icon from 'material-ui/svg-icons/av/skip-next';
import ToolTip from 'rc-tooltip';
import '../tips/tipDark.css';

const style={
    width: 24,
    height: 30,
    padding: 0
};

const StepForward = ({onClick}) => (
    <ToolTip
        placement={'topLeft'}
        prefixCls={'rc-tooltip-dark'}
        overlay={'Step forward'}
        mouseEnterDelay={0.5}
        destroyTooltipOnHide={true}
    >
        <Button style={style}
                onTouchTap={onClick}
        >
            <Icon hoverColor='#64C4E0'/>
        </Button>
    </ToolTip>
);

StepForward.propTypes = {
    onClick: PropTypes.func.isRequired
};

export default StepForward;