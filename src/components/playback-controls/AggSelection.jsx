import React, { Component, PropTypes } from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Divider from 'material-ui/Divider';
import Icon from 'material-ui/svg-icons/image/timelapse';
import {TIME_AGGREGATION_LEVELS} from '../../config/time-aggregation-levels';
import moment from 'moment';
import ToolTip from 'rc-tooltip';
import '../tips/tipDark.css';

const itemStyle = {
    fontSize: 'small',
    lineHeight: '24px',
    minHeight: '20px'
};

const buttonStyle = {
    height: 36,
    padding: 0,
    width: 24
};

const menuStyle = {
};

export default class AggSelection extends Component{

    handleChangeSingle = (event, value) => {
        this.props.onClick(moment.duration(value));
    };

    render () {
        let timeAggAsDays = Math.floor(this.props.timeAggregation.asDays());

        return (
                <IconMenu
                    iconButtonElement={
                        <IconButton style={buttonStyle}>
                            <ToolTip
                                placement={'topLeft'}
                                prefixCls={'rc-tooltip-dark'}
                                overlay={'Time span'}
                                mouseEnterDelay={0.5}
                                destroyTooltipOnHide={true}
                            >
                                <Icon hoverColor='#64C4E0'/>
                            </ToolTip>
                        </IconButton>
                    }
                    onChange={this.handleChangeSingle}
                    value={this.props.timeAggregation.asMilliseconds()}
                    style={menuStyle}
                    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                    targetOrigin={{horizontal: 'right', vertical: 'bottom'}}
                >
                    {this.props.customTimeAggregation?
                        <MenuItem value={this.props.timeAggregation.asMilliseconds()}
                                  primaryText={`${timeAggAsDays>0?timeAggAsDays:''} ${moment.utc(this.props.timeAggregation.asMilliseconds()).format('HH:mm:ss.SSS')}`}
                                  style={itemStyle}/>
                        :''
                    }
                    {this.props.customTimeAggregation ?
                        <Divider />:''
                    }
                    {TIME_AGGREGATION_LEVELS.map(lvl => {
                            return (<MenuItem key={lvl.key}
                                      value={lvl.duration.asMilliseconds()}
                                      primaryText={lvl.display}
                                      style={itemStyle}/>)
                        }
                    )}
                </IconMenu>
        );
    }
}


AggSelection.propTypes = {
    onClick: PropTypes.func.isRequired,
    timeAggregation: PropTypes.object.isRequired,
    customTimeAggregation: PropTypes.bool.isRequired
};
