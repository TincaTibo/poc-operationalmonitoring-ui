import React, { PropTypes } from 'react';
import Button from 'material-ui/IconButton';
import IconNow from 'material-ui/svg-icons/device/access-time';
import ToolTip from 'rc-tooltip';
import '../tips/tipDark.css';

const style={
    width: 24,
    height: 30,
    padding: 0
};

const Now = ({onClick}) => (
    <ToolTip
        placement={'top'}
        prefixCls={'rc-tooltip-dark'}
        overlay={'Now'}
        mouseEnterDelay={0.5}
        destroyTooltipOnHide={true}
    >
        <Button style={style}
                onTouchTap={onClick}
        >
            <IconNow
                hoverColor='#64C4E0'
            />
        </Button>
    </ToolTip>
);

Now.propTypes = {
    onClick: PropTypes.func.isRequired,
};

export default Now;