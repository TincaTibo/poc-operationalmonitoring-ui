import React, { Component, PropTypes } from 'react';
import TextField from 'material-ui/TextField';
import moment from 'moment';

class TimeEditor extends Component {

    constructor(props) {
        super(props);

        this.state = {
            value: this.props.time.format(this.props.timeFormat),
            errorText: ''
        };
    }

    handleChange = (event) => {
        let error = '';
        let newTime = moment(event.target.value, this.props.timeFormat);

        if(!newTime.isValid()){
            error = "Invalid date";
        }
        else{
            try {
                this.props.onChange(newTime);
            }
            catch(e){
                error = e.message;
            }
        }

        this.setState({
            value: event.target.value,
            errorText: error
        });
    };

    validChange = (event) => {
        let error;
        let newTime = moment(event.target.value, this.props.timeFormat);

        if(!this.state.errorText){
            try {
                this.props.onValid(newTime);
            }
            catch(e){
                error = e.message;
                this.myTextField.focus();
            }
        }
        else{
            error = "Please correct input.";
            this.myTextField.focus();
        }

        this.setState({
            value: event.target.value,
            errorText: error
        });
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            value: nextProps.time.format(nextProps.timeFormat),
            errorText: ''
        });
    }

    keyDown = (event) => {
        switch(event.key){
            case 'Enter':
            case 'Escape':
                this.myTextField.blur();
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '-':
            case ':':
            case '.':
                let s = event.currentTarget.selectionStart;
                let val = event.currentTarget.value;
                event.currentTarget.value = val.substr(0, s) + val.substr(s+1);
                event.currentTarget.selectionEnd = s;
                break;
            case 'ArrowLeft':
            case 'ArrowRight':
            case 'End':
            case 'Home':
                break;
            default:
                event.preventDefault();
        }
    };

    render() {
        return (
            <TextField
                ref={(ref) => { this.myTextField = ref }}
                id={this.props.id}
                value={this.state.value}
                onBlur={this.validChange}
                onKeyDown={this.keyDown}
                onChange={this.handleChange}
                errorText={this.state.errorText}
                style={this.props.style}
                underlineStyle={this.props.underlineStyle}
                underlineFocusStyle={this.props.underlineStyle}
            />
        );
    }
}

TimeEditor.propTypes = {
    id: PropTypes.string.isRequired,
    time: PropTypes.instanceOf(moment).isRequired,
    timeFormat: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onValid: PropTypes.func.isRequired,
    style: PropTypes.object,
    underlineStyle: PropTypes.object
};

export default TimeEditor;