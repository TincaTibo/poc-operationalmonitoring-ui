import React, { Component, PropTypes } from 'react';
import Chip from 'material-ui/Chip';

const styles = {
    item: {
        cursor: 'pointer',
    },
    chip: {
        margin: 'auto',
        display: 'inline-flex',
        borderRadius: '4px'
    },
    chipLabelStyle: {
        fontSize: '11px',
        lineHeight: '15px',
        paddingLeft: '2px',
        paddingRight: '2px',
        fontFamily: 'Monospace'
    }
};

const widthByCar = 7;

const Breadcrumb = (props) => {

    const history = [...props.history];
    let truncated = false;
    let currentWidth = 0;
    const separator = '\u00a0/\u00a0';
    const ellipsis = '... / ';

    const breadcrumb = history.reverse().map((item, index) => {
        if(index > 0){
            currentWidth += separator.length * widthByCar;
        }
        currentWidth += (item.name.length + item.type.length + 1) * widthByCar;

        if(currentWidth + ellipsis.length > props.width){
            truncated = true;
            return '';
        }
        else{
            return <span key={index}>
                <Chip style={styles.chip}
                      backgroundColor='#0044aa'
                      labelColor='#ffffff'
                      labelStyle={styles.chipLabelStyle}
                >
                    {item.type}
                </Chip>
                &nbsp;
                <span style={styles.item}
                      onClick={() => {props.openResource(item.type, item.input)}}
                >
                    {item.name}
                </span>
                { index > 0 &&
                    <span>
                          {separator}
                    </span>
                }
            </span>
        }
    }).reverse();

    return <div style={{
        width:'100%',
        overflowX: 'auto',
        fontSize: '11px',
        fontFamily: 'Monospace'
    }}>
        {truncated &&
            <span>{ellipsis}</span>
        }
        {breadcrumb}
    </div>

};

Breadcrumb.propTypes = {
    history: PropTypes.array.isRequired,
    openResource: PropTypes.func.isRequired,
    width: PropTypes.number
};

export default Breadcrumb;