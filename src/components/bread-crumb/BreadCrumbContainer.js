import { connect } from 'react-redux';
import BreadCrumb from './BreadCrumb';
import {DetailsActions} from '../../actions';

const mapStateToProps = (state) => {
    return {
        width: state.details.size -10,
        history: state.details.history
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        openResource: (resourceType, id) => {
            dispatch(DetailsActions.requestOpenDetails(resourceType, id))
        }
    }
};

const BreadCrumbContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(BreadCrumb);

export default BreadCrumbContainer;