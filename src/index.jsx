import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import uriTemplate from 'uri-templates';
import createSagaMiddleware from 'redux-saga';
import { select, scaleTime } from 'd3';
import moment from 'moment';
import co from 'co';
import jwtDecode from 'jwt-decode';
import _ from 'lodash';
import Joi from 'joi';
import qs from 'query-string';

import {searchBO, getConfig} from './containers/BORequests';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();
const muiTheme = getMuiTheme({

});

const parametersSchema = Joi.object().keys({
    start: Joi.string().isoDate().required(),
    stop: Joi.string().isoDate().required(),
    min: Joi.string().isoDate().required(),
    max: Joi.string().isoDate().required(),
    domainMin: Joi.string().isoDate().required(),
    domainMax: Joi.string().isoDate().required(),
});

import networkViewApp from './reducers';
import networkViewSaga from './sagas';
import AppContainer from './containers/AppContainer';

import {TIME_AGGREGATION_LEVELS} from './config/time-aggregation-levels';

//Display loading circle
displayAppLoading();

co(function * (){
    //Load config from server that loaded it from config service
    const config = yield getConfig();

    try{
        //Look on HTML5 store if token exists
        if(localStorage.getItem('access_token')){
            const token = localStorage.getItem('access_token');

            //Check token validity
            const tokenDecoded = jwtDecode(token);
            const expireDate = moment.unix(tokenDecoded.exp);
            if(expireDate.isAfter(moment())){

                //check if exist a previous state or link to load customer / whisperer
                //local storage
                let previousState;
                if(localStorage.getItem('spider.pocState')) {
                    try {
                        previousState = JSON.parse(localStorage.getItem('spider.pocState'));
                    }
                    catch (e) {
                        console.log('Could not parse previous state');
                    }
                }

                //check parameters in querystring
                let stateToLoad = {};
                if(window.location.search){
                    const querystring = qs.parse(window.location.search);

                    const validationError = Joi.validate(querystring, parametersSchema, { allowUnknown: true }).error;
                    if(validationError){
                        console.warn('Invalid querystring, ignoring it');
                        console.warn(validationError);
                        stateToLoad = {};
                    }
                    else{
                        stateToLoad = {
                            time: {
                                timeSpan: {
                                    min: moment(querystring.min),
                                    max: moment(querystring.max),
                                    start: moment(querystring.start),
                                    stop: moment(querystring.stop)
                                },
                                timeAggregation: moment.duration(moment(querystring.stop).diff(moment(querystring.start))),
                                timeLineHisto: {
                                    domains: [{
                                        min: moment(querystring.min),
                                        max: moment(querystring.max)
                                    }]
                                },
                            },
                        };

                        if(querystring.domainMin && querystring.domainMax &&
                            (querystring.domainMin !== querystring.min || querystring.domainMax !== querystring.max)){
                            stateToLoad.time.timeLineHisto.domains.unshift({
                                min: moment(querystring.domainMin),
                                max: moment(querystring.domainMax)
                            });
                        }
                    }
                }

                //Prepare previousStates
                if(previousState){
                    try{
                        prepareIncomingState(previousState);
                    }
                    catch(e){
                        console.log('Could not restore previous state');
                        console.log(e);
                    }
                }

                let min, max;

                let view = 'apps';
                let response;

                //Get timespan for whisperer and initialize state
                switch(view){
                    default:
                        //Get timespan for redis (least number of items) and initialize state
                        response = yield searchBO('timeSpanQuery', config.poc.searchItems, token, {timeField: 'startTime'});
                        break;
                }

                if (response && response.total > 0) { //there is something in  history
                    let global = response.aggs;

                    min = moment(global.minWindow.value_as_string);
                    max = moment(global.maxWindow.value_as_string);

                    [min, max] = scaleTime()
                        .domain([min, max])
                        .nice()
                        .domain();

                    min = moment(min).second(0).millisecond(0);
                    max = moment(max);
                    if(max.second()!==0 || max.millisecond()!==0){
                        max.add(1, 'minute').millisecond(0).second(0);
                    }
                }

                //If could not get min/max or if no selectedWhisperer
                if(!min || !max){
                    min = moment().startOf('day');
                    max = moment().startOf('minute');
                }

                const default_agg_level = TIME_AGGREGATION_LEVELS[1].duration;

                const start = moment.min(
                    moment.max(
                        moment(max).subtract(default_agg_level),
                        moment(min)),
                    moment().subtract(default_agg_level).second(0).millisecond(0)
                );
                const stop = moment(start).add(default_agg_level);

                //Determine center of current screen area
                const bottomDrawerOpen = true;
                const panelHeight = 200;
                const map = select('#root');
                const screen = {
                    width: map.property('clientWidth'),
                    height: map.property('clientHeight')
                };
                const origin = {
                    x: Math.round(screen.width / 2),
                    y: Math.round((screen.height - (bottomDrawerOpen ? panelHeight + 47 : 0)) / 2)
                };

                //create initial state
                const initialState = {
                    userInfo:{
                        isAdmin: tokenDecoded.isAdmin,
                        token,
                        expireDate,
                        rights: tokenDecoded.rights,
                        selectedWhisperers : [],
                        whisperers : [],
                        historySteps: 0,
                    },
                    config,
                    time: {
                        timeSpan: {
                            min,
                            max,
                            start,
                            stop
                        },
                        timeAggregation: default_agg_level,
                        customTimeAggregation: false,
                        isRunning: false,
                        timeLineHisto: {
                            loading: false,
                            mode: 'LOG',
                            domains: [{
                                min,
                                max
                            }],
                            histo: {
                                items: [],
                                intervalMs: null
                            }
                        },
                    },
                    userMessages:{
                        simpleMessage: '',
                        simpleMessageTime: moment(),
                    },
                    view:{
                        screen,
                        zoom: {
                            start: null,
                            stop: null
                        },
                        apps: {},
                        db: {},
                        main: {},
                        whisperers: {},
                        servers: {},
                        ui: {}
                    },
                    grids:{
                        LOG: {
                            columns: [
                                {id:'park'},{id:'pos'},{id:'eventName'},{id:'eventStatus'},
                                {id:'startDate'},{id:'endDate'},{id:'updateDate'},
                                {id:'eventType'},{id:'family'},{id:'actions'}
                            ],
                            sort: { key: 'startTime', order : 'desc' },
                            items: [],
                            total: 0,
                            time: null,
                            lastUpdated: null,
                            selected: [],
                            query: ''
                        }
                    },
                    main:{
                        view: 'apps',
                        autoRefreshActive: true,
                        autoRefreshDelay: moment.duration(config.defaultAutoRefreshDelay).asSeconds() || 60,
                        refreshing: false
                    },
                    details: {
                        id: null,
                        type: null,
                        open: false,
                        pinned: false,
                        size: 600,
                        history: [],
                        links: {},
                        activeTab: 0,
                    },
                    menu: {
                    },
                    cache: {
                        items: []
                    }
                };

                //merge with input state
                if(previousState){
                    _.merge(initialState, previousState);
                }
                if(stateToLoad){
                    _.merge(initialState, stateToLoad);
                }

                //Override min and max of highest zoom domains in order not to use older max time window when reloading
                initialState.time.timeLineHisto.domains[initialState.time.timeLineHisto.domains.length-1]={min, max};

                //safety belt for panels height and width
                initialState.details.size = _.clamp(initialState.details.size, 400, screen.width - 300);

                //create saga, store
                const sagaMiddleware = createSagaMiddleware();
                const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

                let store = createStore(networkViewApp,
                    initialState,
                    composeEnhancers(applyMiddleware(sagaMiddleware))
                );

                sagaMiddleware.run(networkViewSaga, store.dispatch);

                //Load application
                ReactDOM.render(
                    <MuiThemeProvider muiTheme={muiTheme}>
                        <Provider store={store}>
                            <AppContainer/>
                        </Provider>
                    </MuiThemeProvider>,
                    document.getElementById('root')
                )
            }
            else { //Else load for login/pass page
                displayError(
                    <span>
                        Your token has expired! Please first reconnect to SSS SIT0 UI.<br/>
                        <div style={{paddingLeft:10}}>
                            -&nbsp;
                            <span
                                style={{color: 'blue', textDecoration: 'underline', cursor: 'pointer'}}
                                onClick={() => {
                                    clearState();
                                    redirectTo(config.login.uri);
                                }}
                            >
                                Login again
                            </span>
                        </div>
                    </span>);
            }
        }
        else { //Else load for login/pass page
            displayError(
                <span>
                    Please first connect to SSS SIT0 UI.<br/>
                    <div style={{paddingLeft:10}}>
                        -&nbsp;
                        <span
                            style={{color: 'blue', textDecoration: 'underline', cursor: 'pointer'}}
                            onClick={() => {
                                clearState();
                                redirectTo(config.login.uri);
                            }}
                        >
                            Login again
                        </span>
                    </div>
                </span>);
        }
    }
    catch(err){
        displayError(
            <span>
                {err.message}
            </span>
            , err);
    }

}).catch((err) => {
    displayError('Could not load configuration... I\'m a bit perplexed!', err);
});

function redirectTo(urlTemplate){
    localStorage.removeItem('access_token');
    const newLocation = uriTemplate(urlTemplate)
        .fill({uri: window.location.toString()});
    window.location.replace(newLocation);
}

function prepareIncomingState(previousState){
    previousState.time.timeSpan.start = moment(previousState.time.timeSpan.start);
    previousState.time.timeSpan.stop = moment(previousState.time.timeSpan.stop);
    previousState.time.timeAggregation = moment.duration(previousState.time.timeAggregation);
    previousState.time.timeLineHisto.domains.forEach(domain => {
        domain.min = domain.min ? moment(domain.min) : null;
        domain.max = domain.max ? moment(domain.max) : null;
    });
}

function displayAppLoading(){
}

function displayError(msg, error){
    ReactDOM.render(
        <div style={{
            backgroundColor: 'white',
            border: '1px solid black',
            width: '50%',
            margin: 'auto',
            textAlign: 'left',
            padding: 20,
            zIndex: 4100
        }}>
            <span style={{fontWeight: 'bold'}}>Loading operational reporting failed: </span><br/>
            <br/>
            {msg}
        </div>,
        document.getElementById('loadingDiv')
    );

    if(error){
        console.log(error);
    }
}

function clearState(){
    localStorage.removeItem('spider.pocState');
}
