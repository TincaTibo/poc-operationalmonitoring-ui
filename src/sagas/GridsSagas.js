import { select, put, take, fork, cancel } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import moment from 'moment';
import _ from 'lodash';

import {
    GridsActions, GRIDS_ACTIONS, UserMessagesActions, UserActions, USER_ACTIONS,
    TimeActions, TIME_ACTIONS, CacheActions
} from '../actions';
import {
    prepareLog,
} from './CollectionsUtils';
import { searchBO, nextPage } from '../containers/BORequests';

function *fetch(collection){
    let config,
        prepare,
        size,
        aggs,
        items,
        total,
        nextPage
    ;

    const {
        configuration,
        token,
        start,
        stop,
        query,
        sort,
        selectedWhisperers
    } = yield select(state => ({
        configuration: state.config,
        token: state.userInfo.token,
        start: +state.time.timeSpan.start/1e3,
        stop: +state.time.timeSpan.stop/1e3,
        query: state.grids[collection].query,
        nextPageQuery: state.grids[collection].nextPage ? state.grids[collection].nextPage['sc:query'] : null,
        sort: state.grids[collection].sort,
        selectedWhisperers: state.userInfo.selectedWhisperers.length ? state.userInfo.selectedWhisperers : undefined
    }));

    switch(collection){
        case 'LOG':
            config = configuration.poc.searchItems;
            prepare = prepareLog;
            break;
    }

    if(config){

        const response = yield searchBO(
            aggs ? 'computeAggs' : 'searchItems',
            config,
            token,
            {
                start,
                stop,
                query,
                sort,
                size,
                aggs,
                whisperers: selectedWhisperers,
            });


        switch(collection){
            default:
                items = response.items.map(i => prepare(i));
                total = response.total;
                nextPage = response.nextPage;
                break;
        }

        yield put(GridsActions.updateCollection({
            collection,
            items,
            total,
            nextPage,
            time: moment(),
            lastUpdated: moment.duration(0),
            shouldSearchForSelected: false
        }));
    }
}

function *updateGrid({automatic, collection}){
    try{
        yield fetch(collection);
    }
    catch(e){
        yield put(UserMessagesActions.showMessage(`Unexpected error while refreshing grid, check your query!`));
        yield put(GridsActions.searchError({
            collection,
            searchError: `Unexpected error while refreshing grid, check your query!`
        }));
        console.error(e);

        if(e.target && e.target.status === 401){
            yield put(UserMessagesActions.showMessage(`Your token is too old, please refresh the page to reconnect.`));
        }
    }
}

function* requestSearch(){
    while (true) {
        const params = yield take(GRIDS_ACTIONS.REQUEST_SEARCH);

        //Save state in history before proceeding
        yield put(UserActions.saveStateInHistory());
        yield take(USER_ACTIONS.INCREMENT_HISTORY);

        yield put(GridsActions.doSearch(params));
    }
}

function* requestUpdateGrid(){
    let taskId;
    while (true) {
        const params = yield take([
            GRIDS_ACTIONS.REQUEST_UPDATE_GRID,
            GRIDS_ACTIONS.DO_SEARCH
        ]);

        if (taskId) {
            yield cancel(taskId);
        }

        taskId = yield fork(shouldUpdateGrid, params);
    }
}

function *shouldUpdateGrid({automatic, collection}){
    if(!collection){
        const {
            updateLogs,
        } = yield select(state => ({
            updateLogs: state.main.view === 'servers',
        }));

        if(updateLogs){
            yield updateGrid({automatic, collection: 'LOG'});
        }
    }
    else{
        yield updateGrid({automatic, collection});
    }
}

function* requestSortGrid(){
    let taskId;
    while (true) {
        const params = yield take(GRIDS_ACTIONS.SORT_GRID);

        if (taskId) {
            yield cancel(taskId);
        }

        taskId = yield fork(sortGrid, params);
    }
}

function* sortGrid({collection}){
    try{
        yield fetch(collection);
    }
    catch(e){
        yield put(UserMessagesActions.showMessage(`Unexpected error while sorting grid!`));
        console.error(e);

        if(e.target && e.target.status === 401){
            yield put(UserMessagesActions.showMessage(`Your token is too old, please refresh the page to reconnect.`));
        }
    }
}

function* requestNextPage(){
    while (true) {
        const {collection} = yield take(GRIDS_ACTIONS.REQUEST_NEXT_PAGE_COLLECTION);

        const {
            nextPageQuery,
            configuration,
            token,
            count,
        } = yield select(state => ({
            nextPageQuery: state.grids[collection].nextPage ? state.grids[collection].nextPage['sc:query'] : null,
            count: state.grids[collection].items.length,
            configuration: state.config,
            token: state.userInfo.token,
        }));

        if(nextPageQuery){
            let config, prepare;
            switch(collection){
                case 'LOG':
                    config = configuration.poc.searchItems;
                    prepare = prepareLog;
                    break;
            }

            try{
                const response  = yield nextPage(config, token, nextPageQuery, collection);

                response.items.forEach(i => prepare(i));

                yield put(GridsActions.nextPageCollection({
                    collection,
                    items: response.items,
                    nextPage: response.nextPage,
                    total: response.items.length ? response.total : count, //if at the end, change total
                    shouldSearchForSelected: false,
                    time: moment(),
                    lastUpdated: moment.duration(0)
                }));
            }
            catch(e){
                yield put(UserMessagesActions.showMessage('Error while loading data from server'));
                console.error(e);
                if(e.target && e.target.status === 401){
                    yield put(UserMessagesActions.showMessage(`Your token is too old, please refresh the page to reconnect.`));
                }
            }
        }
        else{
            yield put(GridsActions.nextPageCollection({
                collection,
                items: [],
                nextPage: null,
                shouldScrollToSelected: false
            }));
        }
    }
}

function *showSelected(){
    while (true) {
        const {collection} = yield take(GRIDS_ACTIONS.SHOW_SELECTED);

        const {
            selected,
        } = yield select(state => ({
            selected: state.grids[collection].selected,
        }));

        if(selected && selected.length){
            //Compute timespan

            let startFunc, stopFunc;
            switch(collection){
                case 'LOG':
                    startFunc = i => i.json.time;
                    stopFunc = i => i.json.time;
                    break;
                default:
                    yield put(UserMessagesActions.showMessage('View not handled'));
            }

            if(startFunc && stopFunc){
                const start = moment.unix(_.min(selected.map(startFunc)));
                const stop = moment.unix(_.max(selected.map(stopFunc)));

                const query = `@id:(${selected.map(i => i['@id']).join(' OR ')})`;
                yield put(GridsActions.requestSearch({collection, query}));
                yield take(GRIDS_ACTIONS.UPDATE_COLLECTION);

                yield put(TimeActions.requestResetTime());
                yield take(TIME_ACTIONS.UPDATE_DOMAINS_TIMELINE);

                if(start && stop){
                    yield put(TimeActions.setCustomRange(start, stop, false));
                }
            }
        }
        else{
            yield put(UserMessagesActions.showMessage('No selection'));
        }
    }
}

function* refreshDate(){
    while(true){
        yield delay(20000);
        yield put(GridsActions.refreshDate(moment()));
    }
}

export default function *gridsSagas() {
    yield [
        requestSortGrid(),
        requestNextPage(),
        requestUpdateGrid(),
        showSelected(),
        requestSearch(),
        refreshDate()
    ]
}