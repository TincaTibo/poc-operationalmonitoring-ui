import { select, put, take, fork } from 'redux-saga/effects';

import {CACHE_ACTIONS, CacheActions, UserMessagesActions} from '../actions';
import {getItem} from '../containers/BORequests';

function *requestLoadInCache() {
    while (true) {
        const {collection, id} = yield take([
            CACHE_ACTIONS.REQUEST_LOAD_IN_CACHE,
        ]);

        yield fork(loadInCache,collection,id);
    }
}

function *loadInCache(collection, id){
    const {
        token,
        shortUris,
    } = yield select(state => ({
        token: state.userInfo.token,
        shortUris: state.config.shortUris,
    }));

    //Load Item
    try{
        const item = yield getItem({
            uri: id.replace(/^sss:/, shortUris.sss),
            timeout: 2000
        }, token);

        if(item){
            yield put(CacheActions.loadInCache({collection, item}));
        }
    }
    catch(e){
        console.error(e);

        if(e.target && e.target.status === 401){
            yield put(UserMessagesActions.showMessage(`Your token is too old, please refresh the page to reconnect.`));
        }
        else{
            yield put(UserMessagesActions.showMessage(`Error while resolving item.`));
        }
    }
}

export default function *cacheSagas() {
    yield [
        requestLoadInCache()
    ]
}