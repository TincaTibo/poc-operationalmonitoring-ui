import { put, take, select, fork, cancel, spawn } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import moment from 'moment';

import {
    UPDATE_VIEW_ACTIONS, TIME_ACTIONS,
    UpdateViewActions, UserMessagesActions,
    TimeActions, MainActions, GridsActions, CommonViewActions, NetworkMapActions
} from '../actions';

function *requestUpdateView(){
    while (true) {
        const { force, resetTimeSpan, automatic, fromTime } = yield take(UPDATE_VIEW_ACTIONS.REQUEST_UPDATE_VIEW);

        try {
            let actual, toRefresh = [];

            yield spawn(showRequest);

            if(force){
                if(resetTimeSpan){
                    yield put(TimeActions.requestResetTime());
                }
                else{
                    yield put(TimeActions.requestRefreshTimeSpan());
                }

                //Continue when refresh is done
                actual = yield take(TIME_ACTIONS.REFRESH_TIME_SPAN);
            }

            const {
                view,
                shouldRefresh
            } = yield select(state => ({
                view: state.main.view,
                shouldRefresh:
                //if current selection can be updated by standard process
                    (state.time.timeSpan.stop.isAfter(moment().subtract(1, 'MINUTES'))
                        //or manual update
                        || !automatic)
            }));

            toRefresh = [
                put(TimeActions.requestRefreshHisto()),
            ];

            if(shouldRefresh) {
                toRefresh.unshift(put(GridsActions.requestUpdateGrid()));
            }
            if(!fromTime){
                toRefresh.unshift(put(CommonViewActions.requestUpdateCharts({view})));
            }

            yield toRefresh;
        }
        catch (e) {
            yield put(UserMessagesActions.showMessage(`Unexpected error!`));
            console.log(e);
        }
    }
}

function *showRequest(){
    yield put(MainActions.refreshing(true));
    yield delay(2000);
    yield put(MainActions.refreshing(false));
}

function *startAutoRefresh(){
    yield take(UPDATE_VIEW_ACTIONS.START_AUTO_REFRESH_VIEW);

    const active = yield select(state => state.main.autoRefreshActive);
    yield put(UpdateViewActions.autoRefresh(active));
}

function *autoRefresh(){
    let taskId;

    while (true) {
        const { active } = yield take(UPDATE_VIEW_ACTIONS.AUTO_REFRESH_VIEW);

        if(active){
            taskId = yield fork(refreshTask);
        }
        else{
            if(taskId){
                yield cancel(taskId);
            }
        }
    }
}

function *refreshTask(){
    while(true){
        const {
            active,
            refreshDelay,
            zoom,
        } = yield select(state => ({
            active: state.main.autoRefreshActive && state.userInfo.expireDate.isAfter(moment()),
            refreshDelay: state.main.autoRefreshDelay,
            zoom: {
                min: state.time.timeLineHisto.domains[0].min,
                max: state.time.timeLineHisto.domains[0].max,
                levels: state.time.timeLineHisto.domains.length
            }
        }));

        const now = moment();
        if(
            active
            && (zoom.levels === 1
                || (now.isSameOrBefore(zoom.max) && now.isSameOrAfter(zoom.min)))
        ){
            yield put(UpdateViewActions.updateView({force: true, automatic: true}));
        }

        yield delay(refreshDelay * 1e3);
    }
}

export default function* updateViewSagas() {
    yield [
        requestUpdateView(),
        autoRefresh(),
        startAutoRefresh()
    ]
}