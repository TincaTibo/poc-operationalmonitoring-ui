import moment from 'moment';
import _ from 'lodash';

export function prepareLog(l){
    l.startTime = l.startTime && moment(l.startTime);
    l.endTime = l.endTime && moment(l.endTime);
    l.creationDate = l.creationDate && moment(l.creationDate);
    l.lastUpdatedDate = l.lastUpdatedDate && moment(l.lastUpdatedDate);
    l.updateDatetime = l.updateDatetime && moment(l.updateDatetime);
    return l;
}

export function findResourceInCollection(collection, id){
    return _.find(collection, {'@id': id})
}