import { select, take, put, fork, cancel } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import moment from 'moment';
import {scaleTime} from 'd3';
import _ from 'lodash';

import {TIME_ACTIONS, UpdateViewActions, TimeActions, UserMessagesActions, GRIDS_ACTIONS} from '../actions';
import { searchBO } from '../containers/BORequests';

import ConfigViews from '../components/time-line/ConfigViews';
import {TIME_AGGREGATION_LEVELS} from '../config/time-aggregation-levels';

let runTask;

function *requestRefreshTimeSpan(){
    while (true) {
        const {force, shouldResetSelection} = yield take(TIME_ACTIONS.REQUEST_REFRESH_TIME_SPAN);

        try {
            const {
                poc,
                token,
                view,
                start,
                stop,
            } = yield select(state => ({
                poc: state.config.poc,
                token: state.userInfo.token,
                view: state.main.view,
                start: state.time.timeSpan.start,
                stop: state.time.timeSpan.stop,
            }));

            let min, max, response;
            switch(view){
                default:
                    response = yield searchBO('timeSpanQuery', poc.searchItems, token, {timeField: 'startTime'});
                    break;
            }

            if (response && response.total > 0) { //there is something in  history
                let global = response.aggs;

                min = moment(global.minWindow.value_as_string);
                max = moment(global.maxWindow.value_as_string);

                [min, max] = scaleTime()
                    .domain([min, max])
                    .nice()
                    .domain();

                min = moment(min);
                max = moment(max);
            }

            if(!min) {
                min = moment().startOf('day');
                max = moment().endOf('minute');
            }

            //shoudlResetSelection is to handle case when coming back from UI when selected time is outside new max time range
            if(shouldResetSelection && (start.isBefore(min) || stop.isAfter(max))){
                const default_agg_level = TIME_AGGREGATION_LEVELS[1].duration;

                const start = moment.min(
                    moment.max(
                        moment(max).subtract(default_agg_level),
                        moment(min)),
                    moment().subtract(default_agg_level).second(0).millisecond(0)
                );
                const stop = moment(start).add(default_agg_level);
                yield put(TimeActions.customRange(start, stop));
            }

            yield put(TimeActions.refreshTimeSpan(min,max,force));
        }
        catch (e) {
            yield put(UserMessagesActions.showMessage(`Unexpected error!`));
            console.log(e);
        }
    }
}

function *requestResetTime(){
    while (true) {
        const {shouldResetSelection} = yield take(TIME_ACTIONS.REQUEST_RESET_TIME);

        yield put(TimeActions.requestRefreshTimeSpan(true, shouldResetSelection));
        yield take(TIME_ACTIONS.UPDATE_DOMAINS_TIMELINE);

        const {
            domains
        } = yield select(state => ({
            domains: state.time.timeLineHisto.domains
        }));

        if(domains.length !== 1){
            yield put(TimeActions.updateDomains([domains[domains.length-1]]));
        }
    }
}

function* requestStepForward(){
    while(true){
        yield take(TIME_ACTIONS.REQUEST_STEP_FORWARD);

        yield put(TimeActions.stepForward());
        yield put(UpdateViewActions.updateView({fromTime: true}));
    }
}

function* requestStepBackward(){
    while(true){
        yield take(TIME_ACTIONS.REQUEST_STEP_BACKWARD);

        yield put(TimeActions.stepBackward());
        yield put(UpdateViewActions.updateView({fromTime: true}));
    }
}

function* requestCustomStart(){
    while(true){
        const {newTime} = yield take(TIME_ACTIONS.REQUEST_CUSTOM_START);

        const isRunning = yield select((state) => (state.time.isRunning));
        if(isRunning){
            yield put(TimeActions.setRun(false));
        }

        yield put(TimeActions.customStart(newTime));

        yield put(UpdateViewActions.updateView({fromTime: true}));
    }
}

function* requestCustomEnd(){
    while(true){
        const {newTime} = yield take(TIME_ACTIONS.REQUEST_CUSTOM_END);

        const isRunning = yield select((state) => (state.time.isRunning));
        if(isRunning){
            yield put(TimeActions.setRun(false));
        }

        yield put(TimeActions.customEnd(newTime));

        yield put(UpdateViewActions.updateView({fromTime: true}));
    }
}

function* requestGoTo(){
    while(true){
        const {newTime} = yield take(TIME_ACTIONS.REQUEST_GOTO);

        const {
            isRunning,
            timeSpan,
            timeAggregation
        } = yield select(state => ({
            isRunning: state.time.isRunning,
            timeSpan: state.time.timeSpan,
            timeAggregation: state.time.timeAggregation,
        }));

        if(isRunning){
            yield put(TimeActions.setRun(false));
        }

        const halfTimeAgg = moment.duration(timeAggregation.asMilliseconds()/2);
        const newStop = moment(newTime).add(halfTimeAgg);
        const newStart = moment(newTime).subtract(halfTimeAgg);

        const newTimeSpan = {
            start: moment(newStart),
            stop: moment(newStop),
            min: newStart.isBefore(timeSpan.min) ? moment(newStart) : timeSpan.min,
            max: newStop.isAfter(timeSpan.max) ? moment(newStop) : timeSpan.max
        };

        yield [
            put(TimeActions.refreshTimeSpan(newTimeSpan.min, newTimeSpan.max, true)),
            put(TimeActions.customRange(newTimeSpan.start, newTimeSpan.stop)),
        ];

        yield put(UpdateViewActions.updateView({fromTime: true}));
    }
}

function* requestTimeAggLevel(){
    while(true){
        const {duration} = yield take(TIME_ACTIONS.REQUEST_TIME_AGG_LEVEL);

        const isRunning = yield select((state) => (state.time.isRunning));
        if(isRunning){
            yield put(TimeActions.setRun(false));
        }

        yield put(TimeActions.timeAggLevel(duration));
        yield put(UpdateViewActions.updateView({fromTime: true}));
    }
}

function* requestCustomRange(){
    while (true) {
        const {start, stop, updateView} = yield take(TIME_ACTIONS.REQUEST_CUSTOM_RANGE);

        const isRunning = yield select((state) => (state.time.isRunning));
        if(isRunning){
            yield put(TimeActions.setRun(false));
        }

        yield put(TimeActions.customRange(start, stop));
        yield put(UpdateViewActions.updateView({force: updateView, fromTime: true}));
    }
}

function* requestMoveStart(){
    while(true){
        const {start} = yield take(TIME_ACTIONS.REQUEST_MOVE_START);

        const isRunning = yield select((state) => (state.time.isRunning));
        if(isRunning){
            yield put(TimeActions.setRun(false));
        }

        yield put(TimeActions.moveStart(start));
        yield put(UpdateViewActions.updateView({fromTime: true}));
    }
}

function* running(){
    while(true){
        yield delay(1000);
        yield put(TimeActions.setStepForward());
        yield take(TIME_ACTIONS.STEP_FORWARD);

        //if(timeEnd > maxspan, stop simulation)
        const {
            stop,
            max
        } = yield select(state => ({
            stop: state.time.timeSpan.stop,
            max: state.time.timeSpan.max,
        }));
        if(stop.isSameOrAfter(max)){
            yield put(TimeActions.setRun(false));
        }
    }
}

function* requestRun(){
    while (true) {
        const {shouldRun} = yield take(TIME_ACTIONS.REQUEST_RUN);

        const {
            wasRunning,
            timeAggregation
        } = yield select((state) => ({
            wasRunning: state.time.isRunning,
            timeAggregation: state.time.timeAggregation,
        }));

        //if time aggregation > 1min, reject (anti Remi Lyonnet pattern)
        if(timeAggregation <= moment.duration('PT1M')){
            // if we should start running, fork to run step forward every 1s
            if (shouldRun && !wasRunning) {
                runTask = yield fork(running);
            }
            // if we should stop running, cancel task
            else if (!shouldRun && wasRunning && runTask) {
                yield cancel(runTask);
            }

            yield put(TimeActions.run(shouldRun));

            if (wasRunning && !shouldRun) {
                yield put(UserMessagesActions.showMessage('Simulation Paused.'));
            }
        }
        else{
            yield put(UserMessagesActions.showMessage('You cannot run simulation with steps over 1 min.'));
        }
    }
}

function* requestLoadHisto() {
    let taskId;
    while (true) {
        let params = yield take([
            TIME_ACTIONS.REQUEST_REFRESH_HISTO,
            TIME_ACTIONS.SET_HISTO_OPTION,
            TIME_ACTIONS.REQUEST_LOAD_HISTO,
            GRIDS_ACTIONS.DO_SEARCH
        ]);


        if([
            TIME_ACTIONS.REQUEST_REFRESH_HISTO,
            TIME_ACTIONS.SET_HISTO_OPTION,
            GRIDS_ACTIONS.DO_SEARCH
        ].includes(params.type)){
            params = yield select(state => ({
                intervalMs: state.time.timeLineHisto.histo.intervalMs,
                start: state.time.timeLineHisto.domains[0].min,
                end: state.time.timeLineHisto.domains[0].max
            }));
        }

        if(params.intervalMs){
            if (taskId) {
                yield cancel(taskId);
            }
            taskId = yield fork(loadHisto, params);
        }
    }
}

function* loadHisto({intervalMs, start, end}){
    const {
        timer,
        view,
        token,
        config,
        oldHisto,
        codeMode,
        selectedWhisperers
    } = yield select(state => ({
        timer: state.config.view.progressBarTimer,
        view: state.main.view,
        token: state.userInfo.token,
        config: state.config,
        oldHisto: state.time.timeLineHisto.histo,
        codeMode: state.time.timeLineHisto.mode,
        selectedWhisperers: state.userInfo.selectedWhisperers
    }));

    //launch task
    const waitTask = yield fork(function *waiting(){
        yield delay(moment.duration(timer).asMilliseconds());
        yield put(TimeActions.loading(true));
    });

    const mode = _.find(ConfigViews[view], {code: codeMode}) || ConfigViews[view][0];

    const query = yield select(state => state.grids[mode.code].query);

    try{
        let res = yield searchBO('timeLine', mode.metrics.config(config), token, {
            whisperers: selectedWhisperers,
            start: +start/1e3,
            stop: +end/1e3,
            intervalMs,
            query,
            timeField: mode.metrics.timeField,
            aggs: mode.metrics.aggs,
        });

        const histo = {
            items: res.aggs && res.aggs.items.buckets.map(b => ({
                    time: b.key_as_string ? moment(b.key_as_string) : moment.unix(b.key),
                    total: _.sum(mode.metrics.bucketMetrics(b)),
                    metrics: mode.metrics.bucketMetrics(b)
                })),
            intervalMs
        };

        yield put(TimeActions.updateHisto(histo));
    }
    catch(e){
        yield put(TimeActions.updateHisto({...oldHisto})); // to remove waitForLoad of timeline
        yield put(UserMessagesActions.showMessage(`Unexpected error while loading Timeline!`));
        console.log(e);
        if(e.target && e.target.status === 401){
            yield put(UserMessagesActions.showMessage(`Your token is too old, please refresh the page to reconnect.`));
        }
    }

    yield [
        cancel(waitTask),
        put(TimeActions.loading(false))
    ];
}

export default function* timeSagas(){
    yield [
        requestStepForward(),
        requestStepBackward(),
        requestCustomRange(),
        requestCustomStart(),
        requestCustomEnd(),
        requestTimeAggLevel(),
        requestRun(),
        requestMoveStart(),
        requestLoadHisto(),
        requestRefreshTimeSpan(),
        requestResetTime(),
        requestGoTo()
    ]
}