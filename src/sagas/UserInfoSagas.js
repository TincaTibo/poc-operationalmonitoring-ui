import { select, put, take } from 'redux-saga/effects';
import uriTemplate from 'uri-templates';

import {
    USER_ACTIONS, MAIN_ACTIONS,
    TIME_ACTIONS, NETWORK_MAP_ACTIONS, MENU_ACTIONS,
    DETAILS_ACTIONS, UPDATE_VIEW_ACTIONS, SESSION_ACTIONS,
    UserActions, GRIDS_ACTIONS
} from '../actions';

function *signOff(){
    while (true) {
        yield take(USER_ACTIONS.SIGN_OFF);

        localStorage.removeItem('spider.user');
        localStorage.removeItem('spider.token');
        localStorage.removeItem('spider.pocState');

        const loginUri = yield select(state => (state.config.login.uri));
        window.location.replace(
            uriTemplate(loginUri)
                .fill({uri: window.location.toString()})
        );
    }
}

function *saveState(){
    while (true) {
        const {type} = yield take([
            USER_ACTIONS.RELOAD_STATE,
            TIME_ACTIONS.STEP_FORWARD,
            TIME_ACTIONS.STEP_BACKWARD,
            TIME_ACTIONS.TIME_AGG_LEVEL,
            TIME_ACTIONS.CUSTOM_START,
            TIME_ACTIONS.CUSTOM_RANGE,
            TIME_ACTIONS.CUSTOM_END,
            TIME_ACTIONS.MOVE_START,
            TIME_ACTIONS.SET_HISTO_OPTION,
            MAIN_ACTIONS.REQUEST_SWITCH_VIEW,
            GRIDS_ACTIONS.TOGGLE_COLUMN_VISIBILITY,
            GRIDS_ACTIONS.SORT_GRID,
            GRIDS_ACTIONS.END_RESIZE_COLUMN,
            GRIDS_ACTIONS.REQUEST_SEARCH,
            DETAILS_ACTIONS.END_RESIZE_DETAILS,
            UPDATE_VIEW_ACTIONS.AUTO_REFRESH_VIEW,
            UPDATE_VIEW_ACTIONS.SET_REFRESH_DELAY,
        ]);

        //Save state because user did something interesting
        const state = yield select();

        let stateToSave = generateStateToSave(state);

        const serializedStateWithHisto = JSON.stringify(stateToSave);
        delete stateToSave.userInfo.historySteps;
        const serializedStateWithoutHisto = JSON.stringify(stateToSave);

        //Save state to local storage
        localStorage.setItem('spider.pocState', serializedStateWithoutHisto);

        window.history.replaceState(serializedStateWithHisto, '', window.location.origin + window.location.pathname);
    }
}

function *saveStateInHistory() {
    while (true) {
        yield take(USER_ACTIONS.SAVE_STATE_IN_HISTORY);

        const state = yield select();

        const stateToSave = generateStateToSave(state);
        const serialized = JSON.stringify(stateToSave);

        window.history.replaceState(serialized, '');
        window.history.pushState(serialized, '', window.location.origin + window.location.pathname);

        yield put(UserActions.incrementHistory());
    }
}

function generateStateToSave(state){
    return {
        userInfo:{
            historySteps: state.userInfo.historySteps,
        },
        time: {
            timeSpan: {
                start: state.time.timeSpan.start,
                stop: state.time.timeSpan.stop
            },
            timeAggregation: state.time.timeAggregation,
            customTimeAggregation: state.time.customTimeAggregation,
            timeLineHisto: {
                domains: state.time.timeLineHisto.domains,
                mode: state.time.timeLineHisto.mode,
            },
        },
        main:{
            view: state.main.view,
            autoRefreshActive: state.main.autoRefreshActive,
            autoRefreshDelay: state.main.autoRefreshDelay,
        },
        details: {
            size: state.details.size,
            pinned: state.details.pinned,
        },
        grids: {
            LOG: {
                columns: state.grids.LOG.columns,
                sort: state.grids.LOG.sort,
                query: state.grids.LOG.query,
            },
        }
    };
}

export default function *userInfoSagas() {
    yield [
        signOff(),
        saveState(),
        saveStateInHistory(),
    ]
}