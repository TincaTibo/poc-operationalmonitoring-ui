import timeSagas from './TimeSagas';
import updateViewSagas from './UpdateViewsSagas';
import userInfoSagas from './UserInfoSagas';
import detailsSagas from './DetailsSagas';
import updateMainSagas from './MainSagas';
import appsViewSagas from './view/AppsViewSagas';
import gridsSagas from './GridsSagas';
import cacheSagas from './CacheSagas';

export default function *networkViewSaga(dispatch) {
    yield [
        timeSagas(),
        gridsSagas(),
        updateViewSagas(),
        userInfoSagas(),
        detailsSagas(),
        updateMainSagas(),
        appsViewSagas(),
        cacheSagas()
    ]
}