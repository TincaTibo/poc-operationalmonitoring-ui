import { select, put, take, fork } from 'redux-saga/effects';

import {COMMON_VIEW_ACTIONS, CommonViewActions, GRIDS_ACTIONS} from '../../actions';
import { searchBO } from '../../containers/BORequests';
import { buildGraphItemsWithQuery } from './ViewFunctions';

function *watchRequestChart() {
    while (true) {
        const {view, chart, interval} = yield take(COMMON_VIEW_ACTIONS.REQUEST_LOAD_CHART);
        if(view === 'apps'){
            yield fork(chartToFunction[chart], {view, chart, interval});
        }
    }
}

function *watchUpdateCharts() {
    const view = 'apps';
    while (true) {
        yield take([
            COMMON_VIEW_ACTIONS.REQUEST_UPDATE_CHARTS,
            GRIDS_ACTIONS.REQUEST_SEARCH
        ]);
        const {
            charts
        } = yield select(state => ({
            charts: state.view[view]
        }));

        for(const chart of Object.getOwnPropertyNames(charts)){
            const interval = charts[chart].data && charts[chart].data.interval;
            if(undefined !== interval){
                yield fork(chartToFunction[chart], {view, chart, interval});
            }
        }
    }
}

function *warningsInLogs({view, chart, interval}){
    yield inLogs({view, chart, interval, eventNameStartsWith: 'WARN_'})
}
function *errorsInLogs({view, chart, interval}){
    yield inLogs({view, chart, interval, eventNameStartsWith: 'FAIL_'})
}
function *inLogs({view, chart, interval, eventNameStartsWith}){
    let {
        start,
        stop,
        searchAPI,
        token,
        query
    } = yield select(state => ({
        start: state.time.timeSpan.start,
        stop: state.time.timeSpan.stop,
        searchAPI: state.config.poc.searchItems,
        token: state.userInfo.token,
        query: state.grids['LOG'].query
    }));

    yield buildGraphItemsWithQuery({
        view,
        chart,
        start, stop,
        interval,
        query: searchBO('computeAggs', searchAPI, token, {
            start: +start/1e3,
            stop: +stop/1e3,
            query: `eventName:${eventNameStartsWith}*${query ? ' AND ' + query : ''}`,
            aggs: {
                histo: {
                    date_histogram: {
                        field: 'startTime',
                        interval: `${interval}m`
                    },
                    aggs: {
                        eventName: {
                            terms: {
                                field: 'eventName',
                                size: 30,
                            }
                        }
                    }
                }
            }
        }),
        initRes: {},
        itemsProcessing: ({item, res, metrics}) => {
            item.eventName && item.eventName.buckets && item.eventName.buckets.forEach(eventName => {
                res[eventName.key] = eventName.doc_count;
                metrics.add(eventName.key);
            });
        }
    });
}

const chartToFunction = {
    warningsInLogs,
    errorsInLogs,
};

export default function *appsViewSagas() {
    yield [
        watchRequestChart(),
        watchUpdateCharts()
    ]
}