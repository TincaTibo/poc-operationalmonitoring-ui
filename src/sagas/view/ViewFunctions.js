import { select, put, fork, cancel } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import moment from 'moment';
import _ from 'lodash';

import { CommonViewActions, UserMessagesActions} from '../../actions';

const pendingTasks = {
};

function *setRequestStatus({view, charts, loadingStatus}){
    const {
        timer
    } = yield select(state => ({
        timer: state.config.view.progressBarTimer,
    }));

    const key = `${view}:${charts.join('+')}`;
    if(pendingTasks[key]){
        //Cancel old pending
        yield cancel(pendingTasks[key]);
    }
    if(loadingStatus === 'PENDING'){
        pendingTasks[key] = yield fork(function * (){
            yield delay(moment.duration(timer).asMilliseconds());
            yield charts.map(chart =>
                put(CommonViewActions.setLoadingStatus({view, chart, loadingStatus}))
            );
        })
    }
    else if(pendingTasks[key]){
        yield charts.map(chart =>
            put(CommonViewActions.setLoadingStatus({view, chart, loadingStatus}))
        );
    }
}

function *handleError({view, charts, chart, e}){
    console.error(e);

    if(chart){
        charts = [chart];
    }

    yield _.flatten([
        put(UserMessagesActions.showMessage(`Unexpected error while fetching data!`)),
        charts.map(chart =>
            put(CommonViewActions.setData({
                view,
                chart,
                data: null
            }))
        ),
        charts.map(chart =>
            put(CommonViewActions.setLoadingStatus({
                view,
                chart,
                loadingStatus: 'ERROR'
            }))
        )
    ]);
    if(e.target && e.target.status === 401){
        yield put(UserMessagesActions.showMessage(`Your token is too old, please refresh the page to reconnect.`));
    }
}

export function *buildGraphItemsWithQuery(
    {
        view,
        chart, //if a single chart as target
        charts, //if many charts as target
        start, //start of graph
        stop, //end of graph
        interval, //interval in min for sampling
        query, //query to perform to BO
        initRes, //res object initialisation
        statsProcessing, //function to extract stats from result of query
        itemsProcessing, //function to extract values to Res from result of query
        save //if we should update the store
    }){

    if(save === undefined){
        save = true;
    }
    if (chart) {
        charts = [chart];
    }

    try {
        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'PENDING'
        });

        const results = yield query;

        let stats;
        if(results.aggs && statsProcessing){
            stats = statsProcessing(results.aggs);
        }

        const nbOfItems = Math.ceil(stop.diff(start) / (interval * 1000 * 60));
        const items = new Array(nbOfItems);

        const sourceItems = results.aggs && results.aggs.histo.buckets;

        //find shift between first date and start + n interval
        const firstDate = sourceItems && moment(sourceItems[0].key_as_string);
        let shiftDate;

        const metricsSet = new Set();

        for (let i = 0, currentMoment = moment(start);
             i < nbOfItems;
             i++, currentMoment.add(interval, 'minute')) {
            const key = currentMoment.toISOString();

            //init value for graph
            if (i !== nbOfItems) {
                items[i] = {
                    time: key,
                    ...initRes
                };
            }

            //get time shift between start of data buckets and start of graph histo bucket
            if (firstDate && currentMoment.isAfter(firstDate) && !shiftDate) {
                shiftDate = currentMoment.diff(firstDate);
            }

            const item = shiftDate && _.find(sourceItems, item => moment(item.key_as_string).isSame(moment(currentMoment).subtract(shiftDate, 'milliseconds')));
            if (item) {
                const res = {
                    ...initRes
                };

                itemsProcessing({item, res, metrics: metricsSet});

                items[i - 1] = {
                    ...items[i - 1],
                    ...res,
                };
            }
        }

        const metrics = [...metricsSet.values()];
        if(save){
            yield charts.map(chart =>
                put(CommonViewActions.setData({
                    view,
                    chart,
                    data: {
                        stats,
                        items,
                        interval,
                        metrics
                    }
                }))
            );
        }

        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'OK'
        });

        return { items, stats, metrics };
    }
    catch(e){
        yield handleError({view, charts, e});
    }
}

export function *buildDistributionGraphItemsWithQuery(
    {
        view,
        chart, //if a single chart as target
        charts, //if many charts as target
        interval, //interval for sampling
        query, //query to perform to BO
        initRes, //res object initialisation
        itemsProcessing, //function to extract values to Res from result of query
        save //if we should update the store
    }){

    if(save === undefined){
        save = true;
    }
    if (chart) {
        charts = [chart];
    }

    try {
        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'PENDING'
        });

        const results = yield query;

        const sourceItems = (results.aggs && results.aggs.histo.buckets) || [];

        const items = sourceItems.map(item => {
            const res = {
                step: item.key,
                ...initRes
            };

            itemsProcessing({item, res});

            return res;
        });

        if(save){
            yield charts.map(chart =>
                put(CommonViewActions.setData({
                    view,
                    chart,
                    data: {
                        items,
                        interval,
                    }
                }))
            );
        }

        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'OK'
        });

        return { items };
    }
    catch(e){
        yield handleError({view, charts, e});
    }
}

export function *buildStatGraphItemsWithQuery(
    {
        view,
        chart, //if a single chart as target
        charts, //if many charts as target
        query, //query to perform to BO
        itemsProcessing, //function to extract values to Res from result of query
        save //if we should update the store
    }){

    if(save === undefined){
        save = true;
    }
    if (chart) {
        charts = [chart];
    }

    try {
        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'PENDING'
        });

        const results = yield query;

        const items = results.aggs ? itemsProcessing({source : results.aggs}) : [];

        if(save){
            yield charts.map(chart =>
                put(CommonViewActions.setData({
                    view,
                    chart,
                    data: {
                        items,
                    }
                }))
            );
        }

        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'OK'
        });

        return { items };
    }
    catch(e){
        yield handleError({view, charts, e});
    }
}

export function *buildScatterGraphItemsWithQuery(
    {
        view,
        chart, //if a single chart as target
        charts, //if many charts as target
        start, //start of graph
        stop, //end of graph
        query, //query to perform to BO
        statsProcessing, //function to extract stats from result of query
        itemsProcessing, //function to extract values to Res from result of query
        save //if we should update the store
    }){

    if(save === undefined){
        save = true;
    }
    if (chart) {
        charts = [chart];
    }

    try {
        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'PENDING'
        });

        const results = yield query;

        let stats;
        if(results.aggs && statsProcessing){
            stats = statsProcessing(results.aggs);
        }

        const sourceItems = (results.aggs && results.aggs.histo.buckets) || [];
        const items = [];
        sourceItems.forEach(step => {
            const time = step.key;

            step.subHisto.buckets.forEach(sourceItem => {
                const res = itemsProcessing({
                    time,
                    ...sourceItem
                });
                if(res){
                    items.push(
                        itemsProcessing({
                            time,
                            ...sourceItem
                        })
                    );
                }
            });
        });

        if(save){
            yield charts.map(chart =>
                put(CommonViewActions.setData({
                    view,
                    chart,
                    data: {
                        stats,
                        items,
                    }
                }))
            );
        }

        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'OK'
        });

        return { items, stats };
    }
    catch(e){
        yield handleError({view, charts, e});
    }
}

export function *buildGaugeGraphItemsWithQuery(
    {
        view,
        chart, //if a single chart as target
        start, //start of graph
        stop, //end of graph
        query, //query to perform to BO
    }){

    let charts;
    if (chart) {
        charts = [chart];
    }

    try {
        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'PENDING'
        });

        const results = yield query;
        const stats = results.aggs && results.aggs.stats;
        const last = results.aggs && results.aggs.slidingMetric.buckets
            && results.aggs.slidingMetric.buckets[results.aggs.slidingMetric.buckets.length-1];

        yield charts.map(chart =>
            put(CommonViewActions.setData({
                view,
                chart,
                data: stats && {
                    min: _.round(stats.min,5),
                    max: _.round(stats.max,5),
                    average: _.round(stats.avg,5),
                    last: _.round(last.metric.value,5)
                }
            }))
        );

        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'OK'
        });
    }
    catch(e){
        yield handleError({view, charts, e});
    }
}

export function *buildServersGraphItemsWithQuery(
    {
        view,
        chart, //if a single chart as target
        start, //start of graph
        stop, //end of graph
        query, //query to perform to BO
    }){

    let charts;
    if (chart) {
        charts = [chart];
    }

    try {
        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'PENDING'
        });

        const results = yield query;

        let data;
        if(results.aggs){
            data = {};
            results.aggs.node.buckets.forEach(node => {
                const cpu = node.statsCpu;
                const freeRam = node.statsFreeRam;
                const totalRam = node.totalRam.value;
                const totalCpu = node.totalCpu.value;
                const last = node.slidingMetric.buckets
                    && node.slidingMetric.buckets[node.slidingMetric.buckets.length-1];

                data[node.key] = {
                    cpu: {
                        min: _.round(cpu.min, 2),
                        max: _.round(cpu.max, 2),
                        average: _.round(cpu.avg, 2),
                        last: _.round(last.cpu.value, 2),
                    },
                    ram: {
                        max: Math.round(totalRam - freeRam.min),
                        min: Math.round(totalRam - freeRam.max),
                        average: Math.round(totalRam - freeRam.avg),
                        last: Math.round(totalRam - last.freeRam.value),
                    },
                    totalRam,
                    totalCpu
                }
            });
        }

        yield charts.map(chart =>
            put(CommonViewActions.setData({
                view,
                chart,
                data
            }))
        );

        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'OK'
        });
    }
    catch(e){
        yield handleError({view, charts, e});
    }
}

export function *buildESNodesGraphItemsWithQuery(
    {
        view,
        chart, //if a single chart as target
        start, //start of graph
        stop, //end of graph
        query, //query to perform to BO
    }){

    let charts;
    if (chart) {
        charts = [chart];
    }

    try {
        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'PENDING'
        });

        const results = yield query;

        let data;
        if(results.aggs){
            data = {};
            results.aggs.node.buckets.forEach(node => {
                const cpu = node.statsCpu;
                const heapUsed = node.statsHeapUsed;
                const heapTotal = node.totalHeap.value;
                const diskUsed = node.statsDiskUsed;
                const diskTotal = node.totalDisk.value;
                const last = node.slidingMetric.buckets
                    && node.slidingMetric.buckets[node.slidingMetric.buckets.length-1];

                data[node.key] = {
                    cpu: {
                        min: _.round(cpu.min, 2),
                        max: _.round(cpu.max, 2),
                        average: _.round(cpu.avg, 2),
                        last: _.round(last.cpu.value, 2),
                    },
                    heap: {
                        max: Math.round(heapUsed.min),
                        min: Math.round(heapUsed.max),
                        average: Math.round(heapUsed.avg),
                        last: Math.round(last.heapUsed.value),
                    },
                    disk: {
                        max: Math.round(diskUsed.min),
                        min: Math.round(diskUsed.max),
                        average: Math.round(diskUsed.avg),
                        last: Math.round(last.diskUsed.value),
                    },
                    heapTotal,
                    diskTotal
                }
            });
        }

        yield charts.map(chart =>
            put(CommonViewActions.setData({
                view,
                chart,
                data
            }))
        );

        yield setRequestStatus({
            view,
            charts,
            loadingStatus: 'OK'
        });
    }
    catch(e){
        yield handleError({view, charts, e});
    }
}
