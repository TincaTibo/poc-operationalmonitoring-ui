import { select, put, take } from 'redux-saga/effects';
import _ from 'lodash';
import { zoomId } from 'd3';

import { DetailsActions, DETAILS_ACTIONS,
         CacheActions } from '../actions';
import {
    getItem,
} from '../containers/BORequests';
import { findResourceInCollection } from './CollectionsUtils';

const dependenciesLoaders = {
    'SETTINGS': loadSettingsDependencies,
    'LOG': loadLogDependencies,
    'DIFF': loadDiffDependencies,
    'ITEM': loadItemDependencies
};

function *openDetails() {
    while (true) {
        const {resourceType, input} = yield take(DETAILS_ACTIONS.REQUEST_OPEN_DETAILS);

        const {
            grids,
            cache,
            shortUris,
            token
        } = yield select(state => ({
            grids: state.grids,
            cache: state.cache,
            shortUris: state.config.shortUris,
            token: state.userInfo.token,
        }));

        let errorMessage,
            id = input;
        try {

            let item,
                name,
                dependencyLoader = dependenciesLoaders[resourceType];

            switch (resourceType) {
                case 'SETTINGS':
                    name = 'SETTINGS';
                    item = null;
                    break;
                case 'DIFF':
                    name = `Diff ${input.selected.length}x${id.view}`;
                    item = {
                        view: input.view,
                        selected: input.selected
                    };
                    id = `DIFF_${input.view}`;
                    const selectedItems = [];
                    switch(item.view){
                        default:
                            for(let id of item.selected){
                                const item = findResourceInCollection(grids[input.view].selected, id);
                                selectedItems.push(item);
                            }
                            break;
                    }
                    item.selected = selectedItems;
                    break;
                case 'ITEM':
                    item = cache.items[id];
                    if(!item){
                        item = yield getItem({
                            uri: id.replace(/^sss:/, shortUris.sss),
                            timeout: 2000
                        }, token);
                        yield put(CacheActions.loadInCache({collection: 'items', item}))
                    }
                    item;
                    name = item.name['x-default'] || item.alternateName;
                    break;
                case 'LOG':
                    item = findResourceInCollection(grids[resourceType].items, id);
                    name = item['@id'];
                    break;
                default:
                    console.error('Cannot open this type');
            }

            if(dependencyLoader){
                yield [
                    put(DetailsActions.openDetails({resourceType, id, name, input})),
                ];
                yield dependencyLoader(id, item, resourceType);
            }
        }
        catch (e) {
            console.error(e);
            yield put(DetailsActions.openDetails({resourceType, id, name: '', links:{
                errorMessage: errorMessage || `Error communicating with server! `
            }}));
        }
    }
}

function *requestResizeDetails() {
    while (true) {
        const {delta} = yield take(DETAILS_ACTIONS.REQUEST_RESIZE_DETAILS);

        const {
            detailsWidth,
            screenWidth
        } = yield select(state => ({
            detailsWidth: state.details.size,
            screenWidth: state.view.screen.width
        }));

        const size = _.clamp(detailsWidth - delta, 400, screenWidth - 300);
        yield put(DetailsActions.resizeDetails(size));
    }
}

function *loadSettingsDependencies(){
}

function *loadLogDependencies(id, item, resourceType){
    yield put(DetailsActions.updateDetails(resourceType, id, {
        log: item
    }));
}

function *loadItemDependencies(id, item, resourceType){
    yield put(DetailsActions.updateDetails(resourceType, id, {
        item
    }));
}

function *loadDiffDependencies(id, item, resourceType){
    yield put(DetailsActions.updateDetails(resourceType, id, item));
}

export default function *detailsSagas() {
    yield [
        openDetails(),
        requestResizeDetails()
    ]
}