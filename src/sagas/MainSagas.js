import { select, put, take } from 'redux-saga/effects';

import {MAIN_ACTIONS, GridsActions, NetworkMapActions, TimeActions, TIME_ACTIONS} from '../actions';

const uiView = 'ui';

function *requestSwitchView(){
    while (true) {
        const {nextView, activeView} = yield take([
            MAIN_ACTIONS.REQUEST_SWITCH_VIEW,
        ]);

        //If coming from UI to other, change timespan and wait for it to call on refresh grahs
        if(nextView !== activeView && [nextView, activeView].includes(uiView)){
            const shouldResetSelection = activeView === uiView;
            yield put(TimeActions.requestResetTime({shouldResetSelection}));
            yield take(TIME_ACTIONS.UPDATE_DOMAINS_TIMELINE);
        }

        yield put(GridsActions.requestUpdateGrid());
        if(nextView === 'main'){
            yield put(NetworkMapActions.requestUpdateMap());
        }
    }
}

export default function *updateMainSagas() {
    yield [
        requestSwitchView()
    ]
}