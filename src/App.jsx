import React, { Component, PropTypes } from 'react';
import moment from 'moment';

import TimeLineContainer from './containers/actions/TimeLineContainer';
import PlayBackControlPanelContainer from './containers/actions/PlayBackControlPanelContainer';
import SimpleMessageContainer from './containers/SimpleMessageContainer';
import DetailsContainer from './containers/details/DetailsContainer';
import MenuBarContainer from './containers/menu/MenuBarContainer';
import ViewContainer from './containers/view/ViewContainer'
import ExpiredMessage from './components/expired-message/ExpiredMessage';
import Search from './components/grids/SearchContainer';
import ParallelViews from './components/parallel-views/ParallelViews';
import DashboardIcon from 'material-ui/svg-icons/editor/pie-chart';
import GridIcon from 'material-ui/svg-icons/action/view-list';

require('./style.less');

const timeLineHeight = 80;
const titleBarHeight = 30;
const searchBarHeight = 45;
const verticalMargin = 10;
const menuBarWidth = 40;

const styles = {
    container: {
        width: '100%',
        height: '100%',
    },
    menu: {
        position: 'absolute',
        left: 0,
        top:0,
        bottom: 0,
        width: menuBarWidth,
        backgroundColor: '#30363E'
    },
    workArea: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: menuBarWidth,
        right: 0,
        backgroundColor: '#e8ecef'
    },
    zoomControlPanel:{
        position: 'absolute',
        float: 'right',
        right: 10,
    },
    playBackControlPanel: {
        position: 'absolute',
        right: 20,
        top: titleBarHeight + searchBarHeight + verticalMargin * 2,
        backgroundColor: 'white',
        borderRadius: 5,
        paddingTop: 5,
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
        height: timeLineHeight
    },
    timeLineContainer:{
        position: 'absolute',
        top: titleBarHeight + searchBarHeight + verticalMargin * 2,
        backgroundColor: 'white',
        borderRadius: 5,
        left: 20,
        paddingRight: 10,
        paddingTop: 5
    },
    parallelViewBar: {
        position: 'absolute',
        top: verticalMargin,
        height: titleBarHeight
    },
    contentContainer:{
        position: 'absolute',
        top: titleBarHeight + timeLineHeight + searchBarHeight + verticalMargin * 5,
        bottom: 0,
        left: 20,
        right: 20,
        overflowX: 'hidden'
    },
    search:{
        position: 'absolute',
        top: titleBarHeight + verticalMargin * 2,
        left: 20
    }
};

export default class App extends Component {

    componentDidMount() {
        window.addEventListener('resize',this.props.onViewResize);

        this.props.onUpdateView();
        this.props.onStartAutoRefresh();

        window.onpopstate = (event) => {
            const previousState = JSON.parse(event.state);

            //Undo previous search state
            const stateToMerge = {
                time:{
                    timeSpan: {
                        start: moment(previousState.time.timeSpan.start),
                        stop: moment(previousState.time.timeSpan.stop)
                    },
                    timeAggregation: moment.duration(previousState.time.timeAggregation),
                    customTimeAggregation: previousState.time.customTimeAggregation,
                    timeLineHisto:{
                        domains: previousState.time.timeLineHisto.domains.map(domain => ({
                            min: domain.min ? moment(domain.min) : null,
                            max: domain.max ? moment(domain.max) : null
                        }))
                    }
                },
                grids: previousState.grids,
                main: previousState.main,
                userInfo: previousState.userInfo,
            };

            this.props.onReloadState(stateToMerge);
        };
    }

    componentWillUnmount(){
        this.props.onExit();
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.details.pinned !== this.props.details.pinned){
            this.props.onViewResize();
        }
    }

    render(){
        return (
            <div id='container'
                 style={styles.container}>
                <div id="menu"
                     style={styles.menu}
                >
                    <MenuBarContainer/>
                </div>
                <div id="app"
                     style={{
                         ...styles.workArea,
                         right: this.props.details.pinned ? this.props.details.width : 0
                     }}
                >
                    <ParallelViews
                        style={styles.parallelViewBar}
                        viewList={[
                            {code: 'apps', label: 'Dashboard', icon: DashboardIcon},
                            {code: 'servers', label: 'Event status', icon: GridIcon}
                        ]}
                        active={this.props.view}
                        onSwitchView={this.props.onSwitchView}
                    />
                    <Search
                        style={styles.search}
                        width={
                            this.props.width
                            - (this.props.details.pinned ? this.props.details.width : 0)
                            - 40
                        }
                        resourceType={'LOG'}
                    />
                    <TimeLineContainer
                        style={styles.timeLineContainer}
                        width={
                            this.props.width
                            - menuBarWidth
                            - (this.props.details.pinned ? this.props.details.width : 0)
                            - 115
                            - 50
                        }
                        height={timeLineHeight}
                    />
                    <PlayBackControlPanelContainer
                        style={styles.playBackControlPanel}
                    />
                    <div
                        style={styles.contentContainer}
                    >
                        <ViewContainer
                            width={
                                this.props.width
                                - menuBarWidth
                                - (this.props.details.pinned ? this.props.details.width : 0)
                                - 40
                            }
                            height={
                                this.props.height
                                - timeLineHeight
                                - searchBarHeight
                                - titleBarHeight
                                - verticalMargin * 5
                                - 30
                            }
                        />
                    </div>
                    <div style={{
                        textAlign:'center',
                        fontSize: '12px',
                        fontFamily: 'Roboto, sans-serif',
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        bottom: 5,
                        color: 'darkGrey'
                    }}>
                        © Flowbird Urban Intelligence / 2018 / POC Monitoring v0.9
                    </div>
                    <SimpleMessageContainer/>
                    {this.props.expired &&
                    <ExpiredMessage
                        loginUri={this.props.loginUri}
                    />
                    }
                </div>
                <DetailsContainer/>
            </div>
        );
    }

    static propTypes = {
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        details: PropTypes.shape({
            pinned: PropTypes.bool.isRequired,
            id: PropTypes.string,
            type: PropTypes.string,
            width: PropTypes.number,
        }),
        loginUri: PropTypes.string,
        expired: PropTypes.bool,
        view: PropTypes.string,

        onViewResize: PropTypes.func.isRequired,
        onUpdateView: PropTypes.func.isRequired,
        onReloadState: PropTypes.func.isRequired,
        onStartAutoRefresh: PropTypes.func.isRequired,
        onSwitchView: PropTypes.func.isRequired
    };
}

