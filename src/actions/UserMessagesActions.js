export const ACTIONS = {
    SHOW_MESSAGE: 'SHOW_MESSAGE'
};

export const Actions = {
    showMessage: (message) => ({
        type: ACTIONS.SHOW_MESSAGE,
        message
    })
};

