export const ACTIONS = {
    REQUEST_UPDATE_VIEW: 'MAIN_REQUEST_UPDATE_VIEW',
    AUTO_REFRESH_VIEW: 'MAIN_AUTO_REFRESH_VIEW',
    START_AUTO_REFRESH_VIEW: 'MAIN_START_AUTO_REFRESH_VIEW',
    SET_REFRESH_DELAY: 'MAIN_SET_REFRESH_DELAY'
};

export const Actions = {
    updateView: ({force, resetTimeSpan, automatic, fromTime} = {}) => ({
        type: ACTIONS.REQUEST_UPDATE_VIEW,
        force,
        resetTimeSpan,
        fromTime, //when changed time
        automatic //when triggered by automatic refresh
    }),
    autoRefresh: (bool) => ({
        type: ACTIONS.AUTO_REFRESH_VIEW,
        active: bool
    }),
    startAutoRefresh: () => ({
        type: ACTIONS.START_AUTO_REFRESH_VIEW
    }),
    setRefreshDelay: (number) => ({
        type: ACTIONS.SET_REFRESH_DELAY,
        delay: number
    }),
};

