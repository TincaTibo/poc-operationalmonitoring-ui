export const ACTIONS = {
    REQUEST_LOAD_IN_CACHE: 'CACHE_REQUEST_LOAD_IN_CACHE',
    LOAD_IN_CACHE: 'CACHE_LOAD_IN_CACHE',
    REPLACE_CACHE: 'CACHE_REPLACE_CACHE',
};

export const Actions = {
    requestLoadInCache: ({collection, id}) => ({
        type: ACTIONS.REQUEST_LOAD_IN_CACHE,
        collection,
        id
    }),
    loadInCache: ({collection, item}) => ({
        type: ACTIONS.LOAD_IN_CACHE,
        collection,
        item
    }),
    replaceCache: ({collection, items}) => ({
        type: ACTIONS.REPLACE_CACHE,
        collection,
        items
    }),
};

