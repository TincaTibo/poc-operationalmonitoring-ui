export const ACTIONS = {
    REQUEST_REFRESH_TIME_SPAN: 'TIME_REQUEST_REFRESH_TIME_SPAN',
    REFRESH_TIME_SPAN: 'TIME_REFRESH_TIME_SPAN',
    REQUEST_RESET_TIME: 'TIME_REQUEST_RESET_TIME',

    REQUEST_STEP_FORWARD: 'TIME_REQUEST_STEP_FORWARD',
    STEP_FORWARD: 'TIME_STEP_FORWARD',

    REQUEST_STEP_BACKWARD: 'TIME_REQUEST_STEP_BACKWARD',
    STEP_BACKWARD: 'TIME_STEP_BACKWARD',

    REQUEST_TIME_AGG_LEVEL: 'TIME_REQUEST_TIME_AGG_LEVEL',
    TIME_AGG_LEVEL: 'TIME_AGG_LEVEL',

    REQUEST_CUSTOM_START: 'TIME_REQUEST_CUSTOM_START',
    CUSTOM_START: 'TIME_CUSTOM_START',

    REQUEST_CUSTOM_END: 'TIME_REQUEST_CUSTOM_END',
    CUSTOM_END: 'TIME_CUSTOM_END',

    REQUEST_CUSTOM_RANGE: 'TIME_REQUEST_CUSTOM_RANGE',
    CUSTOM_RANGE: 'TIME_CUSTOM_RANGE',

    REQUEST_MOVE_START: 'TIME_REQUEST_MOVE_START',
    MOVE_START: 'TIME_MOVE_START',

    REQUEST_RUN: 'TIME_REQUEST_RUN',
    RUN: 'TIME_RUN',

    REQUEST_GOTO: 'TIME_REQUEST_GOTO',

    LOADING: 'TIME_TIMELINE_LOADING',
    REQUEST_LOAD_HISTO: 'TIME_REQUEST_LOAD_HISTO',
    REQUEST_REFRESH_HISTO: 'TIME_REQUEST_REFRESH_HISTO',
    UPDATE_HISTO: 'TIME_UPDATE_HISTO',
    SET_HISTO_OPTION: 'TIME_SET_HISTO_OPTION',

    UPDATE_DOMAINS_TIMELINE: 'TIME_UPDATE_DOMAINS_TIMELINE'
};

export const Actions = {
    requestRefreshTimeSpan: (force=false, shouldResetSelection) => ({
        type: ACTIONS.REQUEST_REFRESH_TIME_SPAN,
        force,
        shouldResetSelection
    }),
    refreshTimeSpan: (min, max, force=false) => ({
        type: ACTIONS.REFRESH_TIME_SPAN,
        min,
        max,
        force
    }),
    requestResetTime: ({shouldResetSelection} = {}) => ({
        type: ACTIONS.REQUEST_RESET_TIME,
        shouldResetSelection
    }),

    goTo: (time) => ({
        type: ACTIONS.REQUEST_GOTO,
        time
    }),

    setStepForward: () => ({ type: ACTIONS.REQUEST_STEP_FORWARD }),
    stepForward: () => ({ type: ACTIONS.STEP_FORWARD }),

    setStepBackward: () => ({ type: ACTIONS.REQUEST_STEP_BACKWARD }),
    stepBackward: () => ({ type: ACTIONS.STEP_BACKWARD }),

    setCustomStart: (time) => ({
        type: ACTIONS.REQUEST_CUSTOM_START,
        newTime: time
    }),
    customStart: (time) => ({
        type: ACTIONS.CUSTOM_START,
        newTime: time
    }),

    setCustomEnd: (time) => ({
        type: ACTIONS.REQUEST_CUSTOM_END,
        newTime: time
    }),
    customEnd: (time) => ({
        type: ACTIONS.CUSTOM_END,
        newTime: time
    }),

    setCustomRange: (start, stop, updateView = true) => ({
        type: ACTIONS.REQUEST_CUSTOM_RANGE,
        start,
        stop,
        updateView
    }),
    customRange: (start, stop) => ({
        type: ACTIONS.CUSTOM_RANGE,
        start,
        stop
    }),

    setMoveStart: (start) => ({
        type: ACTIONS.REQUEST_MOVE_START,
        start
    }),
    moveStart: (start) => ({
        type: ACTIONS.MOVE_START,
        start
    }),

    setTimeAggLevel: (duration) => ({
        type: ACTIONS.REQUEST_TIME_AGG_LEVEL,
        duration
    }),
    timeAggLevel: (duration) => ({
        type: ACTIONS.TIME_AGG_LEVEL,
        duration
    }),

    setRun: (shouldRun) => ({
        type: ACTIONS.REQUEST_RUN,
        shouldRun: shouldRun
    }),
    run: (bool) => ({
        type: ACTIONS.RUN,
        isRunning: bool
    }),

    loading: (bool) => ({
        type: ACTIONS.LOADING,
        bool
    }),
    loadHisto: (intervalMs, start, end) => ({
        type: ACTIONS.REQUEST_LOAD_HISTO,
        intervalMs,
        start,
        end
    }),
    updateHisto: (histo) => ({
        type: ACTIONS.UPDATE_HISTO,
        histo
    }),
    requestRefreshHisto: () => ({
        type: ACTIONS.REQUEST_REFRESH_HISTO
    }),
    updateDomains: (domains) => ({
        type: ACTIONS.UPDATE_DOMAINS_TIMELINE,
        domains
    }),
    setHistoOption: (mode) => ({
        type: ACTIONS.SET_HISTO_OPTION,
        mode
    })
};


