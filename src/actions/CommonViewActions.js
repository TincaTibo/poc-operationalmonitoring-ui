export const ACTIONS = {
    REQUEST_LOAD_CHART: 'VIEW_REQUEST_LOAD_CHART',
    REQUEST_UPDATE_CHARTS: 'VIEW_REQUEST_UPDATE_CHARTS',
    SET_LOADING_STATUS: 'VIEW_SET_LOADING_STATUS',
    SET_DATA: 'VIEW_SET_DATA'
};

export const Actions = {
    requestLoadChart: ({view, chart, interval}) => ({
        type: ACTIONS.REQUEST_LOAD_CHART,
        view,
        chart,
        interval,
    }),
    requestUpdateCharts: ({view}) => ({
        type: ACTIONS.REQUEST_UPDATE_CHARTS,
        view,
    }),
    setLoadingStatus: ({view, chart, loadingStatus}) => ({
        type: ACTIONS.SET_LOADING_STATUS,
        view,
        chart,
        loadingStatus
    }),
    setData: ({view, chart, data}) => ({
        type: ACTIONS.SET_DATA,
        view,
        chart,
        data
    })
};

