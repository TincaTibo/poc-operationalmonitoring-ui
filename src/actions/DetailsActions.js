export const ACTIONS = {
    REQUEST_OPEN_DETAILS: 'REQUEST_OPEN_DETAILS',
    OPEN_DETAILS: 'OPEN_DETAILS',
    CLOSE_DETAILS: 'CLOSE_DETAILS',
    UPDATE_DETAILS: 'UPDATE_DETAILS',
    RESIZE_DETAILS: 'RESIZE_DETAILS',
    END_RESIZE_DETAILS: 'END_RESIZE_DETAILS',
    CHANGE_TAB: 'CHANGE_TAB_DETAILS',
    CLEAR_HISTORY: 'DETAILS_CLEAR_HISTORY',
    PIN_DETAILS: 'DETAILS_PIN_DETAILS',
    REQUEST_RESIZE_DETAILS: 'REQUEST_RESIZE_DETAILS'
};

export const Actions = {
    requestOpenDetails: (resourceType, input) => ({
        type: ACTIONS.REQUEST_OPEN_DETAILS,
        resourceType,
        input
    }),
    openDetails: ({resourceType, id, name, input, links}) => ({
        type: ACTIONS.OPEN_DETAILS,
        resourceType,
        id,
        name,
        links,
        input
    }),
    closeDetails: () => ({
        type: ACTIONS.CLOSE_DETAILS
    }),
    requestResizeDetails : (delta) => ({
        type: ACTIONS.REQUEST_RESIZE_DETAILS,
        delta
    }),
    resizeDetails: (size) => ({
        type: ACTIONS.RESIZE_DETAILS,
        size
    }),
    endResizeDetails: () => ({
        type: ACTIONS.END_RESIZE_DETAILS,
    }),
    updateDetails: (resourceType, id, links) => ({
        type: ACTIONS.UPDATE_DETAILS,
        resourceType,
        id,
        links
    }),
    changeTab: (activeTab) => ({
        type: ACTIONS.CHANGE_TAB,
        activeTab
    }),
    pinDetails: (bool) => ({
        type: ACTIONS.PIN_DETAILS,
        pinned: bool
    })
};

