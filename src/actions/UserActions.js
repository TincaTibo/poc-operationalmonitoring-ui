export const ACTIONS = {
    SIGN_OFF: 'SIGN_OFF',
    RELOAD_STATE: 'USER_RELOAD_STATE',
    INCREMENT_HISTORY: 'USER_INCREMENT_HISTORY',
    EXIT_APPLICATION: 'USER_EXIT_APPLICATION',
    SAVE_STATE_IN_HISTORY: 'USER_SAVE_STATE_IN_HISTORY',
};

export const Actions = {
    signOff: () => ({ type: ACTIONS.SIGN_OFF}),
    reloadState: state => ({
        type: ACTIONS.RELOAD_STATE,
        state
    }),
    incrementHistory: state => ({
        type: ACTIONS.INCREMENT_HISTORY
    }),
    exit: () => ({
       type: ACTIONS.EXIT_APPLICATION
    }),
    saveStateInHistory: () => ({
        type: ACTIONS.SAVE_STATE_IN_HISTORY
    }),
};
