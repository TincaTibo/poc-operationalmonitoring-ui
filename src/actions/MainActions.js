export const ACTIONS = {
    REQUEST_SWITCH_VIEW: 'MAIN_REQUEST_SWITCH_VIEW',
    REFRESHING_VIEW: 'MAIN_REFRESHING_VIEW'
};

export const Actions = {
    requestSwitchView: ({activeView, nextView}) => ({
        type: ACTIONS.REQUEST_SWITCH_VIEW,
        nextView,
        activeView
    }),
    refreshing: (bool) => ({
        type: ACTIONS.REFRESHING_VIEW,
        refreshing: bool
    })
};

