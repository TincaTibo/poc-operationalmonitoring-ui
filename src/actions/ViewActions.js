export const ACTIONS = {
    VIEW_RESIZE: 'VIEW_RESIZE',
    SET_START_ZOOM: 'VIEW_SET_START_ZOOM',
    SET_STOP_ZOOM: 'VIEW_SET_STOP_ZOOM',
};

export const Actions = {
    viewResize: (screen) => ({
        type: ACTIONS.VIEW_RESIZE,
        screen
    }),
    setStartZoom: ({time}) => ({
        type: ACTIONS.SET_START_ZOOM,
        time
    }),
    setStopZoom: ({time}) => ({
        type: ACTIONS.SET_STOP_ZOOM,
        time
    }),
};

