export const ACTIONS = {
    REQUEST_NEXT_PAGE_COLLECTION: 'GRID_REQUEST_NEXT_PAGE_COLLECTION',
    NEXT_PAGE_COLLECTION: 'GRID_NEXT_PAGE_COLLECTION',
    RESIZE_COLUMN: 'GRID_RESIZE_COLUMN',
    TOGGLE_COLUMN_VISIBILITY: 'GRID_TOGGLE_COLUMN_VISIBILITY',
    UPDATE_COLLECTION: 'GRID_UPDATE_COLLECTION',
    SORT_GRID: 'GRID_SORT_GRID',
    REQUEST_SEARCH: 'GRID_REQUEST_SEARCH',
    DO_SEARCH: 'GRID_DO_SEARCH',
    SEARCH_ERROR: 'GRID_SEARCH_ERROR',
    REQUEST_UPDATE_GRID: 'GRID_REQUEST_UPDATE_GRID',
    END_RESIZE_COLUMN: 'GRID_END_RESIZE_COLUMN',
    SELECT_ITEM: 'GRID_SELECT_ITEM',
    SELECT_ITEMS: 'GRID_SELECT_ITEMS',
    SHOW_SELECTED: 'GRID_SHOW_SELECTED',
    CLEAR_SELECTION: 'GRID_CLEAR_SELECTION',
    REFRESH_DATE: 'GRID_REFRESH_DATE'
};

export const Actions = {
    requestUpdateGrid: ({automatic, collection} = {}) => ({
        type: ACTIONS.REQUEST_UPDATE_GRID,
        automatic,
        collection,
    }),
    requestSearch: ({collection, query} = {}) => ({
        type: ACTIONS.REQUEST_SEARCH,
        query,
        collection,
    }),
    doSearch: ({collection, query} = {}) => ({
        type: ACTIONS.DO_SEARCH,
        query,
        collection,
    }),
    refreshDate: (time) => ({
        type: ACTIONS.REFRESH_DATE,
        time
    }),
    searchError: ({collection, searchError} = {}) => ({
        type: ACTIONS.SEARCH_ERROR,
        searchError,
        collection,
    }),
    requestNextPageCollection: (collection) => ({
        type: ACTIONS.REQUEST_NEXT_PAGE_COLLECTION,
        collection
    }),
    nextPageCollection: ({collection, items, nextPage, shouldSearchForSelected, total, time, lastUpdated}) => ({
        type: ACTIONS.NEXT_PAGE_COLLECTION,
        collection,
        items,
        nextPage,
        shouldSearchForSelected,
        total,
        time,
        lastUpdated
    }),
    updateCollection: ({collection, items, total, nextPage, shouldSearchForSelected, time, lastUpdated}) => ({
        type: ACTIONS.UPDATE_COLLECTION,
        collection,
        items,
        total,
        nextPage,
        shouldSearchForSelected,
        time,
        lastUpdated
    }),
    endResizeColumn: () => ({
        type: ACTIONS.END_RESIZE_COLUMN,
    }),
    resizeColumn: (collection, column, width) => ({
        type: ACTIONS.RESIZE_COLUMN,
        collection,
        column,
        width
    }),
    toggleColumnVisibility: (collection, column, visible) => ({
        type: ACTIONS.TOGGLE_COLUMN_VISIBILITY,
        collection,
        column,
        visible
    }),
    sortGrid: (collection, sort) => ({
        type: ACTIONS.SORT_GRID,
        collection,
        sort
    }),
    selectItem: ({collection, item}) => ({
        type: ACTIONS.SELECT_ITEM,
        collection,
        item
    }),
    selectItems: ({collection, items}) => ({
        type: ACTIONS.SELECT_ITEMS,
        collection,
        items
    }),
    showSelected: ({collection}) => ({
        type: ACTIONS.SHOW_SELECTED,
        collection
    }),
    clearSelection: ({collection}) => ({
        type: ACTIONS.CLEAR_SELECTION,
        collection
    }),
};

