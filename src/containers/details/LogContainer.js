import { connect } from 'react-redux';
import Log from '../../components/log';
import {DetailsActions} from '../../actions';

const mapStateToProps = (state) => {
    return {
        id: state.details.id,
        log: state.details.links && state.details.links.log,
        activeTab: state.details.activeTab,
        width: state.details.size
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onChangeTab: (index) => {
            dispatch(DetailsActions.changeTab(index))
        }
    }
};

const LogContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Log);

export default LogContainer;