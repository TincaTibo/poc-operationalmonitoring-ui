import { connect } from 'react-redux';
import Settings from '../../components/settings';
import {DetailsActions} from '../../actions';

const mapStateToProps = (state) => {
    return {
        activeTab: state.details.activeTab,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onChangeTab: (index) => {
            dispatch(DetailsActions.changeTab(index))
        }
    }
};

const SettingsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Settings);

export default SettingsContainer;