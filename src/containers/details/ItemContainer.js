import { connect } from 'react-redux';
import Item from '../../components/item';
import {DetailsActions} from '../../actions';

const mapStateToProps = (state) => {
    return {
        item: state.details.links && state.details.links.item,
        activeTab: state.details.activeTab,
        width: state.details.size
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onChangeTab: (index) => {
            dispatch(DetailsActions.changeTab(index));
        },
    }
};

const ItemContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Item);

export default ItemContainer;