import React from 'react';
import { connect } from 'react-redux';

import Details from '../../components/details';
import {DetailsActions} from '../../actions';
import SettingsContainer from './SettingsContainer';
import LogContainer from './LogContainer';
import ItemContainer from './ItemContainer';
import DiffContainer from './DiffContainer';

const mapStateToProps = (state, ownProps) => {
    return {
        open: state.details.open,
        width: state.details.size,
        children: displayChild(state.details.type),
        pinned: state.details.pinned,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        closeDrawer: () => {
            dispatch(DetailsActions.closeDetails());
        },
        resizeDrawer: (delta) => {
            dispatch(DetailsActions.requestResizeDetails(delta));
        },
        onEndResizeDrawer: () => {
            dispatch(DetailsActions.endResizeDetails());
        },
        onPinDrawer: (bool) => {
            dispatch(DetailsActions.pinDetails(bool));
        }
    }
};

function displayChild(type){
    switch(type){
        case 'SETTINGS':
            return <SettingsContainer/>;
        case 'LOG':
            return <LogContainer/>;
        case 'DIFF':
            return <DiffContainer/>;
        default:
            return <ItemContainer/>;
    }
}

const DetailsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Details);

export default DetailsContainer;