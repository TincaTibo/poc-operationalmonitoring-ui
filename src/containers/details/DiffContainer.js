import { connect } from 'react-redux';
import Diff from '../../components/diff';
import {DetailsActions} from '../../actions';

const mapStateToProps = (state) => {

    return {
        id: state.details.id,
        count: state.details.links && state.details.links.selected.length,
        view: state.details.links && state.details.links.view,
        resourceType: state.details.links && getLabel(state.details.links.view),
        activeTab: state.details.activeTab,
        width: state.details.size
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onChangeTab: (index) => {
            dispatch(DetailsActions.changeTab(index))
        }
    }
};

const DiffContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Diff);

function getLabel(view){
    switch(view){
        case 'LOG':
            return 'log';
        default:
            return 'unknown resource';
    }
}

export default DiffContainer;