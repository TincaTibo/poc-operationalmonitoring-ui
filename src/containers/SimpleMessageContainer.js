import { connect } from 'react-redux';
import {SimpleMessage} from '../components/user-messages';

const mapStateToProps = (state, ownProps) => {
    return {
        message: state.userMessages.simpleMessage,
        time: state.userMessages.simpleMessageTime
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const SimpleMessageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SimpleMessage);

export default SimpleMessageContainer;