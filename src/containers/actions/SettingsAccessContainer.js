import { connect } from 'react-redux';
import { access } from '../../components/settings';
import {DetailsActions} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onClick: () => {
            dispatch(DetailsActions.requestOpenDetails('SETTINGS', 'main'))
        }
    }
};

const SettingsAccessContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(access);

export default SettingsAccessContainer;