import { connect } from 'react-redux';
import TimeLine from '../../components/time-line';
import { TimeActions, UserMessagesActions} from '../../actions';
import _ from 'lodash';

import ConfigViews from '../../components/time-line/ConfigViews';

const mapStateToProps = (state, ownProps) => {
    const mode = _.find(ConfigViews[state.main.view], {code: state.time.timeLineHisto.mode}) || ConfigViews[state.main.view][0];

    return {
        timeSpan: state.time.timeSpan,
        width: ownProps.width || state.view.screen.width - 140,
        height: ownProps.height || 180,
        timeAggregation: state.time.timeAggregation,
        histo: state.time.timeLineHisto.histo,
        domains: state.time.timeLineHisto.domains,
        intervalMs: state.time.timeLineHisto.intervalMs,
        loading: state.time.timeLineHisto.loading,
        mode,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadHisto: (intervalMs, start, end) => {
            dispatch(TimeActions.loadHisto(intervalMs, start, end));
        },
        customRange: (start, stop) => {
            dispatch(TimeActions.setCustomRange(start,stop, false));
        },
        moveStart: (start) => {
            dispatch(TimeActions.setMoveStart(start));
        },
        showMessage: (message) => {
            dispatch(UserMessagesActions.showMessage(message));
        },
        updateDomains: (domains) => {
            dispatch(TimeActions.updateDomains(domains));
        },
        refreshTimeSpan: (min, max) => {
            dispatch(TimeActions.refreshTimeSpan(min, max));
        },
        onResetTime: () => {
            dispatch(TimeActions.requestResetTime());
        },
    }
};

const TimeLineContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TimeLine);

export default TimeLineContainer;