import { connect } from 'react-redux';
import moment from 'moment';
import PlayBackControlPanel from '../../components/playback-controls';
import { TimeActions} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
        timeSpan: state.time.timeSpan,
        timeAggregation: state.time.timeAggregation,
        customTimeAggregation: state.time.customTimeAggregation,
        isRunning: state.time.isRunning,
        style: ownProps.style
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGoToNow: () => {
            dispatch(TimeActions.goTo(moment()));
        },
        onChangeTimeAggregation: (duration) => {
            dispatch(TimeActions.setTimeAggLevel(duration));
        },
        onCustomEnd: (time) => {
            dispatch(TimeActions.setCustomEnd(time));
        },
        onCustomStart: (time) => {
            dispatch(TimeActions.setCustomStart(time));
        },
        onRun: (bool) => {
            dispatch(TimeActions.setRun(bool));
        },
        onStepBackward: () => {
            dispatch(TimeActions.setStepBackward());
        },
        onStepForward: () => {
            dispatch(TimeActions.setStepForward());
        },
    }
};

const PlayBackControlPanelContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(PlayBackControlPanel);

export default PlayBackControlPanelContainer;