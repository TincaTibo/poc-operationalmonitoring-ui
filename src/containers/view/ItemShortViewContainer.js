import {connect} from 'react-redux';
import _ from 'lodash';

import ItemShortView from '../../components/item/ItemShortView';
import {DetailsActions, CacheActions} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    const item = findItem({
        item: ownProps.item,
        items: state.cache.items
    });

    return {
        itemItem: item,
        item: ownProps.item
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onOpen: (id) => {
            dispatch(DetailsActions.requestOpenDetails('ITEM', id));
        },
        onLoad: (id) => {
            dispatch(CacheActions.requestLoadInCache({collection: 'items', id}));
        },
    }
};

const ItemShortViewContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemShortView);

function findItem({item, items}){
    return items[item];
}

export default ItemShortViewContainer;