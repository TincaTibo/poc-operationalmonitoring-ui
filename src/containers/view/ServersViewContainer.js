import {connect} from 'react-redux';

import ServersView from '../../components/view/serversView';
import {} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const ServersViewContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ServersView);

export default ServersViewContainer;