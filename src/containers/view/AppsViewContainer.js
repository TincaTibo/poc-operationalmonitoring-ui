import {connect} from 'react-redux';

import AppsView from '../../components/view/appsView';
import {} from '../../actions';

const mapStateToProps = (state, ownProps) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const AppsViewContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AppsView);

export default AppsViewContainer;