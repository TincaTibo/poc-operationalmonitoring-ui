import {connect} from 'react-redux';

import View from '../../components/view';

const mapStateToProps = (state, ownProps) => {
    return {
        view: state.main.view,
        width: ownProps.width
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const ViewContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(View);

export default ViewContainer;