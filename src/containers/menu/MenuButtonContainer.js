import { connect } from 'react-redux';
import MenuButton from '../../components/menu/elements/MenuButton';
import {MenuActions, SessionActions} from '../../actions';

const mapStateToProps = (state) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onMenuOpen: (bool) => {
        },
    }
};

const MenuButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(MenuButton);

export default MenuButtonContainer;