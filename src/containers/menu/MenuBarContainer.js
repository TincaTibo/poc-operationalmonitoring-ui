import { connect } from 'react-redux';

import MenuBar from '../../components/menu/MenuBar';

const mapStateToProps = (state, ownProps) => {
    return {
        isAdmin: state.userInfo.isAdmin,
        rights: state.userInfo.rights
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const MenuBarContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(MenuBar);

export default MenuBarContainer;