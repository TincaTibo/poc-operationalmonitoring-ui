import q from 'q';
import { request } from 'd3';
import moment from 'moment';

//List of queries
const queries = {
    // Get min/max of a whisperer captured window
    timeSpanQuery: ({timeField} = {}) => ({
        size: 0,
        aggs: {
            minWindow: {min: {field: timeField}},
            maxWindow: {max: {field: timeField}},
        }
    }),
    searchItems: ({start, stop, sort, query, size, whisperers}) => ({
        whisperers: whisperers || [],
        size: size !== undefined ? size : 20,
        startTime: start,
        stopTime: stop,
        query,
        sort: sort ? [{
            key: sort.key,
            order: sort.order
        }] : null
    }),
    computeAggs: ({start, stop, query, aggs, filterOnParsing, whisperers}) => ({
        whisperers: whisperers || [],
        size: 0,
        startTime: start,
        stopTime: stop,
        query,
        filterOnParsing,
        aggs
    }),
    searchConfig: ({sort, query, size}) => ({
        size: size || 20,
        query,
        sort: sort ? [{
            key: sort.key,
            order: sort.order
        }] : null
    }),
    // Timeline query
    timeLine: ({whisperers, start, stop, intervalMs, query, timeField, aggs}) => ({
        size: 0,
        whisperers,
        startTime: start,
        stopTime: stop,
        query,
        filterOnParsing: timeField === 'parsers.http.lastParsing' ? true: undefined,
        aggs: {
            items: {
                date_histogram: {
                    field: timeField,
                    interval: intervalMs ? `${Math.round(intervalMs / 1e3)}s` : '1h'
                },
                aggs
            }
        }
    }),
};

//////////////// Factorized POST query
const postQuery = ({uri, timeout}, token, body, contentType, accept, parse = resultToJson) => {
    return q.nbind(request(uri)
        .header('Authorization', `Bearer ${token}`)
        .header('Accept', accept)
        .header('Content-Type', contentType)
        .timeout(moment.duration(timeout).asMilliseconds())
        .response(parse)
        .post)(body)
};

//Call a search interface
export const searchBO = (query, {uri, timeout}, token, params) => {
    return postQuery({uri: `${uri}?${query}`, timeout}, token, JSON.stringify(queries[query](params)), 'application/json', 'application/json');
};

//Call next page for a partial view collection
export const nextPage = ({uri, timeout}, token, query, collection) => {
    return postQuery({uri: `${uri}?${collection}-nextPage`, timeout}, token, JSON.stringify(query), 'application/json', 'application/json');
};

//Specific query to get a resource by its uri
export const getItem = ({uri, timeout}, token) => {
    return q.nbind(request(uri)
        .header('Authorization', `Bearer ${token}`)
        .header('Accept', 'application/ld+json')
        .timeout(timeout)
        .response(resultToJson)
        .get)()
};

////////////////////// SPECIFIC QUERIES

//Specific query to get application config
export const getConfig = q.nbind(request('./config')
    .header('Accept', `application/ld+json`)
    .timeout(2000)
    .response(resultToJson)
    .get);

///////////////////// PARSERS

//Parse JSON / JSON-LD result
function resultToJson(xhr) {
    switch(xhr.status) {
        case 204:
            return null;
        case 200:
        case 201:
            return JSON.parse(xhr.responseText);
        default:
            throw new Error(`Error while connecting to server:\n${xhr.responseText}`);
    }
}