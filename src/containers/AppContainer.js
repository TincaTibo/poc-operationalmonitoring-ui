import { connect } from 'react-redux';
import { select } from 'd3';

import App from '../App';
import {ViewActions, UpdateViewActions, UserActions, DetailsActions, MainActions} from '../actions';
import moment from 'moment';

const mapStateToProps = (state) => {

    return {
        expired: state.userInfo.expireDate.isBefore(moment()),
        loginUri: state.config.login.uri,
        width: state.view.screen.width,
        height: state.view.screen.height,
        details: {
            id: state.details.id,
            type: state.details.type,
            pinned: state.details.open && state.details.pinned,
            width: state.details.size,
            input: state.details.input
        },
        view: state.main.view
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onViewResize: () => {
            const map = select('#root');
            const screen = {
                width: map.property('clientWidth'),
                height: map.property('clientHeight')
            };

            dispatch(ViewActions.viewResize(screen));
        },
        onUpdateView: () => {
            dispatch(UpdateViewActions.updateView());
        },
        onReloadState: state => {
            dispatch(UserActions.reloadState(state));
            dispatch(UpdateViewActions.updateView());
        },
        onExit: () => {
            dispatch(UserActions.exit());
        },
        onStartAutoRefresh: () => {
            dispatch(UpdateViewActions.startAutoRefresh())
        },
        onSwitchView: ({nextView, activeView}) => {
            dispatch(MainActions.requestSwitchView({nextView, activeView}));
        },
    }
};

const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(App);

export default AppContainer;