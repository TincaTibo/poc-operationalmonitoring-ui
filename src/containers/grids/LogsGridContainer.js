import { connect } from 'react-redux';

import { LogsGrid } from '../../components/grids';

const mapStateToProps = (state, ownProps) => {
    return {
        columns: state.grids.LOG && state.grids.LOG.columns,
        width: ownProps.width || 1200,
        height: ownProps.height || 300
    }
};

const LogsGridContainer = connect(
    mapStateToProps
)(LogsGrid);

export default LogsGridContainer;