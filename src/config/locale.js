import {timeFormat, timeSecond, timeMinute, timeHour, timeDay, timeWeek, timeMonth, timeYear, formatLocale } from 'd3';
import _ from 'lodash';

export const TIME_FORMAT = 'MM-DD HH:mm:ss.SSS';
export const SHORT_TIME_FORMAT = 'HH:mm:ss.SSS';
export const DATETIME_FORMAT = 'YYYY-MM-DD HH:mm:ss.SSS';
export const TIME_FORMAT_SECOND = 'MM-DD HH:mm:ss';
export const SHORT_TIME_FORMAT_SECOND = 'HH:mm:ss';
export const DATETIME_FORMAT_SECOND = 'YYYY-MM-DD HH:mm:ss';
export const DATETIME_FORMAT_MINUTE = 'YYYY-MM-DD HH:mm';
export const DATE_FORMAT = 'MM-DD';
export const EXPIRATION_FORMAT = 'ddd. MMM. DD, HH:mm';

const formatMillisecond = timeFormat('.%L'), // .456
    formatSecond = timeFormat(':%S'),        // :43
    formatMinute = timeFormat('%H:%M'),      // 13:12
    formatHour = timeFormat('%H:00'),        // 13:00
    formatDay = timeFormat('%b %d'),         // Nov 02
    formatMonth = timeFormat('%b %d'),       // Nov 01
    formatYear = timeFormat('%Y %b %d')
;

export function multiFormat(date){
    return (timeSecond(date) < date ? formatMillisecond
        : timeMinute(date) < date ? formatSecond
            : timeHour(date) < date ? formatMinute
                : timeDay(date) < date ? formatHour
                    : timeMonth(date) < date ? formatDay
                        : timeYear(date) < date ? formatMonth
                            : formatYear)(date);
}

export function multiFormatDay(date){
    return (timeSecond(date) < date ? formatMillisecond
        : timeMinute(date) < date ? formatSecond
            : timeHour(date) < date ? formatMinute
                : timeDay(date) < date ? formatHour
                    : timeMonth(date) < date ? formatDay
                        : timeYear(date) < date ? formatMonth
                            : formatYear)(date);
}

const locale = formatLocale({
    decimal: '.',
    thousands: ' ',
    grouping: [3],
    currency: ['','€']
});

export const formatNumber = (number, pad, plus) => {
    const formatted = locale.format(plus?`+,d`:`,d`)(number);
    if(!pad){
        return formatted;
    }
    else{
        return _.padStart(formatted, pad);
    }
};

export const formatFloat = (number, decimals, pad) => {
    const formatted = locale.format(`,.${decimals}f`)(number);
    if(!pad){
        return formatted;
    }
    else{
        return _.padStart(formatted, pad);
    }
};

export const formatRaw = (number, format) => {
    return locale.format(format)(number);
};