const snippets = [
    { meta: 'Elastic', word: 'AND ' },
    { meta: 'Elastic', word: 'OR ' },
    { meta: 'Elastic', word: 'NOT ' },
    { meta: 'Common', word: '@id:' },

    { meta: 'LOG', word: 'closePosEvent' },
    { meta: 'LOG', word: 'creationDate' },
    { meta: 'LOG', word: 'endTime' },
    { meta: 'LOG', word: 'eventName' },
    { meta: 'LOG', word: 'eventStatus' },
    { meta: 'LOG', word: 'eventType' },
    { meta: 'LOG', word: 'internalId' },
    { meta: 'LOG', word: 'openPosEvent' },
    { meta: 'LOG', word: 'park' },
    { meta: 'LOG', word: 'peripheralType' },
    { meta: 'LOG', word: 'pos' },
    { meta: 'LOG', word: 'serialNumber' },
    { meta: 'LOG', word: 'startTime' },
    { meta: 'LOG', word: 'technicalStatus' },
    { meta: 'LOG', word: 'updateDatetime' },
];

const getSnippets= (grid) => {
    switch(grid){
        case 'LOG':
            return snippets.filter(snip => (['Elastic', 'Common', 'LOG'].indexOf(snip.meta) > -1));
        default:
            return snippets.filter(snip => (['Elastic', 'Common'].indexOf(snip.meta) > -1));
    }
};

export default getSnippets;
