const Colors = {
    //text - axes
    light: '#ececec',
    medium: '#b4b4b4',
    mediumDark: '#545454',
    dark: '#333333',

    //timeline, graphics
    requestFill: '#9be18c',
    requestStroke: '#5db352',

    responseFill: '#ffd680',
    responseStroke: '#f59f12',

    itemFill: '#cddbf4',
    itemStroke: '#3c75a6',
    itemFillSelected: '#ff8080',

    //stats
    statsFill: '#4ba3e2',
    statsStroke: '#3c75a6',
    statsSelectedFill: '#ff8080',
    statsSelectedStroke: '#bd4041',

    //buttons, checkboxes
    optionSelected: '#00bcd4',
    optionMultiSelected: '#55c046',
    iconHover: '#64C4E0',

    //colors for node and seq diag
    server: {
        fill: '#9fc1e9',
        stroke: '#000082'
    },
    client: {
        fill: '#ffd56d',
        stroke: '#D58B24'
    },
    ui: {
        fill: '#d2acff',
        stroke: '#8e36d5'
    },
    poller: {
        fill: '#a0ffae',
        stroke: '#4bd513'
    },
    cache: {
        fill: '#ffaebe',
        stroke: '#d53243'
    },
    db: {
        fill: '#ffcaae',
        stroke: '#d55d2a'
    },
    lock: {
        fill: '#ffd680'
    },

    //menu
    menu: {
        iconHover: '#DFDFDF',
        iconActive: '#333333',
        iconInactive: '#b3b3b3',
    },

    //arrows
    serverError: '#ff5d5a',
    serverErrorStroke: '#f6251e',
    clientError: '#f6bc62',
    clientErrorStroke: '#e69825',
    success: '#9be18c',
    successStroke: '#5db352',
    default: '#d1d6ff',
    hover: '#f6251e',

    //servers status
    serverOK: '#4fe14f',
    serverKO: '#ff5353',

    //messages, config
    error: '#FF0000',
    working: '#e69825',
    info: '#55C046',
    hint: '#DDD',

    seriesColors: [
        '#a6cee3',
        '#cab2d6',
        '#b2df8a',
        '#fdbf6f',
        '#fb9a99',
        '#ffdf20',
        '#1f78b4',
        '#6a3d9a',
        '#33a02c',
        '#ff7f00',
        '#e31a1c',
        '#b15928',
    ]
};

export default Colors;