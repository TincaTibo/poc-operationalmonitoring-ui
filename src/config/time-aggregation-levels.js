import moment from 'moment';

export const TIME_AGGREGATION_LEVELS = [
        { key: 'MIN', display: '00:01:00', duration: moment.duration('PT1M'), unit: 'Minute'},
        { key: 'QUARTER', display: '00:15:00', duration: moment.duration('PT15M'), unit: 'Quarter'},
        { key: 'HOUR', display: '01:00:00', duration: moment.duration('PT1H'), unit: 'Hour'},
        { key: '6HOURS', display: '06:00:00', duration: moment.duration('PT6H'), unit: '6 hours'},
        { key: 'DAY', display: '24:00:00', duration: moment.duration('P1D'), unit: 'Day'}
    ]
;