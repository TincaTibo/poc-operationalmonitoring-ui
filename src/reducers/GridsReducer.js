import {GRIDS_ACTIONS} from '../actions';
import _ from 'lodash';
import moment from 'moment';

export default function GridsReducer(state = {}, action) {
    switch (action.type) {

        case GRIDS_ACTIONS.UPDATE_COLLECTION:{
            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    items: action.items,
                    total: action.total,
                    nextPage: action.nextPage,
                    shouldSearchForSelected: action.shouldSearchForSelected,
                    lastUpdate: action.time,
                    lastUpdated: action.lastUpdated,
                }
            };
        }

        case GRIDS_ACTIONS.NEXT_PAGE_COLLECTION:{
            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    items: _.unionBy(state[action.collection].items, action.items, '@id'),
                    nextPage: action.nextPage,
                    shouldSearchForSelected: action.shouldSearchForSelected,
                    total: action.total,
                    lastUpdate: action.time,
                    lastUpdated: action.lastUpdated,
                }
            };
        }

        case GRIDS_ACTIONS.REFRESH_DATE:{
            const newState = {...state};
            Object.getOwnPropertyNames(newState).forEach(collection => {
                newState[collection] = {
                    ...newState[collection],
                    lastUpdated: moment.duration(action.time.diff(newState[collection].lastUpdate))
                }
            });

            return newState;
        }

        case GRIDS_ACTIONS.RESIZE_COLUMN:{
            let columns = [...state[action.collection].columns];
            const idx = _.findIndex(columns, {id: action.column});

            //return if did not found column
            if(idx === -1){
                return;
            }

            columns[idx] = {
                ...columns[idx],
                width: action.width
            };

            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    columns
                }
            };
        }

        case GRIDS_ACTIONS.TOGGLE_COLUMN_VISIBILITY:{
            let columns = [...state[action.collection].columns];
            const idx = _.findIndex(columns, {id: action.column});

            //return if did not found column
            if(idx === -1){
                return;
            }

            columns[idx] = {
                ...columns[idx],
                visible: action.visible
            };

            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    columns
                }
            };
        }

        case GRIDS_ACTIONS.SORT_GRID:{
            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    sort: action.sort
                }
            };
        }

        case GRIDS_ACTIONS.DO_SEARCH:{
            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    query: action.query
                }
            };
        }

        case GRIDS_ACTIONS.SEARCH_ERROR:{
            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    searchError: action.searchError
                }
            };
        }

        case GRIDS_ACTIONS.SELECT_ITEM:{
            const selected = [...state[action.collection].selected];
            const index = _.findIndex(selected, { '@id' : action.item['@id']});
            if(index > -1){
                selected.splice(index, 1);
            }
            else{
                selected.push(action.item);
            }

            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    selected
                }
            };
        }

        case GRIDS_ACTIONS.SELECT_ITEMS:{
            const selected = [...state[action.collection].selected];
            action.items.forEach(item => {
                const index = _.findIndex(selected, { '@id' : item['@id']});
                if(index > -1){
                    selected.splice(index, 1);
                }
                else{
                    selected.push(item);
                }
            });

            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    selected
                }
            };
        }

        case GRIDS_ACTIONS.CLEAR_SELECTION:{
            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    selected: []
                }
            };
        }

        default:
            return state;
    }
}

