import moment from 'moment';
import {scaleTime} from 'd3';

import {TIME_ACTIONS} from '../actions';

export default function timeReducer(state = {}, action) {
    let start, stop, min, max;
    switch (action.type) {

        case TIME_ACTIONS.REFRESH_TIME_SPAN:
            min = moment(action.min);
            max = moment(action.max);

            [min, max] = scaleTime()
                .domain([min, max])
                .nice()
                .domain();

            max = moment(max);
            min = moment(min).second(0).millisecond(0);
            if(max.second()!==0 || max.millisecond()!==0){
                max.add(1, 'minute').millisecond(0).second(0);
            }

            if(!action.force){
                min = moment.min([min, state.timeSpan.min]);
                max = moment.max([max, state.timeSpan.max]);
                max = moment.min([max, moment().add(1,'day').hour(0).minute(0).second(0).millisecond(0)]);
            }
            return {
                ...state,
                timeSpan: {
                    ...state.timeSpan,
                    min,
                    max
                }
            };

        case TIME_ACTIONS.STEP_FORWARD:
            start = moment(state.timeSpan.start).add(state.timeAggregation);
            stop = moment(start).add(state.timeAggregation);
            max = moment.max([stop, state.timeSpan.max]);
            max = moment.min([max, moment().add(1,'day').hour(0).minute(0).second(0).millisecond(0)]);
            return {
                ...state,
                timeSpan: {
                    ...state.timeSpan,
                    start,
                    stop,
                    max
                }
            };

        case TIME_ACTIONS.STEP_BACKWARD:
            start = moment(state.timeSpan.start).subtract(state.timeAggregation);
            stop = moment(start).add(state.timeAggregation);
            min = moment.min([start, state.timeSpan.min]);
            return {
                ...state,
                timeSpan: {
                    ...state.timeSpan,
                    start,
                    stop,
                    min
                }
            };

        case TIME_ACTIONS.TIME_AGG_LEVEL:
            if(moment(state.timeSpan.start).add(action.duration).isAfter(moment())){
                stop = moment();
                start = moment(stop).subtract(action.duration);
            }
            else{
                start = state.timeSpan.start;
                stop = moment(state.timeSpan.start).add(action.duration);
            }
            min = moment.min([start, state.timeSpan.min]);
            max = moment.max([stop, state.timeSpan.max]);
            max = moment.min([max, moment().add(1,'day').hour(0).minute(0).second(0).millisecond(0)]);

            return {
                ...state,
                timeAggregation: action.duration,
                timeSpan: {
                    ...state.timeSpan,
                    start,
                    stop,
                    min,
                    max
                },
                customTimeAggregation: false
            };

        case TIME_ACTIONS.CUSTOM_START:
            start = action.newTime;
            min = moment(moment.min([start, state.timeSpan.min])).millisecond(0).second(0);
            return {
                ...state,
                timeSpan: {
                    ...state.timeSpan,
                    start,
                    min
                },
                timeAggregation: moment.duration(state.timeSpan.stop.diff(start)),
                customTimeAggregation: true
            };

        case TIME_ACTIONS.CUSTOM_RANGE:
            start = moment(action.start).millisecond(0).second(0);
            stop = moment(action.stop);
            if(stop.second()!==0 || stop.millisecond()!==0){
                stop.add(1, 'minute').millisecond(0).second(0);
            }
            min = moment.min([start, state.timeSpan.min]);
            max = moment.max([stop, state.timeSpan.max]);
            max = moment.min([max, moment().add(1,'day').hour(0).minute(0).second(0).millisecond(0)]);
            return {
                ...state,
                timeSpan: {
                    ...state.timeSpan,
                    start,
                    stop,
                    min,
                    max
                },
                timeAggregation: moment.duration(stop.diff(start)),
                customTimeAggregation: true
            };

        case TIME_ACTIONS.CUSTOM_END:
            stop = moment(action.newTime);
            if(stop.second()!==0 || stop.millisecond()!==0){
                stop.add(1, 'minute').millisecond(0).second(0);
            }
            max = moment.max([stop, state.timeSpan.max]);
            max = moment.min([max, moment().add(1,'day').hour(0).minute(0).second(0).millisecond(0)]);
            return {
                ...state,
                timeSpan: {
                    ...state.timeSpan,
                    stop,
                    max,
                },
                timeAggregation: moment.duration(stop.diff(state.timeSpan.start)),
                customTimeAggregation: true
            };

        case TIME_ACTIONS.MOVE_START:
            start = moment(action.start).millisecond(0).second(0);
            stop = moment(start).add(state.timeAggregation);
            min = moment.min([start, state.timeSpan.min]);
            max = moment.max([stop, state.timeSpan.max]);
            max = moment.min([max, moment().add(1,'day').hour(0).minute(0).second(0).millisecond(0)]);
            return {
                ...state,
                timeSpan: {
                    ...state.timeSpan,
                    start,
                    stop,
                    min,
                    max,
                }
            };

        case TIME_ACTIONS.RUN:
            return {
                ...state,
                isRunning: action.isRunning
            };

        case TIME_ACTIONS.UPDATE_HISTO:
            return {
                ...state,
                timeLineHisto: {
                    ...state.timeLineHisto,
                    histo: action.histo
                }
            };

        case TIME_ACTIONS.LOADING:
            return {
                ...state,
                timeLineHisto: {
                    ...state.timeLineHisto,
                    loading: action.bool
                }
            };

        case TIME_ACTIONS.UPDATE_DOMAINS_TIMELINE:
            return {
                ...state,
                timeLineHisto: {
                    ...state.timeLineHisto,
                    domains: action.domains
                }
            };

        case TIME_ACTIONS.SET_HISTO_OPTION:
            return {
                ...state,
                timeLineHisto: {
                    ...state.timeLineHisto,
                    mode: action.mode
                }
            };

        default:
            return state;
    }
}