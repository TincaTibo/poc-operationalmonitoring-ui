import {VIEW_ACTIONS} from '../actions';
import commonViewReducer from './view/CommonViewReducer';

function networkViewApp(state = {}, action) {
    switch (action.type) {

        case VIEW_ACTIONS.VIEW_RESIZE:
            return {
                ...state,
                screen: action.screen
            };

        case VIEW_ACTIONS.SET_START_ZOOM:
            return {
                ...state,
                zoom: {
                    ...state.zoom,
                    start: action.time
                }
            };

        case VIEW_ACTIONS.SET_STOP_ZOOM:
            return {
                ...state,
                zoom: {
                    ...state.zoom,
                    stop: action.time
                }
            };

        default:
            return {
                ...state,
                [action.view]: action.view && commonViewReducer(state[action.view], action),
            }
    }
}

export default networkViewApp;
