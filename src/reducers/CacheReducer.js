import {CACHE_ACTIONS} from '../actions';

function CacheReducer(state = {}, action) {
    switch (action.type) {

        case CACHE_ACTIONS.LOAD_IN_CACHE:

            return {
                ...state,
                [action.collection]: {
                    ...state[action.collection],
                    [action.item['@id']]: action.item
                }
            };

        case CACHE_ACTIONS.REPLACE_CACHE:
            const hash = {};
            action.items.forEach(item => { hash[item['@id']] = item });
            return {
                ...state,
                [action.collection]: hash
            };

        default:
            return {
                ...state,
            }
    }
}

export default CacheReducer;
