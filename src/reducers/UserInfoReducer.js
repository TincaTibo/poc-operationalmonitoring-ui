import {USER_ACTIONS} from '../actions';

export default function userInfoReducer(state = {}, action) {
    switch (action.type) {

        case USER_ACTIONS.INCREMENT_HISTORY:
            return {
                ...state,
                historySteps: state.historySteps + 1
            };

        default:
            return state;
    }
}
