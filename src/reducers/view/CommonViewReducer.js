import {COMMON_VIEW_ACTIONS} from '../../actions';

function commonViewReducer(state = {}, action) {
    switch (action.type) {

        case COMMON_VIEW_ACTIONS.SET_LOADING_STATUS: 
            return {
                ...state,
                [action.chart]: {
                    ...state[action.chart],
                    loadingStatus: action.loadingStatus
                }
            };

        case COMMON_VIEW_ACTIONS.SET_DATA:
            return {
                ...state,
                [action.chart]: {
                    ...state[action.chart],
                    data: action.data
                }
            };

        default:
            return state;
    }
}

export default commonViewReducer;
