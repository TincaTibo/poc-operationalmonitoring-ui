import {DETAILS_ACTIONS} from '../actions';
import _ from 'lodash';

export default function detailsReducer(state = {}, action) {
    switch (action.type) {

        case DETAILS_ACTIONS.OPEN_DETAILS:{
            let history = [...state.history];
            let activeTab = state.activeTab;

            //reset tab when changing resource type
            if(action.resourceType !== state.type){
                activeTab = 0;
            }

            let alreadyPresentIndex = _.findIndex(history, {id : action.id, type: action.resourceType});
            if(alreadyPresentIndex > -1){
                history = history.slice(0, alreadyPresentIndex + 1);
            }
            else{
                history.push({
                    id: action.id,
                    name: action.name,
                    type: action.resourceType,
                    input: action.input
                });
            }

            return {
                ...state,
                id: action.id,
                type: action.resourceType,
                links: action.links,
                history,
                open: true,
                size: state.size,
                input: action.input,
                activeTab
            };
        }

        case DETAILS_ACTIONS.UPDATE_DETAILS:{
            if(state.id !== action.id || state.type !== action.resourceType){
                return state;
            }

            return {
                ...state,
                links: {
                    ...state.links,
                    ...action.links
                }
            };
        }

        case DETAILS_ACTIONS.CLOSE_DETAILS:{
            return {
                ...state,
                open: false,
                history: []
            }
        }

        case DETAILS_ACTIONS.PIN_DETAILS:{
            return {
                ...state,
                pinned: action.pinned
            }
        }

        case DETAILS_ACTIONS.RESIZE_DETAILS:{
            return {
                ...state,
                size: action.size
            }
        }

        case DETAILS_ACTIONS.CHANGE_TAB:{
            return {
                ...state,
                activeTab: action.activeTab
            }
        }

        case DETAILS_ACTIONS.CLEAR_HISTORY:{
            return {
                ...state,
                history: []
            }
        }

        default:
            return state;
    }
}
