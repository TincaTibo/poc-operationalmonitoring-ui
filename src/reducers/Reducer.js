import {combineReducers} from 'redux';
import _ from 'lodash';

import userInfo from './UserInfoReducer';
import time from './TimeReducer';
import userMessages from './UserMessagesReducer';
import view from './ViewReducer';
import details from './DetailsReducer';
import menu from './MenuReducer';
import main from './MainReducer';
import grids from './GridsReducer';
import cache from './CacheReducer';

import {USER_ACTIONS} from '../actions';

const appReducer = combineReducers({
    userInfo,
    time,
    userMessages,
    view,
    details,
    menu,
    main,
    grids,
    cache,
    config: (state = {}, action) => (state)
});

const rootReducer = (state, action) => {
    if(action.type === USER_ACTIONS.RELOAD_STATE) {
        return _.merge({}, state, action.state);
    }
    else{
        return appReducer(state, action)
    }
};

export default rootReducer;