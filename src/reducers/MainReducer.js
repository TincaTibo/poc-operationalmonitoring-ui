import {MAIN_ACTIONS, UPDATE_VIEW_ACTIONS} from '../actions';

export default function MainReducer(state = {}, action) {
    switch (action.type) {

        case MAIN_ACTIONS.REFRESHING_VIEW: {
            return {
                ...state,
                refreshing: action.refreshing
            }
        }

        case MAIN_ACTIONS.REQUEST_SWITCH_VIEW: {
            return {
                ...state,
                view: action.nextView
            }
        }

        case UPDATE_VIEW_ACTIONS.AUTO_REFRESH_VIEW:
            return {
                ...state,
                autoRefreshActive: action.active
            };

        case UPDATE_VIEW_ACTIONS.SET_REFRESH_DELAY:
            return {
                ...state,
                autoRefreshDelay: action.delay
            };

        default:
            return state;
    }
}

