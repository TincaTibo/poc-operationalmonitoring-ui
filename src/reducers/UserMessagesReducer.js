import {USER_MESSAGES_ACTIONS} from '../actions';
import _ from 'lodash';
import moment from 'moment';

export default function userMessagesReducer(state = {}, action) {
    switch (action.type) {

        case USER_MESSAGES_ACTIONS.SHOW_MESSAGE:
            return _.assign({}, state, {
                simpleMessage: action.message,
                simpleMessageTime: moment()
            });

        default:
            return state;
    }
}

